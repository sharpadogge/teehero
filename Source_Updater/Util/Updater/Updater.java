// 
// Decompiled by Procyon v0.5.36
// 

package Util.Updater;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import java.nio.file.Paths;
import java.io.IOException;
import java.io.File;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert;
import javafx.event.Event;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.beans.value.ObservableValue;
import Util.ServerConnector.ServerConnector;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.geometry.Pos;
import java.io.InputStream;
import java.io.FileInputStream;
import javafx.stage.StageStyle;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.Node;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.HBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.image.Image;
import Util.Enums.FilePathsUpdater;
import javafx.stage.Stage;
import javafx.scene.control.ProgressBar;
import javafx.application.Application;

public class Updater extends Application
{
    private ProgressBar pgb;
    
    public static void main(final String[] args) {
        launch(args);
    }
    
    public void start(final Stage stage) throws Exception {
        final String os = System.getProperty("os.name").toLowerCase();
        if (!os.contains("mac")) {
            stage.getIcons().add((Object)new Image("file:" + FilePathsUpdater.IMG_PATH.getPath() + "Cape.png"));
        }
        final BorderPane p = new BorderPane();
        final HBox vbTop = new HBox();
        final Region region1 = new Region();
        HBox.setHgrow((Node)region1, Priority.ALWAYS);
        final Region region2 = new Region();
        HBox.setHgrow((Node)region2, Priority.ALWAYS);
        p.setPrefSize(200.0, 200.0);
        final ImageView iv = new ImageView();
        final Label lbl = new Label("Searching for Updates...");
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("TeeHero Updater");
        lbl.setPrefHeight(50.0);
        iv.setImage(new Image((InputStream)new FileInputStream(String.valueOf(FilePathsUpdater.IMG_PATH.getPath()) + "TeeHero red.png"), 100.0, 50.0, false, true));
        vbTop.getChildren().addAll((Object[])new Node[] { (Node)region1, (Node)lbl, (Node)region2 });
        p.setTop((Node)vbTop);
        p.setCenter((Node)iv);
        vbTop.setAlignment(Pos.CENTER_LEFT);
        stage.setScene(new Scene((Parent)p));
        stage.show();
        Platform.runLater((Runnable)new Runnable() {
            @Override
            public void run() {
                Updater.this.checkingForUpdates(stage);
            }
        });
    }
    
    private void checkingForUpdates(final Stage stage) {
        final ServerConnector sc = new ServerConnector();
        if (!sc.isNewestVersion()) {
            if (this.softwareShouldBeUpdated()) {
                this.changeToDownloadStage(stage);
                if (!this.doesOldVersionFolderExist()) {
                    this.createOldVersionFolder();
                    if (this.doesOldVersionExist()) {
                        this.moveOldVersion();
                    }
                }
                final String url = sc.getDownloadUrl();
                System.out.println(url);
                final Download download = this.getDownloadJob(url);
                this.pgb.progressProperty().bind((ObservableValue)download.progressProperty());
                new Thread((Runnable)download).start();
                download.setOnSucceeded((EventHandler)new EventHandler<WorkerStateEvent>() {
                    public void handle(final WorkerStateEvent event) {
                        sc.writeNewVersionToVersionFile();
                        Updater.this.showDownloadDoneAlert();
                        Updater.this.launchMainProgram(stage);
                    }
                });
            }
            else {
                this.launchMainProgram(stage);
            }
        }
        else {
            this.launchMainProgram(stage);
        }
    }
    
    private boolean softwareShouldBeUpdated() {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText((String)null);
        alert.setTitle("Updates available");
        alert.setContentText("A newer version of the software is available.\nDo you want to update to the newest version?");
        alert.getButtonTypes().clear();
        final ButtonType btnYes = new ButtonType("Yes");
        alert.getButtonTypes().add((Object)btnYes);
        final ButtonType btnNo = new ButtonType("No");
        alert.getButtonTypes().add((Object)btnNo);
        alert.showAndWait();
        return alert.getResult() == btnYes;
    }
    
    private boolean doesOldVersionExist() {
        final File oldVersion = new File(String.valueOf(FilePathsUpdater.OLD_VERISON_PATH.getPath()) + "TeeHero.jar");
        return oldVersion.exists();
    }
    
    private boolean doesOldVersionFolderExist() {
        final File oldVersionFolder = new File(String.valueOf(FilePathsUpdater.OLD_VERISON_PATH.getPath()) + "Old_Version");
        return oldVersionFolder.exists();
    }
    
    private void createOldVersionFolder() {
        final File oldVersionFolder = new File(String.valueOf(FilePathsUpdater.OLD_VERISON_PATH.getPath()) + "Old_Version");
        oldVersionFolder.mkdir();
    }
    
    private void showDownloadDoneAlert() {
        final Alert complete = new Alert(Alert.AlertType.INFORMATION);
        complete.setHeaderText((String)null);
        complete.setTitle("Download Successful");
        complete.setContentText("The newest version has been downloaded.\nClick \"OK\" to launch the newest version.");
        complete.showAndWait();
    }
    
    private void launchMainProgram(final Stage stage) {
        final File mainProgram = new File(String.valueOf(FilePathsUpdater.MAIN_PROGRAM_PATH.getPath()) + "TeeHero.jar");
        final File oldVersion = new File(String.valueOf(FilePathsUpdater.OLD_VERISON_PATH.getPath()) + "TeeHero.jar");
        if (mainProgram.exists()) {
            try {
                Runtime.getRuntime().exec("java -jar " + mainProgram.getPath());
                stage.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (oldVersion.exists()) {
            try {
                this.getOldVersion();
                Runtime.getRuntime().exec("java -jar " + mainProgram.getPath());
                stage.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            final Alert fatalError = new Alert(Alert.AlertType.ERROR);
            fatalError.setHeaderText((String)null);
            fatalError.setTitle("Error");
            fatalError.setContentText("We were unable to find a version of the software on your computer. Please download the software from our website");
            fatalError.showAndWait();
            stage.close();
        }
    }
    
    private Download getDownloadJob(final String url) {
        final Download download = new Download(url);
        return download;
    }
    
    private void moveOldVersion() {
        final File oldVersion = new File(String.valueOf(FilePathsUpdater.MAIN_PROGRAM_PATH.getPath()) + "TeeHero.jar");
        final File backupFolder = new File(FilePathsUpdater.OLD_VERISON_PATH.getPath());
        try {
            Files.move(Paths.get(oldVersion.getPath(), new String[0]), Paths.get(String.valueOf(backupFolder.getPath()) + "\\" + oldVersion.getPath(), new String[0]), StandardCopyOption.ATOMIC_MOVE);
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    
    private void getOldVersion() {
        final File oldVersion = new File(String.valueOf(FilePathsUpdater.OLD_VERISON_PATH.getPath()) + "TeeHero.jar");
        try {
            Files.move(Paths.get(oldVersion.getPath(), new String[0]), Paths.get(".\\" + oldVersion.getName(), new String[0]), StandardCopyOption.ATOMIC_MOVE);
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    
    private void changeToDownloadStage(final Stage stage) {
        final BorderPane p = new BorderPane();
        final HBox vbTop = new HBox();
        p.setPrefSize(200.0, 200.0);
        final Region region1 = new Region();
        HBox.setHgrow((Node)region1, Priority.ALWAYS);
        final Region region2 = new Region();
        HBox.setHgrow((Node)region2, Priority.ALWAYS);
        final ImageView iv = new ImageView();
        final Label lbl = new Label("Updating...");
        final HBox hbBot = new HBox();
        (this.pgb = new ProgressBar()).setPrefSize(200.0, 30.0);
        hbBot.getChildren().addAll((Object[])new Node[] { (Node)this.pgb });
        lbl.setPrefHeight(50.0);
        iv.setImage(new Image(new File(String.valueOf(FilePathsUpdater.IMG_PATH.getPath()) + "45.gif").toURI().toString()));
        vbTop.getChildren().addAll((Object[])new Node[] { (Node)region1, (Node)lbl, (Node)region2 });
        p.setTop((Node)vbTop);
        p.setCenter((Node)iv);
        p.setBottom((Node)hbBot);
        vbTop.setAlignment(Pos.CENTER_LEFT);
        stage.setScene(new Scene((Parent)p));
    }
    
    public void updateProgress(final double value) {
        this.pgb.setProgress(value);
    }
}
