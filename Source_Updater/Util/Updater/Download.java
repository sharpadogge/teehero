// 
// Decompiled by Procyon v0.5.36
// 

package Util.Updater;

import Util.Enums.FilePathsUpdater;
import java.nio.channels.Channels;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.ReadableByteChannel;
import java.io.FileOutputStream;
import javafx.concurrent.Task;

public class Download extends Task<Void> implements ProgressCallBack
{
    private FileOutputStream fos;
    private ReadableByteChannel rbc;
    private URL url;
    
    public Download(final String url) {
        try {
            this.url = new URL(url);
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    
    private int contentLength(final URL url) {
        int contentLength = -1;
        try {
            final HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            contentLength = connection.getContentLength();
        }
        catch (Exception ex) {}
        return contentLength;
    }
    
    public void callback(final CallBackByteChannel rbc, final double progress) {
        this.updateProgress(progress, 100.0);
    }
    
    protected Void call() throws Exception {
        try {
            this.rbc = new CallBackByteChannel(Channels.newChannel(this.url.openStream()), this.contentLength(this.url), this);
            this.fos = new FileOutputStream(String.valueOf(FilePathsUpdater.MAIN_PROGRAM_PATH.getPath()) + "TeeHero.jar");
            this.fos.getChannel().transferFrom(this.rbc, 0L, Long.MAX_VALUE);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
