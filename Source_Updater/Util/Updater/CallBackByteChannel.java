// 
// Decompiled by Procyon v0.5.36
// 

package Util.Updater;

import java.nio.ByteBuffer;
import java.io.IOException;
import java.nio.channels.ReadableByteChannel;

public class CallBackByteChannel implements ReadableByteChannel
{
    private ProgressCallBack delegate;
    private long size;
    private ReadableByteChannel rbc;
    private long sizeRead;
    
    CallBackByteChannel(final ReadableByteChannel rbc, final long expectedSize, final ProgressCallBack delegate) {
        this.delegate = delegate;
        this.size = expectedSize;
        this.rbc = rbc;
    }
    
    @Override
    public void close() throws IOException {
        this.rbc.close();
    }
    
    @Override
    public boolean isOpen() {
        return this.rbc.isOpen();
    }
    
    public long getReadSoFar() {
        return this.sizeRead;
    }
    
    @Override
    public int read(final ByteBuffer bb) throws IOException {
        final int n;
        if ((n = this.rbc.read(bb)) > 0) {
            this.sizeRead += n;
            final double progress = (this.size > 0L) ? (this.sizeRead / (double)this.size * 100.0) : -1.0;
            this.delegate.callback(this, progress);
        }
        return n;
    }
}
