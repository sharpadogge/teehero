// 
// Decompiled by Procyon v0.5.36
// 

package Util.ServerConnector;

import java.io.PrintWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import Util.Enums.FilePathsUpdater;
import javafx.scene.control.Alert;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.net.ssl.HttpsURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class ServerConnector
{
    private final String secret = "o1A4Fjo0bFrS7lb3VF3zS4CfeS5VS9Jr";
    private final String versionCheck = "https://teehero.net/versionCheck/";
    private String downloadUrl;
    private String version;
    
    public ServerConnector() {
        this.downloadUrl = "";
        this.version = "";
    }
    
    private String sendPostRequest() throws Exception {
        final String httpsURL = "https://teehero.net/versionCheck/";
        final String query = "secret=" + URLEncoder.encode("o1A4Fjo0bFrS7lb3VF3zS4CfeS5VS9Jr", "UTF-8");
        final URL myurl = new URL(httpsURL);
        final HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);
        final DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();
        final DataInputStream input = new DataInputStream(con.getInputStream());
        final StringBuffer response = new StringBuffer();
        for (int c = input.read(); c != -1; c = input.read()) {
            response.append((char)c);
        }
        input.close();
        return response.toString();
    }
    
    public boolean isNewestVersion() {
        String response = "";
        try {
            response = this.sendPostRequest();
        }
        catch (Exception e) {
            final Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText((String)null);
            alert.setContentText("There was an error while communicating with the server. Please check your internet connection and try again.\nLatest version will be launched.");
            alert.showAndWait();
            return true;
        }
        final String fileVersion = this.getVersionFromFile();
        final String[] responses = response.split(";");
        if (responses[0].trim().equals(fileVersion)) {
            return true;
        }
        this.version = responses[0].trim();
        this.downloadUrl = responses[1].trim();
        return false;
    }
    
    private String getVersionFromFile() {
        final File versionFile = new File(String.valueOf(FilePathsUpdater.VERSION_FILE_PATH.getPath()) + "Version.txt");
        if (!versionFile.exists()) {
            return "0";
        }
        try {
            final BufferedReader br = new BufferedReader(new FileReader(versionFile));
            String line = null;
            line = br.readLine();
            if (line != null) {
                br.close();
                return line;
            }
            br.close();
            return "0";
        }
        catch (IOException e) {
            e.printStackTrace();
            return "0";
        }
    }
    
    public String getDownloadUrl() {
        return this.downloadUrl;
    }
    
    public void writeNewVersionToVersionFile() {
        final File versionFile = new File(String.valueOf(FilePathsUpdater.VERSION_FILE_PATH.getPath()) + "Version.txt");
        if (!versionFile.exists()) {
            try {
                versionFile.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            final PrintWriter out = new PrintWriter(versionFile);
            out.println(this.version);
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
