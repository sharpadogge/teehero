// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum FilePathsUpdater
{
    IMG_PATH("IMG_PATH", 0, "TeeHero.app/Contents/Resources/src/util/img/", "/Resources/src/util/img/"), 
    STYLESHEET_PATH("STYLESHEET_PATH", 1, "TeeHero.app/Contents/Resources/src/util/stylesheets/", "/Resources/src/util/stylesheets/"), 
    OLD_VERISON_PATH("OLD_VERISON_PATH", 2, "TeeHero.app/Contents/Java/Old_Version/", "/Java/Old_Version/"), 
    CONFIG_PATH("CONFIG_PATH", 3, "TeeHero.app/Contents/Resources/src/cfg/", "/Resources/src/cfg/"), 
    VERSION_FILE_PATH("VERSION_FILE_PATH", 4, "TeeHero.app/Contents/Java/", "/Java/"), 
    DRIVER_PATH("DRIVER_PATH", 5, "TeeHerp.app/Contents/Resources/driver/", "/Resources/driver/"), 
    MAIN_PROGRAM_PATH("MAIN_PROGRAM_PATH", 6, "TeeHero.app/Contents/Java/", "/Java/");
    
    private String path_win;
    private String path_mac;
    
    private FilePathsUpdater(final String name, final int ordinal, final String path_win, final String path_mac) {
        this.path_win = path_win;
        this.path_mac = path_mac;
    }
    
    public String getPath() {
        final String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("mac")) {
            return String.valueOf(System.getProperty("user.dir")) + this.path_mac;
        }
        return this.path_win;
    }
}
