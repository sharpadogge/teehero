// 
// Decompiled by Procyon v0.5.36
// 

package Util.Drivers;

import org.openqa.selenium.support.ui.FluentWait;
import java.util.function.Function;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.concurrent.TimeUnit;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import java.io.File;
import Util.Enums.FilePaths;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;

public class Driver
{
    public WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor js;
    private String url;
    ChromeOptions options;
    
    public Driver(final String url, final int waitTime) {
        this.options = new ChromeOptions();
        this.url = url;
        final String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("mac")) {
            new File(String.valueOf(FilePaths.DRIVER_PATH.getPath()) + "chromedriver").setExecutable(true);
            System.setProperty("webdriver.chrome.driver", String.valueOf(FilePaths.DRIVER_PATH.getPath()) + "chromedriver");
        }
        else {
            System.setProperty("webdriver.chrome.driver", String.valueOf(FilePaths.DRIVER_PATH.getPath()) + "chromedriver.exe");
        }
        this.options.setCapability("unexpectedAlertBehaviour", UnexpectedAlertBehaviour.ACCEPT);
        this.driver = new ChromeDriver(this.options);
        this.wait = new WebDriverWait(this.driver, waitTime);
        this.js = (JavascriptExecutor)this.driver;
    }
    
    public WebDriver getDriver() {
        return this.driver;
    }
    
    public WebDriverWait getWait() {
        return this.wait;
    }
    
    public void startHeadless() {
        final Set<Cookie> allCookies = this.driver.manage().getCookies();
        this.driver.quit();
        this.options.setHeadless(true);
        this.driver = new ChromeDriver(this.options);
        this.wait = new WebDriverWait(this.driver, 20L);
        this.js = (JavascriptExecutor)this.driver;
        this.openUrl();
        for (final Cookie cookie : allCookies) {
            this.driver.manage().addCookie(cookie);
        }
        this.driver.navigate().to(this.url);
        this.driverWait(10);
    }
    
    public void closeSession() {
        this.driver.quit();
    }
    
    public JavascriptExecutor getJS() {
        return this.js;
    }
    
    public void openUrl() {
        this.driver.get(this.url);
    }
    
    public void openUrl(final String url) {
        this.driver.get(url);
    }
    
    public String getUrl() {
        return this.url;
    }
    
    public String getCurrentUrl() {
        return this.driver.getCurrentUrl();
    }
    
    public WebElement find(final By identifier, final int number) {
        return this.findAll(identifier).get(number);
    }
    
    public List<WebElement> findAll(final By identifier) {
        return this.driver.findElements(identifier);
    }
    
    public void click(final String identifier) {
        this.getJS().executeScript("document." + identifier + ".click();", new Object[0]);
    }
    
    public void clickVue(final String identifier) {
        this.getJS().executeScript("document." + identifier + ".__vue__.onClick();", new Object[0]);
    }
    
    public void clickAll(final String identifier) {
        this.getJS().executeScript("var x = document." + identifier + ";" + "for (var i = 0; i < x.length; i++){x[i].click();}", new Object[0]);
    }
    
    public void clickLast(final String identifier) {
        this.getJS().executeScript("document." + identifier + "[" + "document." + identifier + ".length-1].click();", new Object[0]);
    }
    
    public void sendKeys(final String identifier, String value) {
        final String s = value.replace("'", "\\'");
        value = s.replace("\"", "\\\"");
        this.waitAngular();
        this.createEvent(identifier, "focus");
        this.waitAngular();
        this.getJS().executeScript("document." + identifier + ".value='" + value + "';", new Object[0]);
        this.waitAngular();
        this.createEvent(identifier, "input");
        this.waitAngular();
        this.createEvent(identifier, "change");
        this.waitAngular();
    }
    
    public void sendKeysWithoutEvents(final String identifier, String value) {
        final String s = value.replace("'", "\\'");
        value = s.replace("\"", "\\\"");
        this.getJS().executeScript("document." + identifier + ".value='" + value + "';", new Object[0]);
    }
    
    public int countElements(final String identifier) {
        return Integer.parseInt(((JavascriptExecutor)this.driver).executeScript("return document." + identifier + ".length;", new Object[0]).toString());
    }
    
    public String getText(final String identifier) {
        return ((JavascriptExecutor)this.driver).executeScript("return document." + identifier + ".text;", new Object[0]).toString();
    }
    
    public String getTextContent(final String identifier) {
        return ((JavascriptExecutor)this.driver).executeScript("return document." + identifier + ".textContent;", new Object[0]).toString();
    }
    
    public String getValue(final String identifier) {
        return ((JavascriptExecutor)this.driver).executeScript("return document." + identifier + ".value;", new Object[0]).toString();
    }
    
    public String getStyleDisplay(final String identifier) {
        return ((JavascriptExecutor)this.driver).executeScript("return document." + identifier + ".style.display;", new Object[0]).toString();
    }
    
    public String getInputText(final String identifier) {
        return ((JavascriptExecutor)this.driver).executeScript("if(document." + identifier + "._value != null){return document." + identifier + "._value;}else{return '';}", new Object[0]).toString();
    }
    
    public String getAttribute(final String identifier, final String attribute) {
        return (String)((JavascriptExecutor)this.driver).executeScript("return document." + identifier + "." + attribute + ";", new Object[0]);
    }
    
    public String returnAttribute(final String identifier, final String attribute) {
        return ((JavascriptExecutor)this.driver).executeScript("return document." + identifier + ".getAttribute('" + attribute + "');", new Object[0]).toString();
    }
    
    public long getLength(final String identifier) {
        return (long)((JavascriptExecutor)this.driver).executeScript("return document." + identifier + ".length;", new Object[0]);
    }
    
    public String getTitle(final String identifier) {
        return (String)((JavascriptExecutor)this.driver).executeScript("return document." + identifier + ".title;", new Object[0]);
    }
    
    public Boolean checkPresence(final String identifier) {
        return (boolean)((JavascriptExecutor)this.driver).executeScript("if (document.body.contains(document." + identifier + ")){" + "return true;}else{return false;};", new Object[0]);
    }
    
    public Boolean checkUndef(final String identifier) {
        return this.typeOf(identifier).equals("undefined");
    }
    
    public String typeOf(final String identifier) {
        return ((JavascriptExecutor)this.driver).executeScript("return typeof document." + identifier + ";", new Object[0]).toString();
    }
    
    public void setSelect(final String identifier, final String value) {
        this.getJS().executeScript("document." + identifier + ".selectedIndex=" + value + ";", new Object[0]);
    }
    
    public void setSelect(final String identifier) {
        this.getJS().executeScript("document." + identifier + ".selected='true';", new Object[0]);
    }
    
    public void setStyle(final String identifier, final String style) {
        this.getJS().executeScript("document." + identifier + ".style= '" + style + "';", new Object[0]);
    }
    
    public void replaceChild(final String parent, final String child) {
        this.getJS().executeScript("var parent = document." + parent + ";" + "var child = document." + child + ";" + "parent.removeChild(child);", new Object[0]);
    }
    
    public void removeChilds(final String identifier) {
        this.getJS().executeScript("var myNode = document." + identifier + ";" + "while (myNode.firstChild) {" + "    myNode.removeChild(myNode.firstChild);" + "}", new Object[0]);
    }
    
    public void appendChild(final String identifier, String text, final String element) {
        final String s = text.replace("'", "\\'");
        text = s.replace("\"", "\\\"");
        this.getJS().executeScript("var x = document." + identifier + ";" + "var node = document.createElement('" + element + "');" + "var textnode = document.createTextNode('" + text + "');" + "node.appendChild(textnode);" + "x.appendChild(node);", new Object[0]);
    }
    
    public void createEvent(final String identifier, final String event) {
        this.getJS().executeScript("if ('createEvent' in document) {var evt = document.createEvent('HTMLEvents');evt.initEvent('" + event + "', false, true);" + "document." + identifier + ".dispatchEvent(evt);" + "}", new Object[0]);
    }
    
    public void fire(final String identifier) {
        this.createEvent(identifier, "focus");
        this.createEvent(identifier, "change");
        this.createEvent(identifier, "blur");
    }
    
    public void driverWait(final int seconds) {
        this.driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
    }
    
    public void waitUntilElementVisible(final By identifier, final int number) {
        ((FluentWait<Object>)this.wait).until((Function<? super Object, Object>)ExpectedConditions.visibilityOf(this.find(identifier, number)));
    }
    
    public void waitUntilElementVisible(final By identifier) {
        this.waitUntilElementVisible(identifier, 0);
    }
    
    public void waitElementsNotVisible(final By identifier, final int number) {
        ((FluentWait<Object>)this.wait).until((Function<? super Object, Object>)ExpectedConditions.invisibilityOf(this.find(identifier, number)));
    }
    
    public void waitElementsNotVisible(final By identifier) {
        this.waitElementsNotVisible(identifier, 0);
    }
    
    public void waitUntilPresenceOfElement(final By identifier) {
        ((FluentWait<Object>)this.wait).until((Function<? super Object, Object>)ExpectedConditions.presenceOfAllElementsLocatedBy(identifier));
    }
    
    public void waitUntilElementClickable(final By identifier) {
        ((FluentWait<Object>)this.wait).until((Function<? super Object, Object>)ExpectedConditions.elementToBeClickable(identifier));
    }
    
    public void waitUntilElementClickable(final WebElement we) {
        ((FluentWait<Object>)this.wait).until((Function<? super Object, Object>)ExpectedConditions.elementToBeClickable(we));
    }
    
    public void waitAngular() {
        final long before = System.currentTimeMillis();
        boolean b;
        do {
            b = (boolean)this.js.executeScript("return (window.angular != null) && (angular.element(document).injector() != null) && (angular.element(document).injector().get('$http').pendingRequests.length === 0)", new Object[0]);
            if (System.currentTimeMillis() > before + 10000L) {
                try {
                    throw new Exception("waitAngular out of time");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } while (b);
    }
    
    public void waitReadyState() {
        while (!(boolean)this.js.executeScript("return (document.readyState === 'complete');", new Object[0])) {}
        this.driverWait(10);
    }
    
    public void waitAll() {
        this.driverWait(10);
        this.waitAngular();
        this.waitReadyState();
        try {
            Thread.sleep(50L);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
