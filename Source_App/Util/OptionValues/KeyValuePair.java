// 
// Decompiled by Procyon v0.5.36
// 

package Util.OptionValues;

public class KeyValuePair
{
    private String teeHeroPattern;
    private String otherPlatformPattern;
    
    public KeyValuePair(final String teeHeroPattern, final String otherPlatformPattern) {
        this.teeHeroPattern = teeHeroPattern;
        this.otherPlatformPattern = otherPlatformPattern;
    }
    
    public String getTeeHeroPattern() {
        return this.teeHeroPattern;
    }
    
    public String getOtherPlatformPattern() {
        return this.otherPlatformPattern;
    }
    
    @Override
    public String toString() {
        return String.valueOf(this.teeHeroPattern) + " | " + this.otherPlatformPattern;
    }
}
