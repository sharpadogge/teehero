// 
// Decompiled by Procyon v0.5.36
// 

package Util.OptionValues;

public class SpecialValueTrio
{
    private String teeHeroPattern;
    private String shirteePattern;
    private String shirteeColor;
    
    public SpecialValueTrio(final String teeHeroPattern, final String shirteePattern, final String shirteeColor) {
        this.teeHeroPattern = teeHeroPattern;
        this.shirteePattern = shirteePattern;
        this.shirteeColor = shirteeColor;
    }
    
    public String getTeeHeroPattern() {
        return this.teeHeroPattern;
    }
    
    public String getShirteePattern() {
        return this.shirteePattern;
    }
    
    public String getShirteeColor() {
        return this.shirteeColor;
    }
    
    @Override
    public String toString() {
        return String.valueOf(this.teeHeroPattern) + " | " + this.shirteePattern + "-" + this.shirteeColor;
    }
}
