// 
// Decompiled by Procyon v0.5.36
// 

package Util.ComboBoxRenderer;

import Util.OptionValues.KeyValuePair;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.DefaultListCellRenderer;

public class TwoColumnRenderer extends DefaultListCellRenderer
{
    private static final String START = "<html><table><tr><td width=265>";
    private static final String MIDDLE = "</td><td width=20>|</td><td width=265>";
    private static final String END = "</td></tr></table></html>";
    
    @Override
    public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        final JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        final KeyValuePair kvp = (KeyValuePair)value;
        if (kvp != null) {
            label.setText("<html><table><tr><td width=265>TeeHero:   " + kvp.getTeeHeroPattern() + "</td><td width=20>|</td><td width=265>" + "Platform:   " + kvp.getOtherPlatformPattern() + "</td></tr></table></html>");
        }
        return label;
    }
}
