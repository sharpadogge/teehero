// 
// Decompiled by Procyon v0.5.36
// 

package Util.Sessions;

import org.openqa.selenium.support.ui.FluentWait;
import Util.Encryptor.Encryptor;
import java.util.function.Function;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import Util.Enums.DesignAreaDefinition;
import java.util.Iterator;
import Util.Enums.OptionNamesInConfig;
import Util.Config.ConfigHandler;
import GUI.Pages.MainPage;
import Util.Designs.ShirteeDesign;
import java.util.List;
import Util.Analytics.Analyser;

public class ShirteeSession extends Session
{
    private Analyser analyser;
    private String areaName;
    private List<ShirteeDesign> designList;
    private MainPage mp;
    private ConfigHandler ch;
    private boolean autoLogin;
    private String username;
    private String password;
    
    public ShirteeSession(final String url, final List<ShirteeDesign> designList, final MainPage mp, final String areaName) {
        super(url, 45);
        this.username = "";
        this.password = "";
        this.mp = mp;
        this.areaName = areaName;
        this.designList = designList;
        this.ch = new ConfigHandler();
    }
    
    public void startUpload() {
        boolean autoLogin = false;
        this.analyser = new Analyser(this.mp.getUser());
        autoLogin = false;
        if (this.ch.readSetting(OptionNamesInConfig.TEEHERO_AUTOLOGIN.getOptionName()).toLowerCase().equals("true")) {
            autoLogin = this.isLoginDataProvided();
        }
        if (!autoLogin) {
            this.login();
        }
        if (this.ch.readSetting(OptionNamesInConfig.TEEHERO_HEADLESS.getOptionName()).toLowerCase().equals("true")) {
            this.d.startHeadless();
        }
        if (autoLogin) {
            this.autoLogin(this.username, this.password);
        }
        this.writeStartAnalytics(this.designList.size());
        for (final ShirteeDesign design : this.designList) {
            design.upload(this.d);
        }
        this.d.closeSession();
    }
    
    private boolean isLoginDataProvided() {
        if (this.areaName.equals(DesignAreaDefinition.SHIRTEE_AREA.getAreaName())) {
            this.username = this.ch.readSetting(OptionNamesInConfig.SHIRTEE_LOGIN_USER.getOptionName());
            this.password = this.ch.readSetting(OptionNamesInConfig.SHIRTEE_LOGIN_PASSWORD.getOptionName());
            return !this.username.isEmpty() && !this.password.isEmpty();
        }
        return false;
    }
    
    @Override
    protected void login() {
        this.d.openUrl();
        this.d.driverWait(30);
        final WebDriverWait wait = new WebDriverWait(this.d.getDriver(), 600L);
        ((FluentWait<Object>)wait).until((Function<? super Object, Object>)ExpectedConditions.presenceOfElementLocated(By.className("gomage-dashboard-index-index")));
    }
    
    @Override
    protected void autoLogin(final String username, String password) {
        this.d.openUrl();
        this.d.driverWait(30);
        final Encryptor encryptor = new Encryptor();
        password = encryptor.decryptData(password);
        this.d.waitAll();
        this.d.setStyle("getElementById('header-login-window')", "display: block;");
        this.d.waitAll();
        this.d.sendKeys("getElementById('header-login-form-email')", username);
        this.d.waitAll();
        this.d.sendKeys("getElementById('header-login-form-password')", password);
        this.d.waitAll();
        this.d.click("getElementsByClassName('youama-ajaxlogin-button')[0]");
    }
    
    private void writeStartAnalytics(final int numberOfDesigns) {
        new Thread(() -> {
            try {
                this.analyser.writeUploadReportToServer(numberOfDesigns, this.areaName);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
    
    @Deprecated
    public void autoLoginTest(final String username, final String password) {
        this.autoLogin(username, password);
    }
    
    @Deprecated
    public boolean isLoginDataProvidedTest() {
        return this.isLoginDataProvided();
    }
}
