// 
// Decompiled by Procyon v0.5.36
// 

package Util.Sessions;

import org.openqa.selenium.By;
import Util.Enums.DesignAreaDefinition;
import org.junit.Test;
import org.junit.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import Util.Enums.OptionNamesInConfig;
import Util.Config.ConfigHandler;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import java.util.Date;
import java.io.File;
import java.util.ArrayList;
import org.junit.ClassRule;
import Util.Tests.JavaFXThreadingRule;
import GUI.Pages.MainPage;
import GUI.Window.Window;
import Util.User.User;
import Util.Designs.SpreadshirtDesign;
import java.util.List;
import Util.Drivers.Driver;

public class SpreadshirtSessionTest
{
    static SpreadshirtSession session1;
    static SpreadshirtSession session2;
    static SpreadshirtSession loginSessionEU;
    static SpreadshirtSession loginSessionCOM;
    static Driver driver;
    static Driver dEU;
    static Driver dCOM;
    static String usernameEU;
    static String passwordEU;
    static String usernameCOM;
    static String passwordCOM;
    static List<SpreadshirtDesign> designList;
    static User user;
    static Window window;
    static MainPage mp;
    @ClassRule
    public static JavaFXThreadingRule javafxRule;
    
    static {
        SpreadshirtSessionTest.designList = new ArrayList<SpreadshirtDesign>();
        SpreadshirtSessionTest.javafxRule = new JavaFXThreadingRule();
    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        final File f1 = new File(String.valueOf(System.getProperty("user.dir")) + "/Test Files/Don't fart - Gym workout training muscle shirt.png");
        final String title1 = "Don't fart - Gym workout training" + (1 + (int)(Math.random() * 100.0));
        final String desc1 = "Description: Don't fart - Gym workout training muscle shirt";
        final List<String> kw1 = new ArrayList<String>();
        final String[] strs1 = { "Gym", "Sport", "Fitness", "Bodybuilding", "Pumping", "Iron", "Muscle Shirt", "Biceps", "Legday", "Hobby", "Present" };
        for (int i = 0; i < strs1.length; ++i) {
            kw1.add(strs1[i]);
        }
        final SpreadshirtDesign sd1 = new SpreadshirtDesign(f1, 0, title1, desc1, kw1, "Black", 8.5, null);
        SpreadshirtSessionTest.designList.add(sd1);
        SpreadshirtSessionTest.user = new User("TestUser", new Date());
        SpreadshirtSessionTest.window = new Window();
        SpreadshirtSessionTest.mp = new MainPage(new BorderPane(), SpreadshirtSessionTest.window, SpreadshirtSessionTest.user);
        final ConfigHandler ch = new ConfigHandler();
        SpreadshirtSessionTest.usernameEU = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_LOGIN_USER.getOptionName());
        SpreadshirtSessionTest.passwordEU = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_LOGIN_PASSWORD.getOptionName());
        SpreadshirtSessionTest.usernameCOM = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_LOGIN_USER.getOptionName());
        SpreadshirtSessionTest.passwordCOM = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_LOGIN_PASSWORD.getOptionName());
        SpreadshirtSessionTest.loginSessionEU = new SpreadshirtSession("https://partner.spreadshirt.de/designs", null, null, null);
        (SpreadshirtSessionTest.dEU = SpreadshirtSessionTest.loginSessionEU.getDriver()).startHeadless();
        SpreadshirtSessionTest.loginSessionCOM = new SpreadshirtSession("https://partner.spreadshirt.com/designs", null, null, null);
        (SpreadshirtSessionTest.dCOM = SpreadshirtSessionTest.loginSessionCOM.getDriver()).startHeadless();
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        delteAllDesigns(SpreadshirtSessionTest.driver);
        SpreadshirtSessionTest.driver.closeSession();
    }
    
    @Test
    public void checkAutoLogin() {
        SpreadshirtSessionTest.loginSessionEU.autoLoginTest(SpreadshirtSessionTest.usernameEU, SpreadshirtSessionTest.passwordEU);
        SpreadshirtSessionTest.dEU.openUrl();
        SpreadshirtSessionTest.dEU.driverWait(10);
        String url = SpreadshirtSessionTest.dEU.getCurrentUrl();
        Assert.assertEquals("https://partner.spreadshirt.de/designs", url);
        SpreadshirtSessionTest.dEU.closeSession();
        SpreadshirtSessionTest.loginSessionCOM.autoLoginTest(SpreadshirtSessionTest.usernameCOM, SpreadshirtSessionTest.passwordCOM);
        SpreadshirtSessionTest.dCOM.openUrl();
        SpreadshirtSessionTest.dCOM.driverWait(10);
        url = SpreadshirtSessionTest.dCOM.getCurrentUrl();
        Assert.assertEquals("https://partner.spreadshirt.com/designs", url);
        SpreadshirtSessionTest.dCOM.closeSession();
    }
    
    @Test
    public void checkUploadDesignsEU() {
        (SpreadshirtSessionTest.session1 = new SpreadshirtSession("https://partner.spreadshirt.de/designs", SpreadshirtSessionTest.designList, SpreadshirtSessionTest.mp, DesignAreaDefinition.SPREADSHIRT_EU_AREA.getAreaName())).startUpload();
        final SpreadshirtSession controlEU = new SpreadshirtSession(SpreadshirtSessionTest.session1.url, null, null, null);
        (SpreadshirtSessionTest.driver = controlEU.getDriver()).startHeadless();
        controlEU.autoLogin(SpreadshirtSessionTest.usernameEU, SpreadshirtSessionTest.passwordEU);
        SpreadshirtSessionTest.driver.openUrl();
        this.checkSpreadListing(SpreadshirtSessionTest.designList.get(0).title, "8,50 \u20ac", SpreadshirtSessionTest.designList.get(0).description, SpreadshirtSessionTest.designList.get(0).tags.size(), "26 Produkte", SpreadshirtSessionTest.driver);
    }
    
    @Test
    public void checkUploadDesignsCOM() {
        (SpreadshirtSessionTest.session2 = new SpreadshirtSession("https://partner.spreadshirt.com/designs", SpreadshirtSessionTest.designList, SpreadshirtSessionTest.mp, DesignAreaDefinition.SPREADSHIRT_COM_AREA.getAreaName())).startUpload();
        final SpreadshirtSession controlCOM = new SpreadshirtSession(SpreadshirtSessionTest.session2.url, null, null, null);
        (SpreadshirtSessionTest.driver = controlCOM.getDriver()).startHeadless();
        controlCOM.autoLogin(SpreadshirtSessionTest.usernameCOM, SpreadshirtSessionTest.passwordCOM);
        SpreadshirtSessionTest.driver.openUrl();
        this.checkSpreadListing(SpreadshirtSessionTest.designList.get(0).title, "$8.50", SpreadshirtSessionTest.designList.get(0).description, SpreadshirtSessionTest.designList.get(0).tags.size(), "65 Products", SpreadshirtSessionTest.driver);
    }
    
    @Test
    public void checkIsLoginDataProvided() {
        SpreadshirtSession s = new SpreadshirtSession(null, null, null, DesignAreaDefinition.SPREADSHIRT_EU_AREA.getAreaName());
        Assert.assertTrue(s.isLoginDataProvidedTest());
        s.getDriver().closeSession();
        s = new SpreadshirtSession(null, null, null, DesignAreaDefinition.SPREADSHIRT_COM_AREA.getAreaName());
        Assert.assertTrue(s.isLoginDataProvidedTest());
        s.getDriver().closeSession();
        s = new SpreadshirtSession(null, null, null, "Bla");
        Assert.assertFalse(s.isLoginDataProvidedTest());
        s.getDriver().closeSession();
    }
    
    public void checkSpreadListing(final String title, final String price, final String desc, final int tagsSize, final String numberOfProducts, final Driver d) {
        d.driverWait(10);
        d.waitUntilElementVisible(By.xpath("//*[@class='idea-preview-image preview-image design-preview']"));
        d.click("getElementsByClassName('idea-preview-image preview-image design-preview')[0]");
        d.driverWait(10);
        d.waitAll();
        try {
            Thread.sleep(1000L);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(title, d.getTextContent("getElementsByTagName('strong')[0]"));
        Assert.assertEquals(price, d.getTextContent("getElementsByTagName('strong')[1]"));
        Assert.assertEquals(desc, d.getTextContent("getElementsByTagName('p')[0]"));
        final long l = d.getLength("getElementsByClassName('tag-list')[0].children");
        Assert.assertEquals(tagsSize, l);
        for (int i = 0; i < tagsSize; ++i) {
            Assert.assertEquals(SpreadshirtSessionTest.designList.get(0).tags.get(i), d.getTextContent("getElementsByClassName('tag-list')[0].children[" + i + "]"));
        }
        Assert.assertEquals(numberOfProducts, d.getTextContent("getElementsByTagName('strong')[3]"));
    }
    
    private static void delteAllDesigns(final Driver d) {
        d.openUrl();
        d.driverWait(10);
        d.waitAll();
        d.waitUntilElementVisible(By.id("upload-btn"));
        for (long l = d.getLength("getElementsByClassName('card design-tile')"); l > 0L; l = d.getLength("getElementsByClassName('card design-tile')")) {
            d.waitUntilElementVisible(By.xpath("//*[@class='link-green select-link']"));
            d.click("getElementsByClassName('link-green select-link')[0]");
            d.waitUntilElementVisible(By.id("design-delete-button"));
            d.click("getElementById('design-list-delete-nav-item').children[0]");
            d.waitUntilElementVisible(By.xpath("//*[@class='btn-progress btn btn-lg btn-danger btn-progress IDLE null']"));
            d.clickVue("getElementsByClassName('btn-progress btn btn-lg btn-danger btn-progress IDLE null')[0]");
            d.waitAll();
            d.driverWait(10);
            d.waitAll();
        }
    }
}
