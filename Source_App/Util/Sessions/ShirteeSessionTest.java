// 
// Decompiled by Procyon v0.5.36
// 

package Util.Sessions;

import Util.Enums.DesignAreaDefinition;
import org.junit.Test;
import org.junit.Assert;
import org.junit.BeforeClass;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import java.util.Date;
import Util.Enums.OptionNamesInConfig;
import Util.Config.ConfigHandler;
import Util.Enums.PlatformColors;
import java.io.File;
import java.util.ArrayList;
import org.junit.ClassRule;
import Util.Tests.JavaFXThreadingRule;
import GUI.Pages.MainPage;
import GUI.Window.Window;
import Util.User.User;
import java.util.List;
import Util.Designs.ShirteeDesign;
import Util.Drivers.Driver;

public class ShirteeSessionTest
{
    static ShirteeSession s;
    static Driver d;
    static ShirteeDesign sd1;
    static ShirteeDesign sd2;
    static String username;
    static String password;
    static List<ShirteeDesign> designList;
    static User user;
    static Window window;
    static MainPage mp;
    @ClassRule
    public static JavaFXThreadingRule javafxRule;
    
    static {
        ShirteeSessionTest.designList = new ArrayList<ShirteeDesign>();
        ShirteeSessionTest.javafxRule = new JavaFXThreadingRule();
    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        final File f1 = new File(String.valueOf(System.getProperty("user.dir")) + "/Test Files/Don't fart - Gym workout training muscle shirt.png");
        final String title1 = "Don't fart - Gym workout training" + (1 + (int)(Math.random() * 100.0));
        final String desc1 = "Description: Don't fart - Gym workout training muscle shirt";
        final List<String> kw1 = new ArrayList<String>();
        final String[] strs1 = { "Gym", "Sport", "Fitness" };
        for (int i = 0; i < strs1.length; ++i) {
            kw1.add(strs1[i]);
        }
        final String id1 = "1374297";
        final String category1 = "Fitness";
        final String marketplace1 = "DE";
        final String[] standardColor1 = PlatformColors.SHIRTEE.getPlatformColors()[2];
        ShirteeSessionTest.sd1 = new ShirteeDesign(f1, 0, title1, desc1, kw1, id1, category1, marketplace1, "null", standardColor1);
        final File f2 = new File(String.valueOf(System.getProperty("user.dir")) + "/Test Files/Stop staring on my cock! Funny slogan.png");
        final String title2 = "Stop staring on my cock!" + (1 + (int)(Math.random() * 100.0));
        final String desc2 = "Description: Stop staring on my cock! Funny slogan";
        final List<String> kw2 = new ArrayList<String>();
        final String[] strs2 = { "Funny", "Pet", "Stop" };
        for (int j = 0; j < strs2.length; ++j) {
            kw1.add(strs2[j]);
        }
        final String id2 = "1637779";
        final String category2 = "Spr\u00fcche & Lustiges";
        final String marketplace2 = "ENG";
        final String[] standardColor2 = PlatformColors.SHIRTEE.getPlatformColors()[3];
        ShirteeSessionTest.sd2 = new ShirteeDesign(f2, 0, title2, desc2, kw2, id2, category2, marketplace2, "null", standardColor2);
        ShirteeSessionTest.designList.add(ShirteeSessionTest.sd1);
        ShirteeSessionTest.designList.add(ShirteeSessionTest.sd2);
        final ConfigHandler ch = new ConfigHandler();
        ShirteeSessionTest.username = ch.readSetting(OptionNamesInConfig.SHIRTEE_LOGIN_USER.getOptionName());
        ShirteeSessionTest.password = ch.readSetting(OptionNamesInConfig.SHIRTEE_LOGIN_PASSWORD.getOptionName());
        ShirteeSessionTest.user = new User("TestUser", new Date());
        ShirteeSessionTest.window = new Window();
        ShirteeSessionTest.mp = new MainPage(new BorderPane(), ShirteeSessionTest.window, ShirteeSessionTest.user);
    }
    
    @Test
    public void loginTest() {
        ShirteeSessionTest.s = new ShirteeSession("https://www.shirtee.com/de/", ShirteeSessionTest.designList, null, null);
        (ShirteeSessionTest.d = ShirteeSessionTest.s.getDriver()).startHeadless();
        ShirteeSessionTest.s.autoLoginTest(ShirteeSessionTest.username, ShirteeSessionTest.password);
        ShirteeSessionTest.d.driverWait(10);
        final String url = ShirteeSessionTest.d.getCurrentUrl();
        Assert.assertEquals("https://www.shirtee.com/de/dashboard/index/index/", url);
        ShirteeSessionTest.d.closeSession();
    }
    
    @Test
    public void uploadTest() {
        ShirteeSessionTest.s = new ShirteeSession("https://www.shirtee.com/de/", ShirteeSessionTest.designList, ShirteeSessionTest.mp, DesignAreaDefinition.SHIRTEE_AREA.getAreaName());
        ShirteeSessionTest.s.getDriver().startHeadless();
        ShirteeSessionTest.s.startUpload();
        ShirteeSessionTest.s = new ShirteeSession("https://www.shirtee.com/de/", null, null, null);
        (ShirteeSessionTest.d = ShirteeSessionTest.s.getDriver()).startHeadless();
        ShirteeSessionTest.s.autoLogin(ShirteeSessionTest.username, ShirteeSessionTest.password);
        ShirteeSessionTest.d.openUrl("https://www.shirtee.com/de/dashboard/index/index/?approved=2");
        ShirteeSessionTest.d.driverWait(10);
        String s = ShirteeSessionTest.d.getTextContent("getElementsByClassName('amount amount--has-pages')[0]").trim();
        s = s.substring(s.lastIndexOf(32, s.length()));
        final int i = Integer.parseInt(s.trim()) / 10 + 1;
        ShirteeSessionTest.d.openUrl("https://www.shirtee.com/de/dashboard/index/index/?approved=2&p=" + i);
        final long l = ShirteeSessionTest.d.getLength("getElementsByClassName('campaign-title')");
        final String titleLastUpload = ShirteeSessionTest.d.getTextContent("getElementsByClassName('campaign-title')[" + (l - 3L) + "]").trim();
        String titleFirstUpload;
        if (l < 6L) {
            ShirteeSessionTest.d.openUrl("https://www.shirtee.com/de/dashboard/index/index/?approved=2&p=" + (i - 1));
            titleFirstUpload = ShirteeSessionTest.d.getTextContent("getElementsByClassName('campaign-title')[27]").trim();
        }
        else {
            titleFirstUpload = ShirteeSessionTest.d.getTextContent("getElementsByClassName('campaign-title')[" + (l - 6L) + "]").trim();
        }
        Assert.assertEquals(ShirteeSessionTest.sd1.title, titleFirstUpload);
        Assert.assertEquals(ShirteeSessionTest.sd2.title, titleLastUpload);
        ShirteeSessionTest.d.closeSession();
    }
    
    @Test
    public void checkIsLoginDataProvided() {
        ShirteeSession s = new ShirteeSession(null, null, null, DesignAreaDefinition.SHIRTEE_AREA.getAreaName());
        Assert.assertTrue(s.isLoginDataProvidedTest());
        s.getDriver().closeSession();
        s = new ShirteeSession(null, null, null, "Bla");
        Assert.assertFalse(s.isLoginDataProvidedTest());
        s.getDriver().closeSession();
    }
}
