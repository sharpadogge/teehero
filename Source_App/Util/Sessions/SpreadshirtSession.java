// 
// Decompiled by Procyon v0.5.36
// 

package Util.Sessions;

import org.openqa.selenium.support.ui.FluentWait;
import java.util.function.Function;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Util.Encryptor.Encryptor;
import Util.Enums.DesignAreaDefinition;
import java.util.Iterator;
import Util.Enums.OptionNamesInConfig;
import GUI.Pages.MainPage;
import Util.Designs.SpreadshirtDesign;
import java.util.List;
import Util.Config.ConfigHandler;
import Util.Analytics.Analyser;

public class SpreadshirtSession extends Session
{
    private Analyser analyser;
    private String areaName;
    private ConfigHandler ch;
    private boolean autoLogin;
    private String username;
    private String password;
    private List<SpreadshirtDesign> designList;
    private MainPage mp;
    
    public SpreadshirtSession(final String url, final List<SpreadshirtDesign> designList, final MainPage mp, final String areaName) {
        super(url, mp, 30);
        this.username = "";
        this.password = "";
        this.ch = new ConfigHandler();
        this.areaName = areaName;
        this.designList = designList;
        this.mp = mp;
    }
    
    public void startUpload() {
        this.d.openUrl();
        this.analyser = new Analyser(this.mp.getUser());
        if (this.ch.readSetting(OptionNamesInConfig.TEEHERO_AUTOLOGIN.getOptionName()).toLowerCase().equals("true")) {
            if (this.isLoginDataProvided()) {
                this.autoLogin = true;
            }
            else {
                this.autoLogin = false;
            }
        }
        if (!this.autoLogin) {
            this.login();
        }
        if (this.ch.readSetting(OptionNamesInConfig.TEEHERO_HEADLESS.getOptionName()).toLowerCase().equals("true")) {
            this.d.startHeadless();
        }
        if (this.autoLogin) {
            this.autoLogin(this.username, this.password);
        }
        this.d.openUrl();
        this.d.driverWait(20);
        this.writeStartAnalytics(this.designList.size());
        for (final SpreadshirtDesign design : this.designList) {
            design.upload(this.d);
            this.d.openUrl();
        }
        this.d.closeSession();
    }
    
    private boolean isLoginDataProvided() {
        if (this.areaName.equals(DesignAreaDefinition.SPREADSHIRT_EU_AREA.getAreaName())) {
            this.username = this.ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_LOGIN_USER.getOptionName());
            this.password = this.ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_LOGIN_PASSWORD.getOptionName());
        }
        else {
            if (!this.areaName.equals(DesignAreaDefinition.SPREADSHIRT_COM_AREA.getAreaName())) {
                return false;
            }
            this.username = this.ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_LOGIN_USER.getOptionName());
            this.password = this.ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_LOGIN_PASSWORD.getOptionName());
        }
        return !this.username.isEmpty() && !this.password.isEmpty();
    }
    
    @Override
    protected void autoLogin(final String username, String password) {
        final String uploadCountry = this.url.substring(this.url.lastIndexOf(46) + 1, this.url.lastIndexOf(47));
        final Encryptor encryptor = new Encryptor();
        password = encryptor.decryptData(password);
        try {
            this.d.openUrl("https://accounts.spreadshirt." + uploadCountry + "/login?context=partner&lang=" + uploadCountry);
            this.d.waitAll();
            this.d.sendKeys("getElementById('username')", username);
            this.d.waitAll();
            this.d.sendKeys("getElementById('password')", password);
            this.d.waitAll();
            this.d.click("getElementById('login-submit')");
            this.d.waitAll();
        }
        catch (Exception e) {
            this.autoLogin = false;
            e.printStackTrace();
            System.out.println("Error when logging in");
            return;
        }
        try {
            Thread.sleep(1000L);
            this.d.click("getElementById('login-submit')");
            this.d.waitAll();
            Thread.sleep(1000L);
        }
        catch (Exception e) {
            this.autoLogin = false;
            e.printStackTrace();
        }
    }
    
    @Override
    protected void login() {
        this.d.openUrl();
        this.d.driverWait(30);
        final WebDriverWait wait = new WebDriverWait(this.d.getDriver(), 600L);
        ((FluentWait<Object>)wait).until((Function<? super Object, Object>)ExpectedConditions.urlContains("designs"));
    }
    
    private void writeStartAnalytics(final int numberOfDesigns) {
        new Thread(() -> {
            try {
                this.analyser.writeUploadReportToServer(numberOfDesigns, this.areaName);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
    
    @Deprecated
    public void autoLoginTest(final String username, final String password) {
        this.autoLogin(username, password);
    }
    
    @Deprecated
    public boolean isLoginDataProvidedTest() {
        return this.isLoginDataProvided();
    }
}
