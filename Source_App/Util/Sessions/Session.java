// 
// Decompiled by Procyon v0.5.36
// 

package Util.Sessions;

import Util.Drivers.Driver;
import GUI.Pages.MainPage;

public abstract class Session
{
    private MainPage mp;
    String url;
    Driver d;
    
    Session(final String url, final int waitTime) {
        this.url = url;
        System.out.println(url);
        this.d = new Driver(url, waitTime);
    }
    
    Session(final String url, final MainPage mp, final int waitTime) {
        this.url = url;
        this.mp = mp;
        this.d = new Driver(url, waitTime);
    }
    
    protected void updateProgress(final int val) {
        this.mp.getProgressBar().setProgress((double)val);
    }
    
    public String getURL() {
        return this.url;
    }
    
    protected abstract void login();
    
    protected abstract void autoLogin(final String p0, final String p1);
    
    public Driver getDriver() {
        return this.d;
    }
}
