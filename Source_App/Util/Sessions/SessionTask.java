// 
// Decompiled by Procyon v0.5.36
// 

package Util.Sessions;

import java.util.Iterator;
import GUI.DesignFrames.ShirteeDesignFrame;
import Util.Designs.ShirteeDesign;
import GUI.DesignFrames.SpreadshirtDesignFrame;
import Util.Designs.SpreadshirtDesign;
import java.util.ArrayList;
import Util.Config.ConfigHandler;
import GUI.Pages.MainPage;
import GUI.DesignFrames.DesignFrame;
import java.util.List;
import Util.Enums.DesignAreaDefinition;
import javafx.concurrent.Task;

public class SessionTask extends Task<Void>
{
    private DesignAreaDefinition area;
    private List<? extends DesignFrame> list;
    private MainPage mp;
    
    public SessionTask(final DesignAreaDefinition area, final MainPage mp, final List<? extends DesignFrame> list) {
        this.area = area;
        this.mp = mp;
        this.list = list;
    }
    
    protected Void call() throws Exception {
        final ConfigHandler cm = new ConfigHandler();
        switch (this.area) {
            case SPREADSHIRT_EU_AREA: {
                final List<SpreadshirtDesignFrame> sdf = (List<SpreadshirtDesignFrame>)this.list;
                final List<SpreadshirtDesign> sd = new ArrayList<SpreadshirtDesign>();
                for (final SpreadshirtDesignFrame f : sdf) {
                    sd.add(f.getSpreadshirtDesign());
                }
                new SpreadshirtSession(cm.readSetting("SPREADSHIRT_EU_URL"), sd, this.mp, this.area.getAreaName()).startUpload();
                break;
            }
            case SPREADSHIRT_COM_AREA: {
                final List<SpreadshirtDesignFrame> sdf = (List<SpreadshirtDesignFrame>)this.list;
                final List<SpreadshirtDesign> sd = new ArrayList<SpreadshirtDesign>();
                for (final SpreadshirtDesignFrame f : sdf) {
                    sd.add(f.getSpreadshirtDesign());
                }
                new SpreadshirtSession(cm.readSetting("SPREADSHIRT_COM_URL"), sd, this.mp, this.area.getAreaName()).startUpload();
                break;
            }
            case SHIRTEE_AREA: {
                final List<ShirteeDesignFrame> sdf2 = (List<ShirteeDesignFrame>)this.list;
                final List<ShirteeDesign> sd2 = new ArrayList<ShirteeDesign>();
                for (final ShirteeDesignFrame f2 : sdf2) {
                    sd2.add(f2.getShirteeDesign());
                }
                new ShirteeSession(cm.readSetting("SHIRTEE_URL"), sd2, this.mp, this.area.getAreaName()).startUpload();
                break;
            }
        }
        return null;
    }
}
