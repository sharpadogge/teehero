// 
// Decompiled by Procyon v0.5.36
// 

package Util.FilePicker;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert;
import java.util.Iterator;
import javafx.stage.Window;
import java.util.List;
import javafx.stage.FileChooser;
import java.io.File;
import java.util.ArrayList;

public class FilePicker
{
    private int unvalidFiles;
    private int validFiles;
    private boolean showCancelAllOption;
    private boolean wasCanceled;
    private String[] supportedImageFormats;
    private ArrayList<File> selectedFileList;
    private FileChooser fileChooser;
    
    public FilePicker(final String[] supportedImageFormats) {
        this.unvalidFiles = 0;
        this.validFiles = 0;
        this.showCancelAllOption = false;
        this.wasCanceled = false;
        this.fileChooser = new FileChooser();
        this.supportedImageFormats = supportedImageFormats;
        this.fileChooser.setTitle("Choose your designs");
        this.fileChooser.getExtensionFilters().add((Object)this.createFileFilter(supportedImageFormats));
        this.selectedFileList = new ArrayList<File>();
    }
    
    private FileChooser.ExtensionFilter createFileFilter(final String[] supportedFormats) {
        final FileChooser.ExtensionFilter ff = new FileChooser.ExtensionFilter("Supported Formats", supportedFormats);
        return ff;
    }
    
    private List<File> showFilePicker() {
        return (List<File>)this.fileChooser.showOpenMultipleDialog((Window)null);
    }
    
    private void checkSelectedFileForFolders(final List<File> selection) {
        for (final File f : selection) {
            this.getSelectedFile(f);
        }
    }
    
    private void getSelectedFile(final File selectedFile) {
        if (this.isAcceptableImageFormat(selectedFile)) {
            this.selectedFileList.add(selectedFile);
            ++this.validFiles;
        }
        else {
            ++this.unvalidFiles;
        }
    }
    
    private boolean isAcceptableImageFormat(final File file) {
        String[] supportedImageFormats;
        for (int length = (supportedImageFormats = this.supportedImageFormats).length, i = 0; i < length; ++i) {
            final String format = supportedImageFormats[i];
            if (file.getName().toLowerCase().endsWith(format.toLowerCase().substring(1))) {
                return true;
            }
        }
        return false;
    }
    
    public ArrayList<File> getUploadableFilesFromSelectedDirectory() {
        final List<File> files = this.showFilePicker();
        if (files == null || files.isEmpty()) {
            return null;
        }
        this.checkSelectedFileForFolders(files);
        this.informUserAboutUploadResult();
        if (this.validFiles < 30) {
            return this.selectedFileList;
        }
        final Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.getButtonTypes().clear();
        alert.setHeaderText((String)null);
        alert.setContentText("You are about to load " + this.validFiles + " files.\n\nDo you want to start the loading process?");
        alert.setTitle("Many Items");
        final ButtonType btnYes = new ButtonType("Yes");
        final ButtonType btnNo = new ButtonType("No");
        alert.getButtonTypes().add((Object)btnYes);
        alert.getButtonTypes().add((Object)btnNo);
        alert.showAndWait();
        final ButtonType answer = (ButtonType)alert.getResult();
        if (answer == btnYes) {
            return this.selectedFileList;
        }
        return null;
    }
    
    private void informUserAboutUploadResult() {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(String.valueOf(this.wasCanceled ? "Upload stoped by user" : new StringBuilder("Found ").append(this.unvalidFiles + this.validFiles).append(" file(s)").toString()) + "\n\nWe are unable to load: " + this.unvalidFiles + " File(s) because of not supported formats.\n\nWe are able to load: " + this.validFiles + " file(s)\n\nPress 'OK' to start the upload");
        alert.setTitle("Load");
        alert.setHeaderText((String)null);
        alert.showAndWait();
    }
}
