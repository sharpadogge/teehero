// 
// Decompiled by Procyon v0.5.36
// 

package Util.ServerRequests;

import javafx.scene.control.Alert;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.net.ssl.HttpsURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class LoginRequest
{
    private final String secret = "o1A4Fjo0bFrS7lb3VF3zS4CfeS5VS9Jr";
    private final String loginApi = "https://teehero.net/test/";
    private String email;
    private String password;
    
    public LoginRequest(final String email, final String password) {
        this.email = email;
        this.password = password;
    }
    
    private String sendPostRequest() throws Exception {
        final String httpsURL = "https://teehero.net/test/";
        String query = "email=" + URLEncoder.encode(this.email, "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "password=" + URLEncoder.encode(this.password, "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "secret=" + URLEncoder.encode("o1A4Fjo0bFrS7lb3VF3zS4CfeS5VS9Jr", "UTF-8");
        final URL myurl = new URL(httpsURL);
        final HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);
        final DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();
        final DataInputStream input = new DataInputStream(con.getInputStream());
        final StringBuffer response = new StringBuffer();
        for (int c = input.read(); c != -1; c = input.read()) {
            response.append((char)c);
        }
        input.close();
        this.password = "";
        this.email = "";
        return response.toString();
    }
    
    public boolean isLoginValid() {
        String response = "";
        try {
            response = this.sendPostRequest();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (response.substring(14, 18).toUpperCase().equals("TRUE")) {
            return true;
        }
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Login failed");
        alert.setContentText("Username or password is incorrect or your access is exspired");
        alert.setHeaderText((String)null);
        alert.show();
        return false;
    }
}
