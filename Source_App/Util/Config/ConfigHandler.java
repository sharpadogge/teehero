// 
// Decompiled by Procyon v0.5.36
// 

package Util.Config;

import java.io.File;
import java.util.Iterator;
import java.io.OutputStream;
import GUI.OptionFrame.OptionFrame;
import java.io.FileOutputStream;
import GUI.OptionArea.OptionsArea;
import java.util.Map;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileInputStream;
import Util.Enums.FilePaths;
import java.util.Properties;

public class ConfigHandler
{
    public String readSetting(final String setting) {
        final Properties prop = new Properties();
        InputStream configFile = null;
        if (!this.doesConfigExist()) {
            this.createConfigFile();
        }
        try {
            configFile = new FileInputStream(String.valueOf(FilePaths.CONFIG_PATH.getPath()) + "TeeHero.cfg");
            prop.load(configFile);
            String result = prop.getProperty(setting);
            if (result == null) {
                result = "";
            }
            return result;
        }
        catch (IOException ex) {
            ex.printStackTrace();
            if (configFile != null) {
                try {
                    configFile.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        finally {
            if (configFile != null) {
                try {
                    configFile.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
    
    public void getOptionsFromOptionAreas(final Map<String, OptionsArea> optionsAreas) throws IOException {
        final Properties prop = new Properties();
        OutputStream configFile = null;
        if (!this.doesConfigExist()) {
            this.createConfigFile();
        }
        try {
            configFile = new FileOutputStream(String.valueOf(FilePaths.CONFIG_PATH.getPath()) + "TeeHero.cfg");
            for (final Map.Entry<String, OptionsArea> entry : optionsAreas.entrySet()) {
                for (final OptionFrame of : entry.getValue().getOptionFrameList()) {
                    this.writeOptionToConfig(of, prop);
                }
            }
            prop.store(configFile, null);
        }
        catch (IOException io) {
            throw io;
        }
        finally {
            if (configFile != null) {
                try {
                    configFile.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (configFile != null) {
            try {
                configFile.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private void writeOptionToConfig(final OptionFrame of, final Properties prop) {
        try {
            prop.setProperty(of.getWriteToOption(), of.getConfigValue());
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Option from " + of.getOptionName() + " could not be written");
        }
    }
    
    private boolean doesConfigExist() {
        final File config = new File(String.valueOf(FilePaths.CONFIG_PATH.getPath()) + "TeeHero.cfg");
        return config.exists();
    }
    
    private void createConfigFile() {
        final File config = new File(String.valueOf(FilePaths.CONFIG_PATH.getPath()) + "TeeHero.cfg");
        try {
            config.createNewFile();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
