// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum DesignAreaDefinition
{
    DEFAULT_AREA("DEFAULT_AREA", 0, "DEFAULT_AREA", 0), 
    SPREADSHIRT_EU_AREA("SPREADSHIRT_EU_AREA", 1, "SPREADSHIRT_EU_AREA", 1), 
    SPREADSHIRT_COM_AREA("SPREADSHIRT_COM_AREA", 2, "SPREADSHIRT_COM_AREA", 2), 
    SHIRTEE_AREA("SHIRTEE_AREA", 3, "SHIRTEE_AREA", 3);
    
    private final String area;
    private final int areaCode;
    
    private DesignAreaDefinition(final String name, final int ordinal, final String area, final int areaCode) {
        this.area = area;
        this.areaCode = areaCode;
    }
    
    public String getAreaName() {
        return this.area;
    }
    
    public int getAreaCode() {
        return this.areaCode;
    }
}
