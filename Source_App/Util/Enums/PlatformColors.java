// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum PlatformColors
{
    SHIRTEE("SHIRTEE", 0, new String[] { "Black", "White", "Red", "Blue", "Grey", "Yellow", "Green", "Pink", "Orange", "Brown" }, new String[][] { { "Black", "Deep Black" }, { "White" }, { "Red", "Rot", "Burgundy", "Bright Red", "Independence Red", "Rubin Red", "Wine" }, { "Blue", "Royal Blue", "Navy", "Diva Blue", "Stone Blue", "Swimming Pool", "Aqua", "Bright Royal", "Caribbean Blue", "Dark Royal", "Deep Black", "Dusty Blue", "French Navy", "Indigo", "Light Blue", "Neon Blue", "Royal", "Sky Blue", "Surf Blue", "Turquoise" }, { "Grey", "Grey Heather", "Heather Grey", "Pacific Grey", "Sport Grey (Heather)", "Charcoal (Solid)", "Ash (Heather)", "Dark Grey (Solid)", "Graphite (Solid)", "Graphite Grey", "Grey Melange", "Smoke (Solid) color" }, { "Yellow", "Yellow light", "Gold", "Gold Yellow", "Acid Yellow", "Lemon Yellow" }, { "Green", "Kelly Green", "Bottle Green", "Dark Green", "Urban Khaki", "Irish Green", "Aubergine", "City Green", "Fern Green", "Green Apple", "Light Green", "Lime Green", "LimeGreen" }, { "Pink", "Pink Sixties", "Pinky", "Powder Pink", "Fuchsia", "Sorbet", "Violet" }, { "Orange", "Orange Crush", "Sunset Orange", "Dark Orange", "Middle Orange" }, { "Brown", "Sand" } });
    
    private final String[] displayedColors;
    private final String[][] platformColors;
    
    private PlatformColors(final String name, final int ordinal, final String[] displayedColors, final String[][] platformColors) {
        this.displayedColors = displayedColors;
        this.platformColors = platformColors;
    }
    
    public String[] getDisplayedColors() {
        return this.displayedColors;
    }
    
    public String[][] getPlatformColors() {
        return this.platformColors;
    }
    
    public String[] getMatchedPlatformColors(final String searchValue) {
        return this.platformColors[this.returnIndex(this.displayedColors, searchValue)];
    }
    
    private int returnIndex(final String[] searchArray, final String searchValue) {
        for (int i = 0; i < searchArray.length; ++i) {
            if (searchArray[i].equals(searchValue)) {
                return i;
            }
        }
        return 4;
    }
}
