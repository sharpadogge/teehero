// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum GUIStyles
{
    BASIC_BTN("BASIC_BTN", 0, "basic_btn"), 
    BASIC_TEXTFIELD("BASIC_TEXTFIELD", 1, "basic_textfield"), 
    BASIC_COMBOBOX("BASIC_COMBOBOX", 2, "basic_combobox"), 
    BASIC_LABEL("BASIC_LABEL", 3, "basic_label"), 
    BASIC_PANE("BASIC_PANE", 4, "basic_pane"), 
    BASIC_HBOX("BASIC_HBOX", 5, "basic_hbox"), 
    BASIC_VBOX("BASIC_VBOX", 6, "basic_vbox"), 
    BASIC_CANVAS("BASIC_CANVAS", 7, "basic_canvas"), 
    BASIC_DESIGNFRAME("BASIC_DESIGNFRAME", 8, "basic_designframe"), 
    BASIC_PROGRESSBAR("BASIC_PROGRESSBAR", 9, "basic_progressbar"), 
    SHIRTEE_DESIGNFRAME("SHIRTEE_DESIGNFRAME", 10, "shirtee_designframe"), 
    SPRD_DESIGNFRAME("SPRD_DESIGNFRAME", 11, "sprd_designframe"), 
    BASIC_OPTIONFRAME("BASIC_OPTIONFRAME", 12, "basic_optionframe"), 
    BASIC_CHECKBOX("BASIC_CHECKBOX", 13, "basic_checkbox"), 
    BTN_APPLY("BTN_APPLY", 14, "btn_apply"), 
    BTN_DESIGNAREA("BTN_DESIGNAREA", 15, "btn_designarea"), 
    BTN_UPLOAD("BTN_UPLOAD", 16, "btn_upload"), 
    BTN_SELECTACTION("BTN_SELECTACTION", 17, "btn_action"), 
    BTN_DELETE("BTN_DELETE", 18, "btn_delete"), 
    LBL_OPTIONTEXT("LBL_OPTIONTEXT", 19, "lbl_optiontext"), 
    LBL_DESIGNFRAMETEXT("LBL_DESIGNFRAMETEXT", 20, "lbl_designframetext"), 
    TF_APPLY("TF_APPLY", 21, "tf_apply"), 
    PANE_DESIGNAREA("PANE_DESIGNAREA", 22, "pn_designarea"), 
    SCPN_DESIGNAREA("SCPN_DESIGNAREA", 23, "scpn_designarea"), 
    BASIC_MENU_BAR("BASIC_MENU_BAR", 24, "basic_menu_bar"), 
    BASIC_MENU_ITEM("BASIC_MENU_ITEM", 25, "basic_menu_item"), 
    BASIC_MENU_CHECKBOX("BASIC_MENU_CHECKBOX", 26, "basic_menu_checkbox"), 
    BASIC_SUB_MENU("BASIC_SUB_MENU", 27, "basic_sub_menu"), 
    BASIC_CONTEXT_MENU("BASIC_CONTEXT_MENU", 28, "basic_context_menu"), 
    BTN_MENUBAR("BTN_MENUBAR", 29, "btn_menubar"), 
    BASIC_DESIGNAREA("BASIC_DESIGNAREA", 30, "basic_designarea");
    
    private String styleClasses;
    
    private GUIStyles(final String name, final int ordinal, final String styleClasses) {
        this.styleClasses = styleClasses;
    }
    
    public String getStyleClasses() {
        return this.styleClasses;
    }
}
