// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum OptionNamesInConfig
{
    GEN_TEEHERO_PATTERN("GEN_TEEHERO_PATTERN", 0, "General", new String[] { "TEEHERO_PATTERN" }, "TEEHERO_PATTERN", "TeeHero Pattern", new String[] { "SPREADSHIRT_EU_PATTERN_CASCADE", "SPREADSHIRT_COM_PATTERN_CASCADE", "SHIRTEE_PATTERN_CASCADE", "TEEHERO_DEFAULT_PATTERN", "SHIRTEE_PATTERN_CASCADE_COLOR" }, "list"), 
    TEEHERO_DEFAULT_PATTERN("TEEHERO_DEFAULT_PATTERN", 1, "General", new String[] { "TEEHERO_PATTERN" }, "TEEHERO_DEFAULT_PATTERN", "Choose your default pattern", (String[])null, "defaultList"), 
    TEEHERO_HEADLESS("TEEHERO_HEADLESS", 2, "General", new String[] { "USE_HEADLESS" }, "USE_HEADLESS", "Start upload in headless mode (no browser will be shown)", (String[])null, "checkbox"), 
    TEEHERO_AUTOLOGIN("TEEHERO_AUTOLOGIN", 3, "General", new String[] { "USE_AUTOLOGIN" }, "USE_AUTOLOGIN", "Auto Login - If you have provided your login information on any site, we will try to log you in automatically", (String[])null, "checkbox"), 
    SPREADSHIRT_EU_PATTERN_CASCADE("SPREADSHIRT_EU_PATTERN_CASCADE", 4, "Spreadshirt EU", new String[] { "SPREADSHIRT_EU_PATTERN_CASCADE", "TEEHERO_PATTERN", "SPREADSHIRT_EU_PATTERNS" }, "SPREADSHIRT_EU_PATTERN_CASCADE", "Match your pattern with the TeeHero pattern and make your live easier", (String[])null, "twoColList"), 
    SPREADSHIRT_EU_URL("SPREADSHIRT_EU_URL", 5, "Spreadshirt EU", new String[] { "SPREADSHIRT_EU_URL" }, "SPREADSHIRT_EU_URL", "Spreadshirt EU URL", (String[])null, "text"), 
    SPREADSHIRT_EU_PATTERNS("SPREADSHIRT_EU_PATTERNS", 6, "Spreadshirt EU", new String[] { "SPREADSHIRT_EU_PATTERNS" }, "SPREADSHIRT_EU_PATTERNS", "Spreadshirt EU Patterns", new String[] { "SPREADSHIRT_EU_PATTERN_CASCADE", "SPREADSHIRT_EU_DEFAULT_PATTERN" }, "list"), 
    SPREADSHIRT_EU_DEFAULT_PATTERN("SPREADSHIRT_EU_DEFAULT_PATTERN", 7, "Spreadshirt EU", new String[] { "SPREADSHIRT_EU_PATTERNS" }, "SPREADSHIRT_EU_DEFAULT_PATTERN", "Spreadshirt EU Default Pattern", (String[])null, "defaultList"), 
    SPREADSHIRT_EU_DEFAULT_PRICE("SPREADSHIRT_EU_DEFAULT_PRICE", 8, "Spreadshirt EU", new String[] { "SPREADSHIRT_EU_DEFAULT_PRICE" }, "SPREADSHIRT_EU_DEFAULT_PRICE", "Spreadshirt EU Default Price", (String[])null, "double"), 
    SPREADSHIRT_EU_LOGIN_USER("SPREADSHIRT_EU_LOGIN_USER", 9, "Spreadshirt EU", new String[] { "SPREADSHIRT_EU_USERNAME" }, "SPREADSHIRT_EU_USERNAME", "Spreadshirt Eu Username (optional). Will be used to login on the side, if provided", (String[])null, "text"), 
    SPREADSHIRT_EU_LOGIN_PASSWORD("SPREADSHIRT_EU_LOGIN_PASSWORD", 10, "Spreadshirt EU", new String[] { "SPREADSHIRT_EU_PASSWORD" }, "SPREADSHIRT_EU_PASSWORD", "Spreadshirt Eu Password (optional). The password will be encryped and stored on your computer", (String[])null, "password"), 
    SPREADSHIRT_COM_PATTERN_CASCADE("SPREADSHIRT_COM_PATTERN_CASCADE", 11, "Spreadshirt COM", new String[] { "SPREADSHIRT_COM_PATTERN_CASCADE", "TEEHERO_PATTERN", "SPREADSHIRT_COM_PATTERNS" }, "SPREADSHIRT_COM_PATTERN_CASCADE", "Match your pattern with the TeeHero pattern and make your live easier", (String[])null, "twoColList"), 
    SPREADSHIRT_COM_URL("SPREADSHIRT_COM_URL", 12, "Spreadshirt COM", new String[] { "SPREADSHIRT_COM_URL" }, "SPREADSHIRT_COM_URL", "Spreadshirt COM URL", (String[])null, "text"), 
    SPREADSHIRT_COM_PATTERNS("SPREADSHIRT_COM_PATTERNS", 13, "Spreadshirt COM", new String[] { "SPREADSHIRT_COM_PATTERNS" }, "SPREADSHIRT_COM_PATTERNS", "Spreadshirt COM Patterns", new String[] { "SPREADSHIRT_COM_PATTERN_CASCADE", "SPREADSHIRT_COM_DEFAULT_PATTERN" }, "list"), 
    SPREADSHIRT_COM_DEFAULT_PATTERN("SPREADSHIRT_COM_DEFAULT_PATTERN", 14, "Spreadshirt COM", new String[] { "SPREADSHIRT_COM_PATTERNS" }, "SPREADSHIRT_COM_DEFAULT_PATTERN", "Spreadshirt COM Default Pattern", (String[])null, "defaultList"), 
    SPREADSHIRT_COM_DEFAULT_PRICE("SPREADSHIRT_COM_DEFAULT_PRICE", 15, "Spreadshirt COM", new String[] { "SPREADSHIRT_COM_DEFAULT_PRICE" }, "SPREADSHIRT_COM_DEFAULT_PRICE", "Spreadshirt COM Default Price", (String[])null, "double"), 
    SPREADSHIRT_COM_LOGIN_USER("SPREADSHIRT_COM_LOGIN_USER", 16, "Spreadshirt COM", new String[] { "SPREADSHIRT_COM_USERNAME" }, "SPREADSHIRT_COM_USERNAME", "Spreadshirt Com Username (optional). Will be used to login on the side, if provided", (String[])null, "text"), 
    SPREADSHIRT_COM_LOGIN_PASSWORD("SPREADSHIRT_COM_LOGIN_PASSWORD", 17, "Spreadshirt COM", new String[] { "SPREADSHIRT_COM_PASSWORD" }, "SPREADSHIRT_COM_PASSWORD", "Spreadshirt Com Password (optional). The password will be encryped and stored on your computer", (String[])null, "password"), 
    SHIRTEE_DESIGNID_COLOR_CASCADE("SHIRTEE_DESIGNID_COLOR_CASCADE", 18, "Shirtee", new String[] { "SHIRTEE_PATTERN_CASCADE_COLOR", "TEEHERO_PATTERN", "SHIRTEE_DESIGNIDS" }, "SHIRTEE_PATTERN_CASCADE_COLOR", "Match your pattern with the TeeHero pattern and make your live easier", (String[])null, "threeColList"), 
    SHIRTEE_URL("SHIRTEE_URL", 19, "Shirtee", new String[] { "SHIRTEE_URL" }, "SHIRTEE_URL", "Shirtee URL", (String[])null, "text"), 
    SHIRTEE_MARKETPLACE("SHIRTEE_MARKETPLACE", 20, "Shirtee", new String[] { "SHIRTEE_MARKETPLACE" }, "SHIRTEE_MARKETPLACE", "Shirtee Marketplaces", new String[] { "SHIRTEE_DEFAULT_MARKETPLACE" }, "list"), 
    SHIRTEE_DESIGNID("SHIRTEE_DESIGNID", 21, "Shirtee", new String[] { "SHIRTEE_DESIGNIDS" }, "SHIRTEE_DESIGNIDS", "Shirtee Design IDs", new String[] { "SHIRTEE_DEFAULT_DESIGNID", "SHIRTEE_PATTERN_CASCADE_COLOR" }, "list"), 
    SHIRTEE_DEFAULT_MARKETPLACE("SHIRTEE_DEFAULT_MARKETPLACE", 22, "Shirtee", new String[] { "SHIRTEE_MARKETPLACE" }, "SHIRTEE_DEFAULT_MARKETPLACE", "Default Marketplace for Shirtee", (String[])null, "defaultList"), 
    SHIRTEE_DEFAULT_DESIGNID("SHIRTEE_DEFAULT_DESIGNID", 23, "Shirtee", new String[] { "SHIRTEE_DESIGNIDS" }, "SHIRTEE_DEFAULT_DESIGNID", "Default Design Id for Shirtee", (String[])null, "defaultList"), 
    SHIRTEE_DEFAULT_CATEGORY("SHIRTEE_DEFAULT_CATEGORY", 24, "Shirtee", new String[] { "SHIRTEE_DEFAULT_CATEGORY" }, "SHIRTEE_DEFAULT_CATEGORY", "Default Category for Shirtee", (String[])null, "text"), 
    SHIRTEE_LOGIN_USER("SHIRTEE_LOGIN_USER", 25, "Shirtee", new String[] { "SHIRTEE_USERNAME" }, "SHIRTEE_USERNAME", "Shirtee Username (optional). Will be used to login on the side, if provided", (String[])null, "text"), 
    SHIRTEE_LOGIN_PASSWORD("SHIRTEE_LOGIN_PASSWORD", 26, "Shirtee", new String[] { "SHIRTEE_PASSWORD" }, "SHIRTEE_PASSWORD", "Shirtee Password (optional). The password will be encryped and stored on your computer", (String[])null, "password");
    
    private final String optionsAreaName;
    private final String[] readFromOption;
    private final String writeToOption;
    private final String type;
    private final String[] conntectedOptions;
    private final String optiontext;
    
    private OptionNamesInConfig(final String name, final int ordinal, final String optionsAreaName, final String[] readFromOption, final String writeToOption, final String optiontext, final String[] conntectedOptions, final String type) {
        this.optionsAreaName = optionsAreaName;
        this.readFromOption = readFromOption;
        this.writeToOption = writeToOption;
        this.type = type;
        this.optiontext = optiontext;
        this.conntectedOptions = conntectedOptions;
    }
    
    public String[] getReadFromOptions() {
        return this.readFromOption;
    }
    
    public String getOptionName() {
        return this.writeToOption;
    }
    
    public String getOptionType() {
        return this.type;
    }
    
    public String getOptionText() {
        return this.optiontext;
    }
    
    public String getOptionsAreaName() {
        return this.optionsAreaName;
    }
    
    public String[] getConntectedOptions() {
        return this.conntectedOptions;
    }
}
