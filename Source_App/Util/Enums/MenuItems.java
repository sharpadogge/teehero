// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum MenuItems
{
    SETTINGS_OPEN_SETTINGS("SETTINGS_OPEN_SETTINGS", 0, "Settings", "Open Settings", "normal", "openSettings"), 
    UPLOAD_DISABLE_ALL("UPLOAD_DISABLE_ALL", 1, "Platforms", "Disable All", "normal", "disableAll"), 
    UPLOAD_ENSABLE_ALL("UPLOAD_ENSABLE_ALL", 2, "Platforms", "Enable All", "normal", "enableAll"), 
    UPLOAD_SPREADSHIRT_EU("UPLOAD_SPREADSHIRT_EU", 3, "Platforms", "Spreadshirt EU", "checkbox", "spreadshirtEU"), 
    UPLOAD_SPREADSHIRT_COM("UPLOAD_SPREADSHIRT_COM", 4, "Platforms", "Spreadshirt COM", "checkbox", "spreadshirtCOM"), 
    UPLOAD_SHIRTEE("UPLOAD_SHIRTEE", 5, "Platforms", "Shirtee", "checkbox", "shirtee"), 
    IMPORT_DESIGN_FILE("IMPORT_DESIGN_FILE", 6, "Import/Export", "Import Data", "normal", "import"), 
    EXPORT_DESIGN_FILE("EXPORT_DESIGN_FILE", 7, "Import/Export", "Export Data", "normal", "export"), 
    RESTRICT_SPREADSHIRT("RESTRICT_SPREADSHIRT", 8, "Help", "Restrictions for Spreadshirt", "normal", "showRestrictionsSpreadshirt"), 
    RESTRICT_SHIRTEE("RESTRICT_SHIRTEE", 9, "Help", "Restrictions for Shirtee", "normal", "showRestrictionsShirtee");
    
    private final String menuName;
    private final String menuItemText;
    private final String menuItemType;
    private final String function;
    
    private MenuItems(final String name, final int ordinal, final String menuName, final String menuItemText, final String menuItemType, final String function) {
        this.menuName = menuName;
        this.menuItemType = menuItemType;
        this.menuItemText = menuItemText;
        this.function = function;
    }
    
    public String getFunction() {
        return this.function;
    }
    
    public String getMenuName() {
        return this.menuName;
    }
    
    public String getMenuItemType() {
        return this.menuItemType;
    }
    
    public String getMenuItemText() {
        return this.menuItemText;
    }
}
