// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum ButtonNamesAndActions
{
    DEFAULT_BUTTON_ACTIONS("DEFAULT_BUTTON_ACTIONS", 0, new String[][] { { "Title", "tbTitle", "textbox" }, { "Description", "tbDescription", "textbox" }, { "Tags", "tbTags", "textbox" }, { "TeeHero", "comboPattern", "combobox", OptionNamesInConfig.GEN_TEEHERO_PATTERN.getOptionName() } }), 
    SPREADSHIRT_EU_BUTTON_ACTIONS("SPREADSHIRT_EU_BUTTON_ACTIONS", 1, new String[][] { { "Title", "tbTitle", "textbox" }, { "Description", "tbDescription", "textbox" }, { "Tags", "tbTags", "textbox" }, { "Price", "tbPrice", "textbox" }, { "Template", "comboTemplate", "combobox", OptionNamesInConfig.SPREADSHIRT_EU_PATTERNS.getOptionName() } }), 
    SPREADSHIRT_COM_BUTTON_ACTIONS("SPREADSHIRT_COM_BUTTON_ACTIONS", 2, new String[][] { { "Title", "tbTitle", "textbox" }, { "Description", "tbDescription", "textbox" }, { "Tags", "tbTags", "textbox" }, { "Price", "tbPrice", "textbox" }, { "Template", "comboTemplate", "combobox", OptionNamesInConfig.SPREADSHIRT_COM_PATTERNS.getOptionName() } }), 
    SHIRTEE_BUTTON_ACTIONS("SHIRTEE_BUTTON_ACTIONS", 3, new String[][] { { "Title", "tbTitle", "textbox" }, { "Description", "tbDescription", "textbox" }, { "Tags", "tbTags", "textbox" }, { "Marketplace", "comboMarketplace", "combobox", OptionNamesInConfig.SHIRTEE_MARKETPLACE.getOptionName() }, { "ID", "comboId", "combobox", OptionNamesInConfig.SHIRTEE_DESIGNID.getOptionName() }, { "Category", "tbCategory", "textbox" } });
    
    private final String[][] formatList;
    
    private ButtonNamesAndActions(final String name, final int ordinal, final String[][] formatList) {
        this.formatList = formatList;
    }
    
    public String[][] getNamesAndActions() {
        return this.formatList;
    }
}
