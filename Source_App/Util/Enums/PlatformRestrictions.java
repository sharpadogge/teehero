// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum PlatformRestrictions
{
    SPREADSHIRT("SPREADSHIRT", 0, "Spreadshirt Restrictions", new String[] { "Size in MB: Less than 100 MB", "Keywords: Min 3 and Max 25", "Resolution: Max 4724 x 7087px", "DPI: Min 200", "Supported Formats: JPG, PNG", "Length of Title: Max 50 characters", "Length of Description: Max 200 characters" }), 
    SHIRTEE("SHIRTEE", 1, "Shirtee Restrictions", new String[] { "Size in MB: Less than 6 MB", "Keywords: Min 0 and Max 10", "Resolution (Textiles): Max 2000 x 3250px", "Resolution (Hoodie - Front): Max 2000 x 3048px", "Resolution (Cups): Max 2300 x 2900px", "Resolution (Enamel-Cups): Max 2000 x 2100px", "Resoltuion (Doormat): Max 3076 x 2000px", "Resolution (Pillows): Max 3000 x 3000px", "DPI: Min 300", "Supported Formats: PNG", "Length of Title: Max 40 characters", "Length of Description: Max 2000 characters" });
    
    private final String restrictionHeader;
    private final String[] restrictions;
    
    private PlatformRestrictions(final String name, final int ordinal, final String restrictionHeader, final String[] restirctions) {
        this.restrictionHeader = restrictionHeader;
        this.restrictions = restirctions;
    }
    
    public String[] getPlatformRestrictions() {
        return this.restrictions;
    }
    
    public String getHeaderText() {
        return this.restrictionHeader;
    }
}
