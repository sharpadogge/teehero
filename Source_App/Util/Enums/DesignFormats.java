// 
// Decompiled by Procyon v0.5.36
// 

package Util.Enums;

public enum DesignFormats
{
    ALLOWED_FORMATS("ALLOWED_FORMATS", 0, new String[] { "*.jpeg", "*.jpg", "*.png", "*.gif", "*.svg", "*.ai", "*.cdr" });
    
    private final String[] formatList;
    
    private DesignFormats(final String name, final int ordinal, final String[] formatList) {
        this.formatList = formatList;
    }
    
    public String[] getFormats() {
        return this.formatList;
    }
}
