// 
// Decompiled by Procyon v0.5.36
// 

package Util.Designs;

import org.openqa.selenium.support.ui.FluentWait;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Function;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import Util.Enums.PlatformColors;
import Util.Enums.OptionNamesInConfig;
import Util.Config.ConfigHandler;
import java.util.List;
import java.io.File;
import Util.Drivers.Driver;

public class ShirteeDesign extends Design
{
    public String marketplace;
    public String id;
    public String category;
    public Driver d;
    public String[] standardColor;
    
    public ShirteeDesign(final File f, final int designId, final String title, final String desc, final List<String> kw, final String id, final String category, final String marketplace, final String teeHeroPattern, final String[] standardColor) {
        super(f, designId, title, desc, teeHeroPattern, kw, 0.0);
        this.id = id;
        this.category = category;
        this.marketplace = marketplace;
        this.standardColor = standardColor;
    }
    
    public ShirteeDesign(final File f, final int designId) {
        super(f, designId);
        this.loadDefaultsFromConfig();
    }
    
    private void loadDefaultsFromConfig() {
        final ConfigHandler ch = new ConfigHandler();
        if (ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()) != null && !ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()).equals("None")) {
            System.out.println(this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SHIRTEE_DESIGNID_COLOR_CASCADE.getOptionName()), ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()), 2));
            this.id = this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SHIRTEE_DESIGNID_COLOR_CASCADE.getOptionName()), ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()), 1);
            this.standardColor = PlatformColors.SHIRTEE.getMatchedPlatformColors(this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SHIRTEE_DESIGNID_COLOR_CASCADE.getOptionName()), ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()), 2).trim());
        }
        else {
            this.id = ch.readSetting(OptionNamesInConfig.SHIRTEE_DEFAULT_DESIGNID.getOptionName());
            System.out.println(this.id);
        }
        this.marketplace = ch.readSetting(OptionNamesInConfig.SHIRTEE_DEFAULT_MARKETPLACE.getOptionName());
        this.category = ch.readSetting(OptionNamesInConfig.SHIRTEE_DEFAULT_CATEGORY.getOptionName());
    }
    
    private String findDefaultPattern(final String valueString, final String searchPattern, final int location) {
        if (valueString != null && valueString.length() > 0) {
            final String[] values = valueString.split(",");
            final String[] array;
            final int length = (array = values).length;
            int i = 0;
            while (i < length) {
                final String s = array[i];
                final String[] keyValuePair = s.split(":");
                if (keyValuePair[0].substring(1, keyValuePair[0].length()).equalsIgnoreCase(searchPattern)) {
                    final String pattern = keyValuePair[location].substring(0, keyValuePair[location].length());
                    if (pattern.substring(pattern.length() - 1, pattern.length()).equalsIgnoreCase("]")) {
                        return keyValuePair[location].substring(0, keyValuePair[location].length() - 1);
                    }
                    return keyValuePair[location].substring(0, keyValuePair[location].length());
                }
                else {
                    ++i;
                }
            }
        }
        return "";
    }
    
    @Override
    public void upload(final Driver d) {
        this.d = d;
        int count = 0;
        final int maxTries = 1;
        while (true) {
            try {
                this.openCampaign();
                this.deleteCurrentImage();
                this.loadNewImage();
                this.setStandardColor();
                this.goToSalesTab();
                this.sendText();
                this.selectMarketplace();
                this.selectCategory();
                this.publishDesign();
                this.checkListing();
                this.waitForUploading();
            }
            catch (Exception e) {
                if (++count == maxTries) {
                    e.printStackTrace();
                    throw e;
                }
                continue;
            }
            break;
        }
    }
    
    private void openCampaign() {
        this.d.openUrl(String.valueOf(this.d.getUrl()) + "/designer/index/campaignForwarding/forwarding_campaign/" + this.id + "/");
        this.d.driverWait(30);
    }
    
    private void deleteCurrentImage() {
        for (int i = 0; i < 3; ++i) {
            this.d.waitReadyState();
            try {
                Thread.sleep(1000L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (this.d.getStyleDisplay("getElementById('base_price')").equals("none")) {
                this.d.click("getElementById('pd_delete')");
            }
            this.d.waitReadyState();
            try {
                Thread.sleep(1000L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!this.d.getStyleDisplay("getElementById('base_price')").equals("none")) {
                break;
            }
        }
        if (this.d.getStyleDisplay("getElementById('base_price')").equals("none")) {
            throw new Error("Could not delete old image");
        }
    }
    
    private void loadNewImage() {
        this.d.getDriver().findElement(By.id("filesToUpload")).sendKeys(this.f.toString());
        final WebDriverWait wait = new WebDriverWait(this.d.getDriver(), 90L);
        ((FluentWait<Object>)wait).until((Function<? super Object, Object>)ExpectedConditions.invisibilityOfElementLocated(By.className("designer-load-info-in")));
        this.d.waitReadyState();
    }
    
    private void setStandardColor() {
        if (this.standardColor[0].equals("White")) {
            try {
                this.d.click("querySelectorAll('[data-color_id=\"3\"]')[1]");
            }
            catch (Exception e) {
                for (int i = 0; i < this.d.getLength("getElementById('right-color-group').children"); ++i) {
                    final String s = this.d.getTitle("getElementById('right-color-group').children[" + i + "]");
                    if (s.equals("White")) {
                        this.d.click("getElementById('right-color-group').children[" + i + "]");
                        break;
                    }
                }
            }
        }
        else {
            boolean b = false;
            String[] standardColor;
            for (int length = (standardColor = this.standardColor).length, k = 0; k < length; ++k) {
                final String color = standardColor[k];
                for (int j = 0; j < this.d.getLength("getElementById('right-color-group').children"); ++j) {
                    final String s2 = this.d.getTitle("getElementById('right-color-group').children[" + j + "]");
                    if (s2.equals(color)) {
                        this.d.click("getElementById('right-color-group').children[" + j + "]");
                        b = true;
                        break;
                    }
                }
                if (b) {
                    break;
                }
            }
        }
        this.d.waitReadyState();
    }
    
    private void goToSalesTab() {
        this.d.click("getElementById('sales-tab')");
        this.d.waitReadyState();
        while (this.d.returnAttribute("getElementById('canvas-error')", "style").equals("")) {
            this.d.click("getElementsByClassName('canvas-error-close')[0]");
            this.d.waitReadyState();
            this.d.click("getElementById('sales-tab')");
            this.d.waitReadyState();
        }
        this.d.waitAll();
        try {
            Thread.sleep(1000L);
        }
        catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }
    
    private void sendText() {
        if (this.title.length() > 40) {
            this.title = this.title.substring(0, 40);
        }
        this.d.sendKeys("getElementById('sales_name')", this.title);
        this.d.removeChilds("getElementsByClassName('fr-element fr-view')[0]");
        if (this.description.length() > 2000) {
            this.description = this.description.substring(0, 2000);
        }
        this.d.appendChild("getElementsByClassName('fr-element fr-view')[0]", this.description, "P");
        int counter = 0;
        for (final String tag : this.tags) {
            this.d.sendKeys("getElementById('tags-input')", tag);
            this.d.createEvent("getElementById('tags-input')", "change");
            if (counter++ == 10) {
                break;
            }
            try {
                Thread.sleep(10L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.d.createEvent("getElementById('pdproduct-description')", "change");
    }
    
    private void selectMarketplace() {
        final String marketplace;
        switch (marketplace = this.marketplace) {
            case "English": {
                this.d.setSelect("getElementById('store_ids')", "0");
                this.d.setSelect("getElementById('pdproduct-description')", "2");
                break;
            }
            case "Shirtee FIN": {
                this.d.setSelect("getElementById('store_ids')", "4");
                this.d.setSelect("getElementById('pdproduct-description')", "2");
                break;
            }
            case "Shirtee NOR": {
                this.d.setSelect("getElementById('store_ids')", "8");
                this.d.setSelect("getElementById('pdproduct-description')", "2");
                break;
            }
            case "Shirtee SWE": {
                this.d.setSelect("getElementById('store_ids')", "12");
                this.d.setSelect("getElementById('pdproduct-description')", "2");
                break;
            }
            case "Shirtee TUR": {
                this.d.setSelect("getElementById('store_ids')", "13");
                this.d.setSelect("getElementById('pdproduct-description')", "2");
                break;
            }
            case "Shirtee DK": {
                this.d.setSelect("getElementById('store_ids')", "2");
                this.d.setSelect("getElementById('pdproduct-description')", "2");
                break;
            }
            case "Shirtee ES": {
                this.d.setSelect("getElementById('store_ids')", "3");
                this.d.setSelect("getElementById('pdproduct-description')", "6");
                break;
            }
            case "Shirtee FR": {
                this.d.setSelect("getElementById('store_ids')", "5");
                this.d.setSelect("getElementById('pdproduct-description')", "3");
                break;
            }
            case "Shirtee IT": {
                this.d.setSelect("getElementById('store_ids')", "6");
                this.d.setSelect("getElementById('pdproduct-description')", "5");
                break;
            }
            case "Shirtee NL": {
                this.d.setSelect("getElementById('store_ids')", "7");
                this.d.setSelect("getElementById('pdproduct-description')", "4");
                break;
            }
            case "Shirtee PL": {
                this.d.setSelect("getElementById('store_ids')", "9");
                this.d.setSelect("getElementById('pdproduct-description')", "7");
                break;
            }
            case "Shirtee PT": {
                this.d.setSelect("getElementById('store_ids')", "10");
                this.d.setSelect("getElementById('pdproduct-description')", "6");
                break;
            }
            case "Shirtee RU": {
                this.d.setSelect("getElementById('store_ids')", "11");
                this.d.setSelect("getElementById('pdproduct-description')", "2");
                break;
            }
            case "German": {
                this.d.setSelect("getElementById('store_ids')", "1");
                this.d.setSelect("getElementById('pdproduct-description')", "1");
                break;
            }
            default:
                break;
        }
        this.d.createEvent("getElementById('pdproduct-description')", "change");
    }
    
    private void selectCategory() {
        this.category = this.category.trim();
        this.category = this.category.toLowerCase();
        final int[] contains = { 0 };
        boolean foundExact = false;
        for (int i = 0; i < this.d.getLength("getElementById('categories_ids')"); ++i) {
            String s = this.d.getText("getElementById('categories_ids')[" + i + "]");
            s = s.toLowerCase();
            if (s.charAt(0) + '\u0001' == 161) {
                s = s.substring(3, s.length());
            }
            s = s.trim();
            if (s.equals(this.category)) {
                this.d.setSelect("getElementById('categories_ids')[" + i + "]");
                this.d.createEvent("getElementById('categories_ids')", "change");
                foundExact = true;
                break;
            }
            if (s.contains(this.category) && contains[0] <= 0) {
                contains[0] = i;
            }
        }
        if (!foundExact && contains[0] > 0) {
            this.d.setSelect("getElementById('categories_ids')[" + contains[0] + "]");
            this.d.createEvent("getElementById('categories_ids')", "change");
        }
    }
    
    private void publishDesign() {
        this.d.waitReadyState();
        this.d.click("getElementById('pd_sales_right')");
        this.d.waitReadyState();
        try {
            Thread.sleep(3000L);
        }
        catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }
    
    private void checkListing() {
        final String s = this.d.getTextContent("getElementById('result_block_save_campaign')");
        if (!s.equals("")) {
            this.title = this.replaceWords(s, this.title);
            this.description = this.replaceWords(s, this.description);
            this.d.removeChilds("getElementById('selected-tags')");
            final List<String> newTags = new ArrayList<String>();
            for (final String tag : this.tags) {
                if (!s.contains(tag.toLowerCase())) {
                    newTags.add(tag);
                }
            }
            this.tags = newTags;
            this.sendText();
            this.d.waitReadyState();
            this.d.click("getElementById('pd_sales_right')");
            this.d.waitReadyState();
        }
    }
    
    private void waitForUploading() {
        try {
            final WebDriverWait wait = new WebDriverWait(this.d.getDriver(), 60L);
            ((FluentWait<Object>)wait).until((Function<? super Object, Object>)ExpectedConditions.invisibilityOfElementLocated(By.id("loading_bar_save_campaign")));
        }
        catch (Exception e) {
            System.out.println("Shirtee uploading bug - Design keeps processing. TeeHero will continue the upload with the next design.");
            this.d.getJS().executeScript("window.onbeforeunload = function() {};", new Object[0]);
            this.d.openUrl("https://www.shirtee.com/de/dashboard/index/index/");
            ((FluentWait<Object>)this.d.getWait()).until((Function<? super Object, Object>)ExpectedConditions.urlContains("/dashboard/index/index/"));
        }
    }
    
    String replaceWords(String forbidden, String text) {
        forbidden = forbidden.trim();
        forbidden = forbidden.substring(forbidden.indexOf(":") + 1, forbidden.length()).trim();
        forbidden = forbidden.substring(forbidden.indexOf(":") + 1, forbidden.length()).trim();
        final String[] forbiddenWords = forbidden.split(",");
        for (int i = 0; i < forbiddenWords.length; ++i) {
            forbiddenWords[i] = forbiddenWords[i].trim();
            text = text.replaceAll("(?i)" + forbiddenWords[i], "");
        }
        text = text.replaceAll("  ", " ");
        return text;
    }
    
    @Deprecated
    protected void openCampaignTest() {
        this.openCampaign();
    }
    
    @Deprecated
    protected void deleteCurrentImageTest() {
        this.deleteCurrentImage();
    }
    
    @Deprecated
    protected void loadNewImageTest() {
        this.loadNewImage();
    }
    
    @Deprecated
    protected void setStandardColorTest() {
        this.setStandardColor();
    }
    
    @Deprecated
    protected void goToSalesTabTest() {
        this.goToSalesTab();
    }
    
    @Deprecated
    protected void sendTextTest() {
        this.sendText();
    }
    
    @Deprecated
    protected void selectMarketplaceTest() {
        this.selectMarketplace();
    }
    
    @Deprecated
    protected void selectCategoryTest() {
        this.selectCategory();
    }
    
    @Deprecated
    protected void publishDesignTest() {
        this.publishDesign();
    }
    
    @Deprecated
    protected void checkListingTest() {
        this.checkListing();
    }
    
    @Deprecated
    protected void waitForUploadingTest() {
        this.waitForUploading();
    }
}
