// 
// Decompiled by Procyon v0.5.36
// 

package Util.Designs;

import org.openqa.selenium.By;
import org.junit.Test;
import org.junit.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import Util.Enums.PlatformColors;
import java.util.ArrayList;
import java.io.File;
import Util.Enums.OptionNamesInConfig;
import Util.Config.ConfigHandler;
import GUI.Pages.MainPage;
import java.util.List;
import Util.Drivers.Driver;
import Util.Sessions.ShirteeSession;

public class ShirteeDesignTest
{
    private static ShirteeSession s;
    private static Driver d;
    private static ShirteeDesign testDesign;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ShirteeDesignTest.s = new ShirteeSession("https://www.shirtee.com/de/", null, null, null);
        (ShirteeDesignTest.d = ShirteeDesignTest.s.getDriver()).startHeadless();
        final ConfigHandler ch = new ConfigHandler();
        final String username = ch.readSetting(OptionNamesInConfig.SHIRTEE_LOGIN_USER.getOptionName());
        final String password = ch.readSetting(OptionNamesInConfig.SHIRTEE_LOGIN_PASSWORD.getOptionName());
        ShirteeDesignTest.s.autoLoginTest(username, password);
        final File f1 = new File(String.valueOf(System.getProperty("user.dir")) + "/Test Files/Sorry this beard is taken.png");
        final String title1 = "Sorry this beard is taken";
        final String desc1 = "Description: Sorry this beard is taken";
        final List<String> kw1 = new ArrayList<String>();
        final String[] strs1 = { "Beard", "Bart", "Mann" };
        for (int i = 0; i < strs1.length; ++i) {
            kw1.add(strs1[i]);
        }
        final String id1 = "1350260";
        final String category1 = "Bart";
        final String marketplace1 = "DE";
        final String[] standardColor1 = PlatformColors.SHIRTEE.getPlatformColors()[2];
        ShirteeDesignTest.testDesign = new ShirteeDesign(f1, 0, title1, desc1, kw1, id1, category1, marketplace1, "null", standardColor1);
        ShirteeDesignTest.testDesign.d = ShirteeDesignTest.d;
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        ShirteeDesignTest.d.closeSession();
    }
    
    @Test
    public void checkStartUpload() {
        ShirteeDesignTest.testDesign.title = "Awesome Beard";
        ShirteeDesignTest.testDesign.upload(ShirteeDesignTest.d);
        final String s = this.getTitleOfLastUploaded(ShirteeDesignTest.d);
        Assert.assertEquals("Awesome Beard", s);
    }
    
    @Test
    public void checkCopyCampaign() {
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.d.waitReadyState();
        ShirteeDesignTest.d.click("getElementById('sales-tab')");
        ShirteeDesignTest.d.waitReadyState();
        String title = ShirteeDesignTest.d.returnAttribute("getElementById('sales_name')", "value");
        Assert.assertEquals("Template WHITE", title);
        ShirteeDesignTest.testDesign.id = "1374297";
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.d.waitReadyState();
        ShirteeDesignTest.d.click("getElementById('sales-tab')");
        ShirteeDesignTest.d.waitReadyState();
        title = ShirteeDesignTest.d.returnAttribute("getElementById('sales_name')", "value");
        Assert.assertEquals("Template RED", title);
    }
    
    @Test
    public void checkDeleteCurrentImage() {
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.d.waitReadyState();
        try {
            Thread.sleep(1000L);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("none", ShirteeDesignTest.d.getStyleDisplay("getElementById('base_price')"));
        ShirteeDesignTest.testDesign.deleteCurrentImageTest();
        Assert.assertEquals("", ShirteeDesignTest.d.getStyleDisplay("getElementById('base_price')"));
    }
    
    @Test
    public void checkLoadNewImage() {
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.testDesign.deleteCurrentImageTest();
        Assert.assertEquals("", ShirteeDesignTest.d.getStyleDisplay("getElementById('base_price')"));
        ShirteeDesignTest.testDesign.loadNewImageTest();
        ShirteeDesignTest.d.waitAll();
        Assert.assertEquals("none", ShirteeDesignTest.d.getStyleDisplay("getElementById('base_price')"));
    }
    
    @Test
    public void checkSetStandardColor() {
        final String[] colors = { "Black", "3", "Rot", "Royal Blue", "Heather Grey", "Gold", "Kelly Green", "Sorbet", "Orange Crush", "Brown" };
        for (int i = 0; i < colors.length; ++i) {
            ShirteeDesignTest.testDesign.openCampaignTest();
            ShirteeDesignTest.testDesign.standardColor = PlatformColors.SHIRTEE.getPlatformColors()[i];
            ShirteeDesignTest.testDesign.setStandardColorTest();
            Assert.assertEquals(1L, ShirteeDesignTest.d.getLength("getElementsByClassName('color-btn selected')"));
            if (i == 1) {
                Assert.assertEquals(colors[i], ShirteeDesignTest.d.returnAttribute("getElementsByClassName('color-btn selected')[0]", "data-color_id"));
            }
            else {
                Assert.assertEquals(colors[i], ShirteeDesignTest.d.returnAttribute("getElementsByClassName('color-btn selected')[0]", "title"));
            }
        }
    }
    
    @Test
    public void checkGoToSalesTab() {
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.testDesign.goToSalesTabTest();
        final String className = ShirteeDesignTest.d.getDriver().findElement(By.id("sales-tab")).getAttribute("className");
        Assert.assertEquals("active", className);
    }
    
    @Test
    public void checkSendText() {
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.testDesign.goToSalesTabTest();
        ShirteeDesignTest.testDesign.title = "Das sind genau 40 Zeichen bis zu hierhin DER REST IST ZUVIEL";
        String desc = "";
        for (int i = 0; i < 520; ++i) {
            desc = String.valueOf(desc) + "Test";
        }
        ShirteeDesignTest.testDesign.description = desc;
        final List<String> kw = new ArrayList<String>();
        final String[] strs = { "Test1", "Test2", "Test3", "Test4", "Test5", "Test6", "Test7", "Test8", "Test9", "Test10", "Test11" };
        for (int j = 0; j < strs.length; ++j) {
            kw.add(strs[j]);
        }
        ShirteeDesignTest.testDesign.tags = kw;
        ShirteeDesignTest.testDesign.sendTextTest();
        Assert.assertEquals(ShirteeDesignTest.testDesign.title.substring(0, 40), ShirteeDesignTest.d.getValue("getElementById('sales_name')"));
        Assert.assertEquals(ShirteeDesignTest.testDesign.description.substring(0, 2000), ShirteeDesignTest.d.getTextContent("getElementsByClassName('fr-element fr-view')[0].firstChild"));
        Assert.assertEquals(10L, ShirteeDesignTest.d.getLength("getElementsByClassName('selected-tag')"));
        for (int j = 0; j < 10; ++j) {
            Assert.assertEquals(kw.get(j), ShirteeDesignTest.d.getTextContent("getElementsByClassName('selected-tag')[" + j + "]"));
        }
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.testDesign.goToSalesTabTest();
        ShirteeDesignTest.testDesign.title = "`~!@#$%^&*()_+=|}]{[:;\"'?/>.,<\u00e0\u00e4\u00e6\u00df\u00fc\u00dc\u00f6\u00e4";
        ShirteeDesignTest.testDesign.description = "`~!@#$%^&*()_+=|}]{[:;\"'?/>.,<\u00e0\u00e4\u00e6\u00df\u00fc\u00dc\u00f6\u00e4";
        final String[] strs2 = { "`~!@#$%", "^&*()_+=", "|}]{[:;", "'?/>.,<\u00e0", "\"\u00e4\u00e6\u00df\u00fc\u00dc\u00f6\u00e4" };
        kw.clear();
        for (int k = 0; k < strs2.length; ++k) {
            kw.add(strs2[k]);
        }
        (ShirteeDesignTest.testDesign.tags = kw).add("");
        ShirteeDesignTest.testDesign.sendTextTest();
        Assert.assertEquals(ShirteeDesignTest.testDesign.title, ShirteeDesignTest.d.getValue("getElementById('sales_name')"));
        Assert.assertEquals(ShirteeDesignTest.testDesign.description, ShirteeDesignTest.d.getTextContent("getElementsByClassName('fr-element fr-view')[0].firstChild"));
        Assert.assertEquals(5L, ShirteeDesignTest.d.getLength("getElementsByClassName('selected-tag')"));
        for (int k = 0; k < 5; ++k) {
            Assert.assertEquals(kw.get(k), ShirteeDesignTest.d.getTextContent("getElementsByClassName('selected-tag')[" + k + "]"));
        }
    }
    
    @Test
    public void checkSelectMarketplaceTest() {
        final String[][] marketAndLang = { { "English", "Limited Edition" }, { "German", "Limitierte Edition" }, { "Shirtee DK", "Limited Edition" }, { "Shirtee ES", "Edici\u00f3n limitada" }, { "Shirtee FIN", "Limited Edition" }, { "Shirtee FR", "Edition Limit\u00e9e" }, { "Shirtee IT", "Edizione limitata " }, { "Shirtee NL", "Limited edition " }, { "Shirtee NOR", "Limited Edition" }, { "Shirtee PL", "Edycja limitowana" }, { "Shirtee PT", "Edici\u00f3n limitada" }, { "Shirtee RU", "Limited Edition" }, { "Shirtee SWE", "Limited Edition" }, { "Shirtee TUR", "Limited Edition" } };
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.testDesign.goToSalesTabTest();
        for (int i = 0; i < marketAndLang.length; ++i) {
            ShirteeDesignTest.testDesign.marketplace = marketAndLang[i][0];
            ShirteeDesignTest.d.removeChilds("getElementsByClassName('fr-element fr-view')[0]");
            ShirteeDesignTest.testDesign.selectMarketplaceTest();
            Assert.assertEquals(marketAndLang[i][0], ShirteeDesignTest.d.getText("getElementById('store_ids').options[document.getElementById('store_ids').selectedIndex]"));
            Assert.assertTrue(ShirteeDesignTest.d.getTextContent("getElementsByClassName('fr-element fr-view')[0]").startsWith(marketAndLang[i][1]));
        }
    }
    
    @Test
    public void checkSelectCategory() {
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.testDesign.goToSalesTabTest();
        ShirteeDesignTest.testDesign.category = "Basketball";
        ShirteeDesignTest.testDesign.selectCategoryTest();
        String s = ShirteeDesignTest.d.getTextContent("getElementById('categories_ids').options[document.getElementById('categories_ids').selectedIndex]").trim().replaceAll(" ", "");
        Assert.assertEquals("Basketball", s);
        ShirteeDesignTest.testDesign.category = "Tiere";
        ShirteeDesignTest.testDesign.selectCategoryTest();
        s = ShirteeDesignTest.d.getTextContent("getElementById('categories_ids').options[document.getElementById('categories_ids').selectedIndex]").trim().replaceAll(" ", "");
        Assert.assertEquals("Tiere", s);
        ShirteeDesignTest.testDesign.category = "Tie";
        ShirteeDesignTest.testDesign.selectCategoryTest();
        s = ShirteeDesignTest.d.getTextContent("getElementById('categories_ids').options[document.getElementById('categories_ids').selectedIndex]").trim().replaceAll(" ", "");
        Assert.assertEquals("Tiere", s);
        ShirteeDesignTest.testDesign.category = "Bowling";
        ShirteeDesignTest.testDesign.selectCategoryTest();
        s = ShirteeDesignTest.d.getTextContent("getElementById('categories_ids').options[document.getElementById('categories_ids').selectedIndex]").trim().replaceAll(" ", "");
        Assert.assertEquals("Bowling & Kegeln", s);
    }
    
    @Test
    public void checkAllPublish() {
        ShirteeDesignTest.testDesign.openCampaignTest();
        ShirteeDesignTest.testDesign.goToSalesTabTest();
        ShirteeDesignTest.testDesign.title = "Test title Nike h&m gucci";
        ShirteeDesignTest.testDesign.description = "Test description Nike h&m gucci";
        final List<String> kw = new ArrayList<String>();
        final String[] strs = { "nike", "Gucci", "H&M", "Test" };
        for (int i = 0; i < strs.length; ++i) {
            kw.add(strs[i]);
        }
        ShirteeDesignTest.testDesign.tags = kw;
        ShirteeDesignTest.testDesign.sendTextTest();
        ShirteeDesignTest.testDesign.publishDesignTest();
        ShirteeDesignTest.testDesign.checkListingTest();
        ShirteeDesignTest.testDesign.waitForUploadingTest();
        final String s = this.getTitleOfLastUploaded(ShirteeDesignTest.d);
        Assert.assertEquals("Test title", s);
    }
    
    private String getTitleOfLastUploaded(final Driver d) {
        d.openUrl("https://www.shirtee.com/de/dashboard/index/index/?approved=2");
        d.driverWait(10);
        String s = d.getTextContent("getElementsByClassName('amount amount--has-pages')[0]").trim();
        s = s.substring(s.lastIndexOf(32, s.length()));
        final int i = Integer.parseInt(s.trim()) / 10 + 1;
        d.openUrl("https://www.shirtee.com/de/dashboard/index/index/?approved=2&p=" + i);
        final long l = (d.getLength("getElementsByClassName('campaign-title')") - 1L) / 3L;
        final String titleLastUpload = d.getTextContent("getElementsByClassName('campaign-title')[" + l * 3L + "]").trim();
        return titleLastUpload;
    }
}
