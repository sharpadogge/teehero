// 
// Decompiled by Procyon v0.5.36
// 

package Util.Designs;

import org.openqa.selenium.support.ui.FluentWait;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.function.Function;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.By;
import Util.Enums.OptionNamesInConfig;
import Util.Config.ConfigHandler;
import java.util.List;
import java.io.File;
import Util.Drivers.Driver;

public class SpreadshirtDesign extends Design
{
    public int templateNumber;
    public String templateName;
    public Driver d;
    
    @Deprecated
    public SpreadshirtDesign(final File f) {
        super(f);
    }
    
    public SpreadshirtDesign(final File f, final int designId, final String title, final String description, final List<String> tags, final String templateName, final double price, final String teeHeroPattern) {
        super(f, designId, title, description, teeHeroPattern, tags, price);
        super.title = title;
        super.description = description;
        super.price = price;
        this.tags = tags;
        this.templateName = templateName;
    }
    
    public SpreadshirtDesign(final File f, final int designId, final String type) {
        super(f, designId);
        this.loadDefaultsFromConfig(type);
    }
    
    private void loadDefaultsFromConfig(final String type) {
        final ConfigHandler ch = new ConfigHandler();
        switch (type) {
            case "eu": {
                if (ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()) != null && ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()) != "None") {
                    this.templateName = this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_PATTERN_CASCADE.getOptionName()), ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()));
                }
                else {
                    this.templateName = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_DEFAULT_PATTERN.getOptionName());
                }
                if (ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_DEFAULT_PRICE.getReadFromOptions()[0]).isEmpty()) {
                    this.price = 0.0;
                    break;
                }
                this.price = Double.parseDouble(ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_DEFAULT_PRICE.getReadFromOptions()[0]));
                break;
            }
            case "com": {
                if (ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()) != null && ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()) != "None") {
                    this.templateName = this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_PATTERN_CASCADE.getOptionName()), ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName()));
                }
                else {
                    this.templateName = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_DEFAULT_PATTERN.getOptionName());
                }
                if (ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_DEFAULT_PRICE.getReadFromOptions()[0]).isEmpty()) {
                    this.price = 0.0;
                    break;
                }
                this.price = Double.parseDouble(ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_DEFAULT_PRICE.getReadFromOptions()[0]));
                break;
            }
            default:
                break;
        }
    }
    
    private String findDefaultPattern(final String valueString, final String searchPattern) {
        if (valueString != null && valueString.length() > 0) {
            final String[] values = valueString.split(",");
            String[] array;
            for (int length = (array = values).length, i = 0; i < length; ++i) {
                final String s = array[i];
                final String[] keyValuePair = s.split(":");
                if (keyValuePair[0].substring(1, keyValuePair[0].length()).equalsIgnoreCase(searchPattern)) {
                    return keyValuePair[1].substring(0, keyValuePair[1].length() - 1);
                }
            }
        }
        return "";
    }
    
    @Override
    public void upload(final Driver d) {
        this.d = d;
        this.uploadPage();
        int count = 0;
        final int maxTries = 3;
        while (true) {
            try {
                this.selectDesignPage();
                this.templatePage();
                this.detailsPage();
            }
            catch (Exception e) {
                if (++count == maxTries) {
                    e.printStackTrace();
                    throw e;
                }
                continue;
            }
            break;
        }
    }
    
    private void uploadPage() {
        this.d.waitAll();
        this.d.waitUntilElementVisible(By.id("upload-btn"));
        if (this.d.checkPresence("getElementsByClassName('rocket-image')[0]")) {
            ((JavascriptExecutor)this.d.getDriver()).executeScript("document.getElementById('hiddenFileInput').classList.remove('hidden-file-input');", new Object[0]);
            this.d.waitUntilElementVisible(By.xpath("//*[@id='hiddenFileInput']"));
            this.d.getDriver().findElement(By.xpath("//*[@id='hiddenFileInput']")).sendKeys(this.f.toString());
            this.d.waitAll();
            final WebDriverWait wait = new WebDriverWait(this.d.getDriver(), 60L);
            ((FluentWait<Object>)wait).until((Function<? super Object, Object>)ExpectedConditions.invisibilityOf(this.d.getDriver().findElement(By.xpath("//*[@class='modal-dialog modal-md']"))));
            if (this.d.checkPresence("getElementsByClassName('design-list-view')[0]")) {
                this.d.click("getElementsByClassName('link-green')[0]");
            }
        }
        else {
            if (this.d.checkPresence("getElementsByClassName('design-list-view')[0]")) {
                this.d.click("getElementsByClassName('link-green')[0]");
            }
            this.d.click("getElementById('upload-btn')");
            ((JavascriptExecutor)this.d.getDriver()).executeScript("document.getElementById('design-upload-open-file-browser-qa').style.display='block';", new Object[0]);
            this.d.waitUntilElementVisible(By.xpath("//*[@id='design-upload-open-file-browser-qa']/input"));
            this.d.getDriver().findElement(By.xpath("//*[@id='design-upload-open-file-browser-qa']/input")).sendKeys(this.f.toString());
            this.d.waitAll();
            final WebDriverWait wait = new WebDriverWait(this.d.getDriver(), 60L);
            ((FluentWait<Object>)wait).until((Function<? super Object, Object>)ExpectedConditions.invisibilityOf(this.d.getDriver().findElement(By.xpath("//*[@class='modal-dialog modal-md']"))));
        }
    }
    
    private void selectDesignPage() {
        this.d.openUrl();
        this.d.driverWait(10);
        this.d.waitUntilElementVisible(By.xpath("//*[@class='idea-preview-image preview-image design-preview']"));
        this.d.click("getElementsByClassName('idea-preview-image preview-image design-preview')[0]");
        int counter = 0;
        while (this.d.checkPresence("getElementsByClassName('preview-image-container preview-image-loader')[0]")) {
            try {
                Thread.sleep(2000L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.d.openUrl();
            this.d.driverWait(10);
            this.d.waitUntilElementVisible(By.xpath("//*[@class='idea-preview-image preview-image design-preview']"));
            this.d.click("getElementsByClassName('idea-preview-image preview-image design-preview')[0]");
            if (counter++ <= 3) {
                try {
                    throw new Exception("File keeps processing on spreadshirt");
                }
                catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
    
    private void templatePage() {
        this.d.waitUntilPresenceOfElement(By.id("account-settings-save-button"));
        this.d.waitUntilElementClickable(By.id("account-settings-save-button"));
        this.d.waitAll();
        this.d.driverWait(10);
        if (this.d.checkPresence("getElementsByClassName('toggle')[0]")) {
            this.d.click("getElementsByClassName('toggle')[0]");
            this.d.waitAngular();
            this.d.clickVue("getElementById('account-settings-save-button')");
        }
        this.d.waitAll();
        this.d.driverWait(10);
        this.d.waitUntilPresenceOfElement(By.xpath("//a[@class='link-green icon-link load-template']"));
        this.d.click("getElementsByClassName('link-green icon-link load-template')[0]");
        this.d.waitUntilElementVisible(By.xpath("//button[@class='btn-progress btn btn-primary icon-btn IDLE null']"));
        this.d.waitAll();
        this.createTemplateNumber();
        this.d.waitAll();
        this.d.clickVue("getElementsByClassName('btn-progress btn btn-primary icon-btn IDLE null')[" + this.templateNumber + "]");
        this.d.driverWait(10);
        this.d.waitAll();
        this.d.driverWait(10);
        try {
            Thread.sleep(1000L);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.d.clickVue("getElementById('account-settings-save-button')");
    }
    
    private void detailsPage() {
        this.d.waitUntilElementVisible(By.id("input-design-name"));
        this.setTitle();
        this.d.waitAll();
        this.setDescription();
        this.d.waitAll();
        this.setKeywords();
        this.d.waitAll();
        this.d.click("getElementsByClassName('btn btn-default manual')[0]");
        this.d.waitUntilElementVisible(By.name("commission"));
        this.d.sendKeys("getElementsByName('commission')[0]", Double.toString(this.price));
        this.d.createEvent("getElementsByName('commission')[0]", "blur");
        this.checkTitle();
        this.checkDescription();
        this.checkKW();
        if (this.d.countElements("getElementsByClassName('tag-input-item pull-left')") < 3) {
            this.sendKeywords("TooFewKeywords1, TooFewKeywords2, TooFewKeywords3");
            this.d.waitAll();
            try {
                Thread.sleep(1000L);
            }
            catch (Exception ex) {}
            this.d.waitAll();
            this.d.createEvent("getElementsByClassName('dropdown-input')[0]", "blur");
            this.d.waitAngular();
            this.d.fire("getElementsByClassName('dropdown-input')[0]");
            this.d.waitAll();
        }
        this.d.waitAll();
        this.d.createEvent("getElementById('account-settings-save-button')", "focus");
        this.d.clickVue("getElementById('account-settings-save-button')");
        this.d.waitAll();
        this.d.driverWait(10);
        this.d.waitAll();
        try {
            this.d.waitUntilPresenceOfElement(By.xpath("//div[@class='modal-container idea-config-dialog']"));
        }
        catch (Exception e) {
            this.d.waitUntilPresenceOfElement(By.className("idea-config-dialog"));
        }
    }
    
    private void createTemplateNumber() {
        this.templateName = this.templateName.toLowerCase();
        this.templateName = this.templateName.trim();
        for (int i = 0; i < this.d.getLength("getElementsByClassName('name')"); ++i) {
            String s = this.d.getTextContent("getElementsByClassName('name')[" + i + "]");
            String[] parts = s.split("    ");
            s = parts[0];
            if (s.contains("   ")) {
                parts = s.split("   ");
                s = parts[0];
            }
            s = s.toLowerCase();
            s = s.trim();
            if (s.equals(this.templateName)) {
                this.templateNumber = i;
                break;
            }
        }
    }
    
    private void setTitle() {
        if (this.title.length() > 50) {
            this.title = this.title.substring(0, 50);
        }
        if (this.title.length() == 0) {
            this.title = "No Title";
        }
        int counter = 0;
        while (!this.title.equals(this.d.getInputText("getElementById('input-design-name')"))) {
            this.d.sendKeys("getElementById('input-design-name')", this.title);
            this.d.waitAll();
            if (counter++ > 5) {
                try {
                    throw new Exception("Title couldn't be inserted");
                }
                catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
        this.checkTitle();
    }
    
    private void setDescription() {
        if (this.description.length() > 200) {
            this.description = this.description.substring(0, 200);
        }
        int counter = 0;
        while (!this.description.equals(this.d.getInputText("getElementById('input-design-description')"))) {
            this.d.sendKeys("getElementById('input-design-description')", this.description);
            this.d.waitAll();
            if (counter++ > 5) {
                try {
                    throw new Exception("Description couldn't be inserted");
                }
                catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
        this.checkDescription();
    }
    
    private void setKeywords() {
        this.d.clickAll("getElementsByClassName('tag-input-item pull-left')");
        this.d.driverWait(10);
        String connectedTags = "";
        for (int i = 0; i < this.tags.size(); ++i) {
            connectedTags = String.valueOf(connectedTags) + this.tags.get(i) + ",";
            if (i == 24) {
                break;
            }
        }
        this.sendKeywords(connectedTags);
        try {
            this.d.clickLast("getElementsByClassName('tag-input-item pull-left')");
        }
        catch (Exception e) {
            this.d.waitAll();
            this.d.createEvent("getElementsByClassName('dropdown-input')[0]", "blur");
            this.d.waitAngular();
            this.d.fire("getElementsByClassName('dropdown-input')[0]");
            try {
                this.d.clickLast("getElementsByClassName('tag-input-item pull-left')");
            }
            catch (Exception e2) {
                try {
                    Thread.sleep(1000L);
                    this.d.clickLast("getElementsByClassName('tag-input-item pull-left')");
                }
                catch (Exception ex) {}
            }
        }
        this.d.clickAll("getElementsByClassName('tag-input-item pull-left invalid')");
    }
    
    private void sendKeywords(final String connectedTags) {
        this.d.fire("getElementsByClassName('dropdown-input')[0]");
        this.d.waitAngular();
        this.d.createEvent("getElementsByClassName('dropdown-input')[0]", "focus");
        this.d.waitAll();
        this.d.sendKeys("getElementsByClassName('dropdown-input')[0]", connectedTags);
        this.d.waitAll();
        this.d.createEvent("getElementsByClassName('dropdown-input')[0]", "blur");
        this.d.waitAngular();
        this.d.fire("getElementsByClassName('dropdown-input')[0]");
        this.d.waitAll();
    }
    
    private void checkTitle() {
        this.d.waitAll();
        this.d.driverWait(10);
        this.d.waitAll();
        try {
            Thread.sleep(200L);
        }
        catch (Exception ex) {}
        if (this.d.checkPresence("getElementsByClassName('error-info error-info-name')[0]")) {
            this.d.waitUntilElementVisible(By.className("error-info-name"));
            this.title = this.replaceWords(this.d.getTextContent("getElementsByClassName('error-info error-info-name')[0]"), this.title);
            this.d.waitAll();
            this.setTitle();
            this.d.waitAll();
        }
    }
    
    private void checkDescription() {
        this.d.waitAll();
        this.d.driverWait(10);
        this.d.waitAll();
        if (this.d.checkPresence("getElementsByClassName('error-info error-info-description')[0]")) {
            this.description = this.replaceWords(this.d.getTextContent("getElementsByClassName('error-info error-info-description')[0]"), this.description);
            this.d.waitAll();
            this.setDescription();
            this.d.waitAll();
        }
    }
    
    private void checkKW() {
        this.d.waitAll();
        this.d.driverWait(10);
        this.d.waitAll();
        if (this.d.checkPresence("getElementsByClassName('error-info error-info-tags')[0]")) {
            final int kwElements = this.d.countElements("getElementsByClassName('tag-input-item pull-left invalid')");
            final String[] forbiddenKW = new String[kwElements];
            for (int i = 0; i < kwElements; ++i) {
                forbiddenKW[i] = this.d.getTextContent("getElementsByClassName('tag-input-item pull-left invalid')[" + i + "]").trim();
            }
            String[] array;
            for (int length = (array = forbiddenKW).length, j = 0; j < length; ++j) {
                final String fKW = array[j];
                final List<String> newTags = new ArrayList<String>();
                for (final String KW : this.tags) {
                    if (!KW.trim().equals(fKW)) {
                        newTags.add(KW);
                    }
                }
                this.tags = newTags;
            }
            this.d.waitAll();
            this.setKeywords();
            this.d.waitAll();
        }
    }
    
    private String replaceWords(String forbidden, String text) {
        forbidden = forbidden.trim();
        forbidden = forbidden.substring(forbidden.indexOf(":") + 1, forbidden.length()).trim();
        final String[] forbiddenWords = forbidden.split(",");
        for (int i = 0; i < forbiddenWords.length; ++i) {
            forbiddenWords[i] = forbiddenWords[i].trim();
            text = text.replaceAll("(?i)" + forbiddenWords[i], "");
        }
        text = text.replaceAll("  ", " ");
        return text;
    }
    
    @Override
    public String toString() {
        return "SpreadshirtDesign [designId=" + this.designId + ", title=" + this.title + ", description=" + this.description + ", f=" + this.f + ", tags=" + this.tags + ", price=" + this.price + ", isSelected=" + this.isSelected + "]";
    }
    
    @Deprecated
    public void uploadPageTest() {
        this.uploadPage();
    }
    
    @Deprecated
    public void selectDesignPageTest() {
        this.selectDesignPage();
    }
    
    @Deprecated
    public void templatePageTest() {
        this.templatePage();
    }
    
    @Deprecated
    public void detailsPageTest() {
        this.detailsPage();
    }
}
