// 
// Decompiled by Procyon v0.5.36
// 

package Util.Designs;

import Util.Enums.OptionNamesInConfig;
import Util.Config.ConfigHandler;
import Util.Drivers.Driver;
import java.util.ArrayList;
import java.util.List;
import java.io.File;

public class Design
{
    public int designId;
    public String title;
    public String description;
    public File f;
    public String teeHeroPattern;
    public List<String> tags;
    public double price;
    public boolean isSelected;
    
    public Design(final File f, final int designId) {
        this.isSelected = false;
        this.designId = designId;
        this.tags = new ArrayList<String>();
        this.f = f;
        this.loadDefaultsFromConfig();
    }
    
    public Design(final File f, final int designId, final String title, final String description, final String teeHeroPattern, final List<String> tags, final double price) {
        this.isSelected = false;
        this.designId = designId;
        this.title = title;
        this.description = description;
        this.price = price;
        this.teeHeroPattern = teeHeroPattern;
        this.tags = (List<String>)(ArrayList)tags;
        this.f = f;
    }
    
    public Design(final File f) {
        this.isSelected = false;
        this.tags = new ArrayList<String>();
        this.f = f;
        this.loadDefaultsFromConfig();
    }
    
    public void upload(final Driver d) {
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public File getF() {
        return this.f;
    }
    
    public List<String> getTags() {
        return this.tags;
    }
    
    public double getPrice() {
        return this.price;
    }
    
    public void changeSelectionState(final boolean state) {
        this.isSelected = state;
    }
    
    public boolean isSelected() {
        return this.isSelected;
    }
    
    public int getDesignId() {
        return this.designId;
    }
    
    @Override
    public String toString() {
        return "Design [designId=" + this.designId + ", title=" + this.title + ", description=" + this.description + ", f=" + this.f + ", tags=" + this.tags + ", price=" + this.price + ", isSelected=" + this.isSelected + "]";
    }
    
    private void loadDefaultsFromConfig() {
        final ConfigHandler ch = new ConfigHandler();
        this.teeHeroPattern = ch.readSetting(OptionNamesInConfig.TEEHERO_DEFAULT_PATTERN.getOptionName());
    }
}
