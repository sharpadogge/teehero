// 
// Decompiled by Procyon v0.5.36
// 

package Util.Designs;

import org.openqa.selenium.By;
import org.junit.Assert;
import org.junit.Test;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import java.util.ArrayList;
import java.io.File;
import Util.Enums.OptionNamesInConfig;
import GUI.Pages.MainPage;
import Util.Sessions.SpreadshirtSession;
import Util.Config.ConfigHandler;
import java.util.List;
import Util.Drivers.Driver;

public class SpreadshirtDesignTest
{
    private static SpreadshirtDesign sDesignCOM;
    private static SpreadshirtDesign sDesignEU;
    private static Driver dCOM;
    private static Driver dEU;
    private static String title1;
    private static String desc1;
    private static List<String> kw1;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        final ConfigHandler ch = new ConfigHandler();
        final SpreadshirtSession sCOM = new SpreadshirtSession("https://partner.spreadshirt.com/designs", null, null, null);
        (SpreadshirtDesignTest.dCOM = sCOM.getDriver()).startHeadless();
        final String usernameCOM = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_LOGIN_USER.getOptionName());
        final String passwordCOM = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_LOGIN_PASSWORD.getOptionName());
        sCOM.autoLoginTest(usernameCOM, passwordCOM);
        SpreadshirtDesignTest.dCOM.openUrl();
        final SpreadshirtSession sEU = new SpreadshirtSession("https://partner.spreadshirt.de/designs", null, null, null);
        (SpreadshirtDesignTest.dEU = sEU.getDriver()).startHeadless();
        final String usernameEU = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_LOGIN_USER.getOptionName());
        final String passwordEU = ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_LOGIN_PASSWORD.getOptionName());
        sEU.autoLoginTest(usernameEU, passwordEU);
        SpreadshirtDesignTest.dEU.openUrl();
        final File f1 = new File(String.valueOf(System.getProperty("user.dir")) + "/Test Files/Police - Feel safe at night, sleep with a cop!.png");
        SpreadshirtDesignTest.title1 = "Police - Feel safe at night, sleep with a cop!'" + (1 + (int)(Math.random() * 100.0));
        SpreadshirtDesignTest.desc1 = "Description: Police - Feel safe at night, sleep with a cop!";
        SpreadshirtDesignTest.kw1 = new ArrayList<String>();
        final String[] strs1 = { "Police", "Polizei", "Cop", "Cops", "Hero", "Safe", "Men", "Man", "Night", "Sleep", "Fun", "Present" };
        for (int i = 0; i < strs1.length; ++i) {
            SpreadshirtDesignTest.kw1.add(strs1[i]);
        }
        SpreadshirtDesignTest.sDesignCOM = new SpreadshirtDesign(f1, 0, SpreadshirtDesignTest.title1, SpreadshirtDesignTest.desc1, SpreadshirtDesignTest.kw1, "whIte", 4.53, null);
        SpreadshirtDesignTest.sDesignCOM.d = SpreadshirtDesignTest.dCOM;
        SpreadshirtDesignTest.sDesignEU = new SpreadshirtDesign(f1, 0, SpreadshirtDesignTest.title1, SpreadshirtDesignTest.desc1, SpreadshirtDesignTest.kw1, "WHITE", 4.53, null);
        SpreadshirtDesignTest.sDesignEU.d = SpreadshirtDesignTest.dEU;
    }
    
    @AfterClass
    public static void tearDown() {
        delteAllDesigns(SpreadshirtDesignTest.dCOM);
        SpreadshirtDesignTest.dCOM.closeSession();
        delteAllDesigns(SpreadshirtDesignTest.dEU);
        SpreadshirtDesignTest.dEU.closeSession();
    }
    
    @After
    public void setStandardListing() {
        SpreadshirtDesignTest.sDesignCOM.title = SpreadshirtDesignTest.title1;
        SpreadshirtDesignTest.sDesignCOM.description = SpreadshirtDesignTest.desc1;
        SpreadshirtDesignTest.sDesignCOM.tags = SpreadshirtDesignTest.kw1;
        SpreadshirtDesignTest.sDesignCOM.templateName = "whIte";
        SpreadshirtDesignTest.sDesignCOM.price = 4.53;
        SpreadshirtDesignTest.sDesignEU.title = SpreadshirtDesignTest.title1;
        SpreadshirtDesignTest.sDesignEU.description = SpreadshirtDesignTest.desc1;
        SpreadshirtDesignTest.sDesignEU.tags = SpreadshirtDesignTest.kw1;
        SpreadshirtDesignTest.sDesignEU.templateName = "WHITE";
        SpreadshirtDesignTest.sDesignEU.price = 4.53;
    }
    
    @Test
    public void checkUploadEU() {
        this.checkUploadHelper(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU, "4,53 \u20ac", "76 Produkte");
    }
    
    @Test
    public void checkUploadCOM() {
        this.checkUploadHelper(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM, "$4.53", "66 Products");
    }
    
    private void checkUploadHelper(final Driver d, final SpreadshirtDesign sDesign, final String price, final String products) {
        this.checkIfUplaodNeeded(d, sDesign);
        sDesign.upload(d);
        this.checkSpreadListing(sDesign.title, price, sDesign.description, sDesign.tags.size(), products, d, sDesign.tags);
    }
    
    @Test
    public void checkUploadPageEU() {
        this.checkUploadPageHelper(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU);
    }
    
    @Test
    public void checkUploadPageCOM() {
        this.checkUploadPageHelper(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM);
    }
    
    private void checkUploadPageHelper(final Driver d, final SpreadshirtDesign sDesign) {
        delteAllDesigns(d);
        d.waitAll();
        d.driverWait(5);
        sDesign.uploadPageTest();
        d.waitAll();
        d.driverWait(10);
        sDesign.uploadPageTest();
        d.waitAll();
        d.driverWait(10);
        Assert.assertEquals(2L, d.getLength("getElementsByClassName('card design-tile')"));
    }
    
    @Test
    public void checkSelectDesignPageEU() {
        this.checkSelectDesignPage(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU);
        SpreadshirtDesignTest.dEU.waitUntilPresenceOfElement(By.id("account-settings-save-button"));
        SpreadshirtDesignTest.dEU.waitUntilElementClickable(By.id("account-settings-save-button"));
        if (SpreadshirtDesignTest.dEU.checkPresence("getElementsByClassName('toggle')[0]")) {
            SpreadshirtDesignTest.dEU.click("getElementsByClassName('toggle')[0]");
            SpreadshirtDesignTest.dEU.waitAngular();
            SpreadshirtDesignTest.dEU.clickVue("getElementById('account-settings-save-button')");
            SpreadshirtDesignTest.dEU.waitAll();
            SpreadshirtDesignTest.dEU.driverWait(10);
            SpreadshirtDesignTest.dEU.waitAll();
            SpreadshirtDesignTest.dEU.waitUntilPresenceOfElement(By.id("account-settings-save-button"));
            SpreadshirtDesignTest.dEU.waitUntilElementClickable(By.id("account-settings-save-button"));
            SpreadshirtDesignTest.dEU.driverWait(10);
            SpreadshirtDesignTest.dEU.waitAll();
        }
        Assert.assertEquals("W\u00e4hle und gestalte Produkte", SpreadshirtDesignTest.dEU.getTextContent("getElementsByClassName('heading')[0]"));
    }
    
    @Test
    public void checkSelectDesignPageCOM() {
        this.checkSelectDesignPage(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM);
        SpreadshirtDesignTest.dCOM.waitUntilPresenceOfElement(By.id("account-settings-save-button"));
        SpreadshirtDesignTest.dCOM.waitUntilElementClickable(By.id("account-settings-save-button"));
        if (SpreadshirtDesignTest.dCOM.checkPresence("getElementsByClassName('toggle')[0]")) {
            SpreadshirtDesignTest.dCOM.click("getElementsByClassName('toggle')[0]");
            SpreadshirtDesignTest.dCOM.waitAngular();
            SpreadshirtDesignTest.dCOM.clickVue("getElementById('account-settings-save-button')");
            SpreadshirtDesignTest.dCOM.waitAll();
            SpreadshirtDesignTest.dCOM.driverWait(10);
            SpreadshirtDesignTest.dCOM.waitAll();
        }
        Assert.assertEquals("Choose and design products", SpreadshirtDesignTest.dCOM.getTextContent("getElementsByClassName('heading')[0]"));
    }
    
    private void checkSelectDesignPage(final Driver d, final SpreadshirtDesign sDesign) {
        this.checkIfUplaodNeeded(d, sDesign);
        sDesign.selectDesignPageTest();
        d.waitUntilPresenceOfElement(By.xpath("//div[@class='wizard-content']"));
        d.waitUntilElementClickable(By.id("account-settings-save-button"));
    }
    
    @Test
    public void checkTemplatePageEU() {
        this.checkIfUplaodNeeded(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU, "F\u00fcr dunkle Designs", 126);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU, "F\u00fcr helle Designs", 117);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU, "black", 33);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU, "WHITE", 83);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU, "rEd", 58);
    }
    
    @Test
    public void checkTemplatePageCOM() {
        this.checkIfUplaodNeeded(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM, "For dark designs", 116);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM, "For bright designs", 109);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM, "black", 72);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM, "WHITE", 73);
        this.checkTemplatePageHelper(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM, "rEd", 50);
    }
    
    private void checkTemplatePageHelper(final Driver d, final SpreadshirtDesign sDesign, final String template, final int count) {
        sDesign.templateName = template;
        d.openUrl();
        sDesign.selectDesignPageTest();
        sDesign.templatePageTest();
        d.waitUntilElementVisible(By.id("input-design-name"));
        d.click("getElementsByClassName('btn btn-light icon-btn')[0]");
        d.waitAll();
        d.waitUntilPresenceOfElement(By.xpath("//div[@class='wizard-content']"));
        d.waitUntilElementClickable(By.id("account-settings-save-button"));
        Assert.assertEquals(count, d.getLength("getElementsByClassName('product-list')[0].children"));
    }
    
    @Test
    public void checkDetailsPageEU() {
        final List<String> kw = new ArrayList<String>();
        final String[] strs = { "Allowed", "Keywords", "TooFewKeywords1", "TooFewKeywords2", "TooFewKeywords3" };
        for (int i = 0; i < strs.length; ++i) {
            kw.add(strs[i]);
        }
        this.checkDetailsPageHelperForbidden(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU);
        this.checkSpreadListing("This is the title", "15,00 \u20ac", "This is the description", 5, "1 Produkte", SpreadshirtDesignTest.dEU, kw);
        this.checkDetailsPageHelperToLongListing(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU, "13,33 \u20ac", "1 Produkte");
        this.checkDetailsPageHelperSpecialCharacter(SpreadshirtDesignTest.dEU, SpreadshirtDesignTest.sDesignEU, "6,54 \u20ac", "1 Produkte");
    }
    
    @Test
    public void checkDetailsPageCOM() {
        final List<String> kw = new ArrayList<String>();
        final String[] strs = { "Allowed", "Keywords", "TooFewKeywords1", "TooFewKeywords2", "TooFewKeywords3" };
        for (int i = 0; i < strs.length; ++i) {
            kw.add(strs[i]);
        }
        this.checkDetailsPageHelperForbidden(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM);
        this.checkSpreadListing("This is the title", "$15.00", "This is the description", 5, "1 Products", SpreadshirtDesignTest.dCOM, kw);
        this.checkDetailsPageHelperToLongListing(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM, "$13.33", "1 Products");
        this.checkDetailsPageHelperSpecialCharacter(SpreadshirtDesignTest.dCOM, SpreadshirtDesignTest.sDesignCOM, "$6.54", "1 Products");
    }
    
    private void checkDetailsPageHelperForbidden(final Driver d, final SpreadshirtDesign sDesign) {
        sDesign.title = "Adidas Nike This is the title";
        sDesign.description = "Adidas Nike This is the description";
        final List<String> kw = new ArrayList<String>();
        final String[] strs = { "Adidas", "Allowed", "Allowed", "Keywords", "Nike" };
        for (int i = 0; i < strs.length; ++i) {
            kw.add(strs[i]);
        }
        sDesign.tags = kw;
        sDesign.price = 15.0;
        this.checkDetailsPageHelper(d, sDesign);
    }
    
    private void checkDetailsPageHelperToLongListing(final Driver d, final SpreadshirtDesign sDesign, final String price, final String products) {
        sDesign.title = "This is a title which is way too long the rest ... WILL NOT DISPLAYED";
        sDesign.description = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxThis is a title which is way too long the rest ... WILL NOT BE DISPLAYED";
        final List<String> kw = new ArrayList<String>();
        final String[] strs = { "Test1", "Test2", "Test3", "Test4", "Test5", "Test6", "Test7", "Test8", "Test9", "Test10", "Test11", "Test12", "Test13", "Test14", "Test15", "Test16", "Test17", "Test18", "Test19", "Test20", "Test21", "Test22", "Test23", "Test24", "Test25", "Test26" };
        for (int i = 0; i < strs.length; ++i) {
            kw.add(strs[i]);
        }
        sDesign.tags = kw;
        sDesign.price = 13.33;
        this.checkDetailsPageHelper(d, sDesign);
        sDesign.title = "This is a title which is way too long the rest ...";
        sDesign.description = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxThis is a title which is way too long the rest ...";
        sDesign.tags.remove(sDesign.tags.size() - 1);
        this.checkSpreadListing(sDesign.title, price, sDesign.description, 25, products, d, kw);
    }
    
    private void checkDetailsPageHelperSpecialCharacter(final Driver d, final SpreadshirtDesign sDesign, final String price, final String products) {
        sDesign.title = "!@#$%^&*()-=+[{]}|'\";:/?.>,<`~";
        sDesign.description = "!@#$%^&*()-=+[{]}|'\";:/?.>,<`~";
        final List<String> kw = new ArrayList<String>();
        final String[] strs = { "!@#$%^&*()-=+[{]}|'\";:/?.>", "<`~", "^^^^" };
        for (int i = 0; i < strs.length; ++i) {
            kw.add(strs[i]);
        }
        sDesign.tags = kw;
        sDesign.price = 6.54;
        this.checkDetailsPageHelper(d, sDesign);
        this.checkSpreadListing(sDesign.title, price, sDesign.description, 3, products, d, kw);
    }
    
    private void checkDetailsPageHelper(final Driver d, final SpreadshirtDesign sDesign) {
        sDesign.templateName = "OneProduct";
        this.checkIfUplaodNeeded(d, sDesign);
        sDesign.selectDesignPageTest();
        sDesign.templatePageTest();
        sDesign.detailsPageTest();
    }
    
    private static void delteAllDesigns(final Driver d) {
        d.openUrl();
        d.driverWait(10);
        d.waitAll();
        d.waitUntilElementVisible(By.id("upload-btn"));
        for (long l = d.getLength("getElementsByClassName('card design-tile')"); !d.checkPresence("getElementsByClassName('rocket-image')[0]") && l > 0L; l = d.getLength("getElementsByClassName('card design-tile')")) {
            d.waitUntilElementVisible(By.xpath("//*[@class='link-green select-link']"));
            d.click("getElementsByClassName('link-green select-link')[0]");
            d.waitUntilElementVisible(By.id("design-delete-button"));
            d.click("getElementById('design-list-delete-nav-item').children[0]");
            d.waitUntilElementVisible(By.xpath("//*[@class='btn-progress btn btn-lg btn-danger btn-progress IDLE null']"));
            d.clickVue("getElementsByClassName('btn-progress btn btn-lg btn-danger btn-progress IDLE null')[0]");
            d.waitAll();
            d.driverWait(10);
            d.waitAll();
            d.openUrl();
            d.driverWait(10);
            d.waitAll();
        }
    }
    
    public void checkSpreadListing(final String title, final String price, final String desc, final int tagsSize, final String numberOfProducts, final Driver d, final List<String> tags) {
        d.openUrl();
        d.driverWait(10);
        d.waitUntilElementVisible(By.xpath("//*[@class='idea-preview-image preview-image design-preview']"));
        d.click("getElementsByClassName('idea-preview-image preview-image design-preview')[0]");
        d.driverWait(10);
        d.waitAll();
        try {
            Thread.sleep(1000L);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(title, d.getTextContent("getElementsByTagName('strong')[0]"));
        Assert.assertEquals(price, d.getTextContent("getElementsByTagName('strong')[1]"));
        Assert.assertEquals(desc, d.getTextContent("getElementsByTagName('p')[0]"));
        final long l = d.getLength("getElementsByClassName('tag-list')[0].children");
        Assert.assertEquals(tagsSize, l);
        for (int i = 0; i < tagsSize; ++i) {
            Assert.assertEquals(tags.get(i), d.getTextContent("getElementsByClassName('tag-list')[0].children[" + i + "]"));
        }
        Assert.assertEquals(numberOfProducts, d.getTextContent("getElementsByTagName('strong')[3]"));
    }
    
    private void checkIfUplaodNeeded(final Driver d, final SpreadshirtDesign sDesign) {
        d.openUrl();
        d.waitAll();
        d.waitUntilElementVisible(By.id("upload-btn"));
        if (d.checkPresence("getElementsByClassName('rocket-image')[0]")) {
            sDesign.uploadPageTest();
        }
        else {
            d.waitUntilElementVisible(By.xpath("//*[@class='idea-preview-image preview-image design-preview']"));
            d.click("getElementsByClassName('idea-preview-image preview-image design-preview')[0]");
            d.driverWait(10);
            try {
                Thread.sleep(1000L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (d.checkPresence("getElementsByClassName('idea-summary')[0]")) {
                d.openUrl();
                d.driverWait(10);
                sDesign.uploadPageTest();
            }
            else {
                d.openUrl();
                d.driverWait(10);
            }
        }
    }
}
