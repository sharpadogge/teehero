// 
// Decompiled by Procyon v0.5.36
// 

package Util.SpecialActions;

import javafx.event.Event;
import Util.Enums.OptionNamesInConfig;
import Util.Config.ConfigHandler;
import java.util.Iterator;
import GUI.DesignFrames.ShirteeDesignFrame;
import Util.Designs.ShirteeDesign;
import GUI.DesignFrames.SpreadshirtComDesignFrame;
import GUI.DesignFrames.SpreadshirtEuDesignFrame;
import Util.Designs.SpreadshirtDesign;
import GUI.DesignFrames.DesignFrame;
import java.util.Map;
import GUI.DesignFrames.DefaultDesignFrame;
import GUI.DesignArea.DesignArea;
import java.util.HashMap;
import javafx.scene.control.ComboBox;
import java.lang.reflect.Field;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import Util.Designs.Design;

public class SpecialComboBoxChangePatternAction<DesignType extends Design> implements EventHandler<ActionEvent>
{
    private String property;
    private Field[] classFields;
    private Field[] superclassFields;
    private DesignType design;
    private ComboBox<String> combo;
    private HashMap<String, DesignArea> designAreas;
    
    public SpecialComboBoxChangePatternAction(final HashMap<String, DesignArea> designAreas, final String property, final DesignType design, final ComboBox<String> combo) {
        this.property = property;
        this.classFields = design.getClass().getDeclaredFields();
        this.superclassFields = design.getClass().getSuperclass().getDeclaredFields();
        this.design = design;
        this.combo = combo;
        this.designAreas = designAreas;
    }
    
    public void handle(final ActionEvent event) {
        if (this.combo.getValue() != null) {
            ((DefaultDesignFrame)this.combo.getParent()).setTeeHeroPattern(((String)this.combo.getValue()).toString());
            for (final Map.Entry<String, DesignArea> entry : this.designAreas.entrySet()) {
                if (this.combo.getValue() != null) {
                    for (final DesignFrame df : entry.getValue().getDesignFrameList()) {
                        if (this.design.designId == df.getDesign().designId) {
                            Field[] classFields;
                            for (int length = (classFields = this.classFields).length, i = 0; i < length; ++i) {
                                final Field f = classFields[i];
                                if (f.getName().toUpperCase().equals(this.property.toUpperCase())) {
                                    try {
                                        if (this.design.getClass() == SpreadshirtDesign.class) {
                                            if (df.getClass() == SpreadshirtEuDesignFrame.class) {
                                                this.setProperPattern(f, ((SpreadshirtEuDesignFrame)df).spreadshirtDesign, "eu");
                                                break;
                                            }
                                            if (df.getClass() == SpreadshirtComDesignFrame.class) {
                                                this.setProperPattern(f, ((SpreadshirtComDesignFrame)df).spreadshirtDesign, "com");
                                                break;
                                            }
                                            break;
                                        }
                                        else {
                                            if (this.design.getClass() == ShirteeDesign.class && df.getClass() == ShirteeDesignFrame.class) {
                                                this.setProperPattern(f, ((ShirteeDesignFrame)df).shirteeDesign);
                                                break;
                                            }
                                            break;
                                        }
                                    }
                                    catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            Field[] superclassFields;
                            for (int length2 = (superclassFields = this.superclassFields).length, j = 0; j < length2; ++j) {
                                final Field f = superclassFields[j];
                                if (f.getName().toUpperCase().equals(this.property.toUpperCase())) {
                                    try {
                                        if (this.design.getClass() == SpreadshirtDesign.class) {
                                            if (df.getClass() == SpreadshirtEuDesignFrame.class) {
                                                this.setProperPattern(f, ((SpreadshirtEuDesignFrame)df).spreadshirtDesign, "eu");
                                                break;
                                            }
                                            if (df.getClass() == SpreadshirtComDesignFrame.class) {
                                                this.setProperPattern(f, ((SpreadshirtComDesignFrame)df).spreadshirtDesign, "com");
                                                break;
                                            }
                                            break;
                                        }
                                        else {
                                            if (this.design.getClass() == ShirteeDesign.class && df.getClass() == ShirteeDesignFrame.class) {
                                                this.setProperPattern(f, ((ShirteeDesignFrame)df).shirteeDesign);
                                                break;
                                            }
                                            break;
                                        }
                                    }
                                    catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void setProperPattern(final Field f, final ShirteeDesign d) {
        final String value = this.findProperSettingValue(this.combo);
        try {
            f.set(d, value);
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }
    
    private void setProperPattern(final Field f, final SpreadshirtDesign d, final String type) {
        final String value = this.findProperSettingValue(this.combo, type);
        try {
            f.set(d, value);
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }
    
    private String findProperSettingValue(final ComboBox<String> combo) {
        final ConfigHandler ch = new ConfigHandler();
        return this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SHIRTEE_DESIGNID_COLOR_CASCADE.getOptionName()), ((String)combo.getValue()).toString());
    }
    
    private String findProperSettingValue(final ComboBox<String> combo, final String type) {
        final ConfigHandler ch = new ConfigHandler();
        switch (type) {
            case "eu": {
                return this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SPREADSHIRT_EU_PATTERN_CASCADE.getOptionName()), ((String)combo.getValue()).toString());
            }
            case "com": {
                return this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SPREADSHIRT_COM_PATTERN_CASCADE.getOptionName()), ((String)combo.getValue()).toString());
            }
            default:
                break;
        }
        return "";
    }
    
    private String findDefaultPattern(final String valueString, final String searchPattern) {
        if (valueString != null && valueString.length() > 0) {
            final String[] values = valueString.split(",");
            final String[] array;
            final int length = (array = values).length;
            int i = 0;
            while (i < length) {
                final String s = array[i];
                final String[] keyValuePair = s.split(":");
                if (keyValuePair[0].substring(1, keyValuePair[0].length()).equalsIgnoreCase(searchPattern)) {
                    if (keyValuePair[1].substring(keyValuePair[1].length() - 1, keyValuePair[1].length()).equals("]")) {
                        return keyValuePair[1].substring(0, keyValuePair[1].length() - 1);
                    }
                    return keyValuePair[1].substring(0, keyValuePair[1].length());
                }
                else {
                    ++i;
                }
            }
        }
        return "";
    }
}
