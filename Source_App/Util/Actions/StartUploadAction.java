// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import java.util.Iterator;
import javafx.event.Event;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.concurrent.WorkerStateEvent;
import Util.Sessions.SessionTask;
import GUI.DesignFrames.DesignFrame;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import Util.Enums.DesignAreaDefinition;
import java.util.Map;
import Util.Config.ConfigHandler;
import Util.Analytics.Analyser;
import GUI.Pages.MainPage;
import GUI.DesignArea.DesignArea;
import java.util.HashMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class StartUploadAction implements EventHandler<ActionEvent>
{
    private HashMap<String, DesignArea> designAreas;
    private MainPage mp;
    private Analyser analyser;
    
    public StartUploadAction(final HashMap<String, DesignArea> designAreas, final MainPage mp) {
        this.designAreas = designAreas;
        this.mp = mp;
        this.analyser = new Analyser(mp.getUser());
    }
    
    public void handle(final ActionEvent arg0) {
        final ConfigHandler cm = new ConfigHandler();
        for (final Map.Entry<String, DesignArea> entry : this.designAreas.entrySet()) {
            if (!entry.getValue().getDesignFrameList().isEmpty()) {
                System.out.println(entry.getKey().toUpperCase());
                final DesignAreaDefinition area = DesignAreaDefinition.valueOf(entry.getKey().toUpperCase());
                ((Button)arg0.getSource()).setCursor(Cursor.WAIT);
                switch (area) {
                    case SPREADSHIRT_EU_AREA: {
                        if (!this.mp.getBtnSpreadshirtEu().isDisabled()) {
                            final SessionTask sprdEuTask = new SessionTask(area, this.mp, entry.getValue().getDesignFrameList());
                            final Thread sprdEuThread = new Thread((Runnable)sprdEuTask);
                            sprdEuThread.start();
                            sprdEuTask.setOnSucceeded((EventHandler)new EventHandler<WorkerStateEvent>() {
                                public void handle(final WorkerStateEvent event) {
                                    final Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.setHeaderText((String)null);
                                    alert.setContentText("Upload of Spreadshirt.eu is completed");
                                    alert.setTitle("Spreadshirt.eu");
                                    alert.show();
                                    final Stage stage = (Stage)alert.getDialogPane().getScene().getWindow();
                                    stage.setAlwaysOnTop(true);
                                    StartUploadAction.this.writeAnalytics(entry.getValue().getDesignFrameList().size(), area.getAreaName(), "success");
                                }
                            });
                            sprdEuTask.setOnFailed((EventHandler)new EventHandler<WorkerStateEvent>() {
                                public void handle(final WorkerStateEvent event) {
                                    final Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.setHeaderText((String)null);
                                    alert.setContentText("We were unable to at least upload one of your designs");
                                    alert.setTitle("Spreadshirt.eu");
                                    alert.show();
                                    final Stage stage = (Stage)alert.getDialogPane().getScene().getWindow();
                                    stage.setAlwaysOnTop(true);
                                    StartUploadAction.this.writeAnalytics(entry.getValue().getDesignFrameList().size(), area.getAreaName(), "fail");
                                }
                            });
                            break;
                        }
                        break;
                    }
                    case SPREADSHIRT_COM_AREA: {
                        if (!this.mp.getBtnSpreadshirtCom().isDisabled()) {
                            final SessionTask sprdComTask = new SessionTask(area, this.mp, entry.getValue().getDesignFrameList());
                            final Thread sprdComThread = new Thread((Runnable)sprdComTask);
                            sprdComThread.start();
                            sprdComTask.setOnSucceeded((EventHandler)new EventHandler<WorkerStateEvent>() {
                                public void handle(final WorkerStateEvent event) {
                                    final Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.setHeaderText((String)null);
                                    alert.setContentText("Upload of Spreadshirt.com is completed");
                                    alert.setTitle("Spreadshirt.com");
                                    alert.show();
                                    final Stage stage = (Stage)alert.getDialogPane().getScene().getWindow();
                                    stage.setAlwaysOnTop(true);
                                    StartUploadAction.this.writeAnalytics(entry.getValue().getDesignFrameList().size(), area.getAreaName(), "success");
                                }
                            });
                            sprdComTask.setOnFailed((EventHandler)new EventHandler<WorkerStateEvent>() {
                                public void handle(final WorkerStateEvent event) {
                                    final Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.setHeaderText((String)null);
                                    alert.setContentText("We were unable to at least upload one of your designs");
                                    alert.setTitle("Spreadshirt.com");
                                    alert.show();
                                    final Stage stage = (Stage)alert.getDialogPane().getScene().getWindow();
                                    stage.setAlwaysOnTop(true);
                                    StartUploadAction.this.writeAnalytics(entry.getValue().getDesignFrameList().size(), area.getAreaName(), "fail");
                                }
                            });
                            break;
                        }
                        break;
                    }
                    case SHIRTEE_AREA: {
                        if (!this.mp.getBtnShirtee().isDisabled()) {
                            final SessionTask shirteeTask = new SessionTask(area, this.mp, entry.getValue().getDesignFrameList());
                            final Thread shirteeThread = new Thread((Runnable)shirteeTask);
                            shirteeThread.start();
                            shirteeTask.setOnSucceeded((EventHandler)new EventHandler<WorkerStateEvent>() {
                                public void handle(final WorkerStateEvent event) {
                                    final Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.setHeaderText((String)null);
                                    alert.setContentText("Upload of Shirtee is completed");
                                    alert.setTitle("Shirtee");
                                    alert.show();
                                    final Stage stage = (Stage)alert.getDialogPane().getScene().getWindow();
                                    stage.setAlwaysOnTop(true);
                                    StartUploadAction.this.writeAnalytics(entry.getValue().getDesignFrameList().size(), area.getAreaName(), "success");
                                }
                            });
                            shirteeTask.setOnFailed((EventHandler)new EventHandler<WorkerStateEvent>() {
                                public void handle(final WorkerStateEvent event) {
                                    final Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.setHeaderText((String)null);
                                    alert.setContentText("We were unable to at least upload one of your designs");
                                    alert.setTitle("Shirtee");
                                    alert.show();
                                    final Stage stage = (Stage)alert.getDialogPane().getScene().getWindow();
                                    stage.setAlwaysOnTop(true);
                                    StartUploadAction.this.writeAnalytics(entry.getValue().getDesignFrameList().size(), area.getAreaName(), "fail");
                                }
                            });
                            break;
                        }
                        break;
                    }
                }
                ((Button)arg0.getSource()).setCursor(Cursor.DEFAULT);
                this.mp.getPreUploadCheckBox().fire();
            }
        }
    }
    
    private void writeAnalytics(final int numberOfRecords, final String areaName, final String operation) {
        switch (operation) {
            case "success": {
                new Thread(() -> {
                    try {
                        this.analyser.writeUploadSuccessReportToServer(numberOfRecords, areaName);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }).start();
                break;
            }
            case "fail": {
                new Thread(() -> {
                    try {
                        this.analyser.writeUploadFailReportToServer(numberOfRecords, areaName);
                    }
                    catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    return;
                }).start();
                break;
            }
            default:
                break;
        }
    }
}
