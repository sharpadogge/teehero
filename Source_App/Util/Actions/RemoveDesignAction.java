// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import GUI.DesignArea.DesignArea;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import GUI.DesignFrames.DesignFrame;

public class RemoveDesignAction<T extends DesignFrame> implements EventHandler<ActionEvent>
{
    private T df;
    private DesignArea<T> da;
    
    public RemoveDesignAction(final T df, final DesignArea da) {
        this.df = df;
        this.da = (DesignArea<T>)da;
    }
    
    public void handle(final ActionEvent arg0) {
        try {
            this.da.removeDesign(this.df);
            this.da.updateVisualDesignList();
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
