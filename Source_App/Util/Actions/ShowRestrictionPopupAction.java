// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import javafx.stage.StageStyle;
import javafx.scene.control.Alert;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ShowRestrictionPopupAction implements EventHandler<ActionEvent>
{
    private String header;
    private String[] restrictions;
    
    public ShowRestrictionPopupAction(final String header, final String[] restrictions) {
        this.header = header;
        this.restrictions = restrictions;
    }
    
    public void handle(final ActionEvent arg0) {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Platform Restrictions");
        alert.setHeaderText(this.header);
        alert.setContentText(String.join("\n", (CharSequence[])this.restrictions));
        alert.initStyle(StageStyle.UTILITY);
        alert.showAndWait();
    }
}
