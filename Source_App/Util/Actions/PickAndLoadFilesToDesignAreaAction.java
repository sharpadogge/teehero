// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.util.Iterator;
import java.util.ArrayList;
import javafx.scene.control.Alert;
import GUI.DesignFrames.ShirteeDesignFrame;
import Util.Designs.ShirteeDesign;
import GUI.DesignFrames.SpreadshirtComDesignFrame;
import GUI.DesignArea.DesignArea;
import GUI.DesignFrames.SpreadshirtEuDesignFrame;
import Util.Designs.SpreadshirtDesign;
import GUI.DesignFrames.DefaultDesignFrame;
import Util.Designs.Design;
import Util.Canvas.ImageCanvas;
import java.io.File;
import Util.Enums.DesignAreaDefinition;
import Util.FilePicker.FilePicker;
import javafx.scene.layout.Pane;
import Util.Config.ConfigHandler;
import GUI.Pages.MainPage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class PickAndLoadFilesToDesignAreaAction implements EventHandler<ActionEvent>
{
    private String[] formats;
    private MainPage mp;
    private ConfigHandler ch;
    
    public PickAndLoadFilesToDesignAreaAction(final String[] formats, final Pane designContainer, final MainPage mp) {
        this.formats = formats;
        this.mp = mp;
        this.ch = new ConfigHandler();
    }
    
    public void handle(final ActionEvent arg0) {
        final FilePicker fp = new FilePicker(this.formats);
        final ArrayList<File> af = fp.getUploadableFilesFromSelectedDirectory();
        if (af == null || af.isEmpty()) {
            return;
        }
        final DesignArea<DefaultDesignFrame> dda = (DesignArea<DefaultDesignFrame>)this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.DEFAULT_AREA.getAreaName());
        final DesignArea<SpreadshirtEuDesignFrame> sda = (DesignArea<SpreadshirtEuDesignFrame>)this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.SPREADSHIRT_EU_AREA.getAreaName());
        final DesignArea<SpreadshirtComDesignFrame> scda = (DesignArea<SpreadshirtComDesignFrame>)this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.SPREADSHIRT_COM_AREA.getAreaName());
        final DesignArea<ShirteeDesignFrame> stda = (DesignArea<ShirteeDesignFrame>)this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.SHIRTEE_AREA.getAreaName());
        this.mp.getProgressBar().setProgress(0.0);
        this.mp.getProgressBar().setVisible(true);
        for (final File f : af) {
            final int id = this.mp.getDesignAreaHandler().getDesignId();
            final String fileName = f.getName().substring(0, f.getName().length() - (f.getName().length() - f.getName().lastIndexOf(".")));
            ImageCanvas ic1 = new ImageCanvas(f);
            final Design d = new Design(f, id);
            d.title = fileName;
            final DefaultDesignFrame ddf = new DefaultDesignFrame(d, dda, ic1);
            ddf.tbTitle.setText(fileName);
            dda.addDesign(ddf);
            ImageCanvas ic2 = ic1.getCloneOfImageCanvas();
            final SpreadshirtDesign sd = new SpreadshirtDesign(f, id, "eu");
            sd.title = fileName;
            final SpreadshirtEuDesignFrame sdf = new SpreadshirtEuDesignFrame(sd, sda, ic2);
            sda.addDesign(sdf);
            ImageCanvas ic3 = ic1.getCloneOfImageCanvas();
            final SpreadshirtDesign scd = new SpreadshirtDesign(f, id, "com");
            scd.title = fileName;
            final SpreadshirtComDesignFrame scdf = new SpreadshirtComDesignFrame(scd, scda, ic3);
            scda.addDesign(scdf);
            ImageCanvas ic4 = ic1.getCloneOfImageCanvas();
            final ShirteeDesign std = new ShirteeDesign(f, id);
            std.title = fileName;
            final ShirteeDesignFrame stdf = new ShirteeDesignFrame(std, stda, ic4);
            stda.addDesign(stdf);
            ic2 = (ic1 = (ic3 = (ic4 = null)));
            this.updateProgress((af.indexOf(f) + 1) / af.size());
        }
        this.mp.getBtnAll().setOnAction((EventHandler)new ChangeSelectionStateOfAllDesignsAction(this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.DEFAULT_AREA.getAreaName()), true));
        this.mp.getBtnNone().setOnAction((EventHandler)new ChangeSelectionStateOfAllDesignsAction(this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.DEFAULT_AREA.getAreaName()), false));
        this.mp.getBtnGeneral().fire();
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Upload completed");
        alert.setHeaderText((String)null);
        alert.setTitle("Upload completed");
        alert.showAndWait();
        this.mp.getProgressBar().setVisible(false);
        System.runFinalization();
        System.gc();
    }
    
    private void updateProgress(final int val) {
        this.mp.getProgressBar().setProgress((double)val);
    }
    
    private String findDefaultPattern(final String valueString, final String searchPattern) {
        if (valueString != null && valueString.length() > 0) {
            final String[] values = valueString.split(",");
            String[] array;
            for (int length = (array = values).length, i = 0; i < length; ++i) {
                final String s = array[i];
                final String[] keyValuePair = s.split(":");
                if (keyValuePair[0].substring(1, keyValuePair[0].length()).equalsIgnoreCase(searchPattern)) {
                    return keyValuePair[1].substring(0, keyValuePair[1].length() - 1);
                }
            }
        }
        return "";
    }
}
