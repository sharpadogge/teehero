// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.util.Iterator;
import Util.Config.ConfigHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import GUI.Pages.MainPage;
import java.util.List;
import javafx.scene.control.Control;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ChangeApplyBtnFunctionality implements EventHandler<ActionEvent>
{
    private EventHandler<ActionEvent> action;
    private Button applyBtn;
    private Control applyComponent;
    private String inputField;
    private List<Button> btnList;
    private MainPage mp;
    private String on;
    
    public ChangeApplyBtnFunctionality(final EventHandler<ActionEvent> action, final Button applyBtn, final Control applyCompnent, final List<Button> btnList, final String inputField, final String on, final MainPage mp) {
        this.action = action;
        this.applyBtn = applyBtn;
        this.applyComponent = applyCompnent;
        this.btnList = btnList;
        this.inputField = inputField;
        this.mp = mp;
        this.on = on;
    }
    
    public void handle(final ActionEvent arg0) {
        this.applyBtn.setOnAction((EventHandler)null);
        for (final Button b : this.btnList) {
            b.setStyle("-fx-text-fill: #666666");
        }
        if (this.inputField.toUpperCase().equals("TEXTBOX")) {
            this.hideAllInputComponents();
            ((Button)arg0.getSource()).setStyle("-fx-text-fill: #e60000");
            this.applyBtn.setOnAction((EventHandler)this.action);
            this.applyComponent.setVisible(true);
            this.applyComponent.requestFocus();
            ((TextField)this.applyComponent).selectAll();
        }
        else if (this.inputField.toUpperCase().equals("COMBOBOX")) {
            this.hideAllInputComponents();
            ((Button)arg0.getSource()).setStyle("-fx-text-fill: #e60000");
            this.applyBtn.setOnAction((EventHandler)this.action);
            ((ComboBox)this.applyComponent).getItems().clear();
            final String valueString = new ConfigHandler().readSetting(this.on);
            if (valueString != null && valueString.length() > 0) {
                final String[] values = valueString.split(",");
                String[] array;
                for (int length = (array = values).length, i = 0; i < length; ++i) {
                    final String s = array[i];
                    ((ComboBox)this.applyComponent).getItems().add((Object)s);
                }
            }
            this.applyComponent.setVisible(true);
            this.applyComponent.requestFocus();
        }
    }
    
    private void hideAllInputComponents() {
        this.mp.getTbApplyValue().setVisible(false);
        this.mp.getComboBoxApplyValue().setVisible(false);
    }
}
