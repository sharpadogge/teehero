// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.util.Iterator;
import GUI.DesignFrames.DesignFrame;
import java.lang.reflect.Field;
import javafx.scene.control.TextField;
import GUI.DesignArea.DesignArea;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ApplyToAllTextFieldsOfCheckedDesignsAction implements EventHandler<ActionEvent>
{
    private DesignArea da;
    private String componentName;
    private TextField tf;
    private Field[] classFields;
    private Field[] superclassFields;
    private Field[] supersuperclassFields;
    
    public ApplyToAllTextFieldsOfCheckedDesignsAction(final DesignArea da, final String componentName, final TextField tf) {
        this.da = da;
        this.componentName = componentName;
        this.tf = tf;
    }
    
    public void handle(final ActionEvent arg0) {
        for (final DesignFrame df : this.da.getDesignFrameList()) {
            if (df.getCheckBox().isSelected()) {
                this.classFields = df.getClass().getDeclaredFields();
                this.superclassFields = df.getClass().getSuperclass().getDeclaredFields();
                this.supersuperclassFields = df.getClass().getSuperclass().getSuperclass().getDeclaredFields();
                Field[] classFields;
                for (int length = (classFields = this.classFields).length, i = 0; i < length; ++i) {
                    final Field f = classFields[i];
                    if (f.getName().toUpperCase().equals(this.componentName.toUpperCase())) {
                        try {
                            ((TextField)f.get(df)).setText(this.tf.getText());
                            break;
                        }
                        catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        catch (IllegalAccessException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                Field[] superclassFields;
                for (int length2 = (superclassFields = this.superclassFields).length, j = 0; j < length2; ++j) {
                    final Field f = superclassFields[j];
                    if (f.getName().toUpperCase().equals(this.componentName.toUpperCase())) {
                        try {
                            ((TextField)f.get(df)).setText(this.tf.getText());
                            break;
                        }
                        catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        catch (IllegalAccessException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                Field[] supersuperclassFields;
                for (int length3 = (supersuperclassFields = this.supersuperclassFields).length, k = 0; k < length3; ++k) {
                    final Field f = supersuperclassFields[k];
                    if (f.getName().toUpperCase().equals(this.componentName.toUpperCase())) {
                        try {
                            ((TextField)f.get(df)).setText(this.tf.getText());
                            break;
                        }
                        catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        catch (IllegalAccessException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
