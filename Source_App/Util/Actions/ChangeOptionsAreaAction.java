// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.util.Iterator;
import GUI.OptionArea.OptionsArea;
import javafx.scene.control.Button;
import java.util.ArrayList;
import GUI.Pages.OptionsPage;
import javafx.scene.layout.Pane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ChangeOptionsAreaAction implements EventHandler<ActionEvent>
{
    private Pane optionsAreaContainer;
    private OptionsPage op;
    private String optionsAreaName;
    private ArrayList<Button> btnList;
    
    public ChangeOptionsAreaAction(final String optionsAreaName, final Pane optionsAreaContainer, final OptionsPage op, final ArrayList<Button> btnList) {
        this.optionsAreaContainer = optionsAreaContainer;
        this.op = op;
        this.optionsAreaName = optionsAreaName;
        this.btnList = btnList;
    }
    
    public void handle(final ActionEvent arg0) {
        this.optionsAreaContainer.getChildren().clear();
        final OptionsArea oa = this.op.getOptionsAreaHandler().getOptionsArea(this.optionsAreaName);
        this.optionsAreaContainer.getChildren().add((Object)oa);
        try {
            oa.updateVisualOptionsList();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.activateButton((Button)arg0.getSource());
    }
    
    private void restoreDefaultButtonAppearance() {
        for (final Button btn : this.btnList) {
            btn.setStyle("-fx-text-fill: #666666");
        }
    }
    
    private void activateButton(final Button btn) {
        this.restoreDefaultButtonAppearance();
        btn.setStyle("-fx-text-fill: #e60000");
    }
}
