// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import Util.Enums.OptionNamesInConfig;
import java.util.Iterator;
import java.util.HashMap;
import GUI.OptionFrame.OptionFrame;
import java.util.Map;
import javafx.scene.control.Alert;
import GUI.OptionArea.OptionsArea;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class AddNewPatternAction implements EventHandler<ActionEvent>
{
    private TextField textfield;
    private ComboBox<String> comPattern;
    private String[] connectedOptions;
    private OptionsArea oa;
    private String optionName;
    
    public AddNewPatternAction(final TextField textfield, final ComboBox<String> comPattern, final String[] connectedOptions, final String optionName, final OptionsArea oa) {
        this.textfield = textfield;
        this.comPattern = comPattern;
        this.connectedOptions = connectedOptions;
        this.oa = oa;
        this.optionName = optionName;
    }
    
    public void handle(final ActionEvent arg0) {
        if (this.comPattern.getItems().size() >= 20) {
            final Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Not more than 20 patterns");
            alert.setTitle("Too many");
            alert.showAndWait();
            return;
        }
        for (int i = 0; i < this.comPattern.getItems().size(); ++i) {
            if (((String)this.comPattern.getItems().get(i)).toUpperCase().equals(this.textfield.getText().toUpperCase())) {
                final Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
                alert2.setContentText("This pattern is already in the list");
                alert2.setTitle("Double");
                alert2.showAndWait();
                return;
            }
        }
        if (this.textfield.getText().length() > 0) {
            this.comPattern.getItems().add((Object)this.textfield.getText());
            String[] connectedOptions;
            for (int length = (connectedOptions = this.connectedOptions).length, j = 0; j < length; ++j) {
                final String s = connectedOptions[j];
                final HashMap<String, OptionsArea> optionAreas = this.oa.getOpionPage().getOptionsAreaHandler().getOptionsAreas();
                for (final Map.Entry<String, OptionsArea> entry : optionAreas.entrySet()) {
                    for (final OptionFrame of : entry.getValue().getOptionFrameList()) {
                        if (of.getOptionName().equals(s)) {
                            if (this.isTeeHeroDesign(this.optionName)) {
                                this.addToComboBox(of.getTeeHeroComboBox(), this.textfield.getText());
                            }
                            else {
                                this.addToComboBox(of.getPlatformComboBox(), this.textfield.getText());
                            }
                            this.addToComboBox(of.getDefaultComboBox(), this.textfield.getText());
                        }
                    }
                }
            }
        }
        this.textfield.setText((String)null);
        this.textfield.requestFocus();
    }
    
    private void addToComboBox(final ComboBox combo, final String text) {
        if (combo != null) {
            combo.getItems().add((Object)text);
        }
    }
    
    private boolean isTeeHeroDesign(final String s) {
        return s.equals(OptionNamesInConfig.GEN_TEEHERO_PATTERN.getOptionName());
    }
}
