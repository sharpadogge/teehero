// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import javax.xml.transform.Transformer;
import java.util.Iterator;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javafx.scene.control.Alert;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import org.w3c.dom.Node;
import GUI.DesignFrames.DesignFrame;
import GUI.DesignArea.DesignArea;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javafx.stage.Window;
import javafx.stage.FileChooser;
import java.io.File;
import GUI.Pages.MainPage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ExportDataToFileAction implements EventHandler<ActionEvent>
{
    private MainPage mp;
    private File exportFile;
    
    public ExportDataToFileAction(final MainPage mp) {
        this.mp = mp;
    }
    
    public void handle(final ActionEvent arg0) {
        this.selectExportFile();
        this.writeXMLToFile();
    }
    
    private void selectExportFile() {
        final FileChooser fc = new FileChooser();
        fc.setTitle("Choose a existing file or enter a name");
        fc.getExtensionFilters().add((Object)new FileChooser.ExtensionFilter("TeeHero-File", new String[] { "*.teehero" }));
        this.exportFile = fc.showSaveDialog((Window)this.mp.getStage());
    }
    
    private void writeXMLToFile() {
        try {
            final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            final Document doc = dBuilder.newDocument();
            final Element rootElement = doc.createElement("DESIGNDATA");
            for (final Map.Entry<String, DesignArea> da : this.mp.getDesignAreaHandler().getDesignAreas().entrySet()) {
                final Element designArea = doc.createElement(da.getValue().getName());
                for (final DesignFrame df : da.getValue().getDesignFrameList()) {
                    designArea.appendChild(df.getXMLExportElement(doc));
                }
                rootElement.appendChild(designArea);
            }
            doc.appendChild(rootElement);
            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            final Transformer transformer = transformerFactory.newTransformer();
            final DOMSource source = new DOMSource(doc);
            final StreamResult result = new StreamResult(this.exportFile);
            transformer.transform(source, result);
            final Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText((String)null);
            alert.setTitle("Export complete");
            alert.setContentText("The current data was exported to: \n\n" + this.exportFile);
            alert.showAndWait();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
