// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import Util.OptionValues.KeyValuePair;
import javafx.scene.control.ComboBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RemoveFromCascadeFieldAction implements EventHandler<ActionEvent>
{
    private ComboBox<KeyValuePair> comPattern;
    private ComboBox<String> comTeeHeroPattern;
    private ComboBox<String> comUserPattern;
    
    public RemoveFromCascadeFieldAction(final ComboBox<String> comUserPattern, final ComboBox<String> comTeeHeroPattern, final ComboBox<KeyValuePair> comPattern) {
        this.comPattern = comPattern;
        this.comTeeHeroPattern = comTeeHeroPattern;
        this.comUserPattern = comUserPattern;
    }
    
    public void handle(final ActionEvent e) {
        if (this.comPattern.getValue() != null) {
            this.comTeeHeroPattern.getItems().add((Object)((KeyValuePair)this.comPattern.getValue()).getTeeHeroPattern());
            this.comUserPattern.getItems().add((Object)((KeyValuePair)this.comPattern.getValue()).getOtherPlatformPattern());
            this.comPattern.getItems().remove(this.comPattern.getValue());
        }
    }
}
