// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import GUI.DesignArea.DesignArea;
import GUI.DesignFrames.DesignFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RemoveDesignFromAllDesignAreasAction implements EventHandler<ActionEvent>
{
    private DesignFrame df;
    private DesignArea da;
    
    public RemoveDesignFromAllDesignAreasAction(final DesignFrame df, final DesignArea da) {
        this.df = df;
        this.da = da;
    }
    
    public void handle(final ActionEvent arg0) {
        final int designIndex = this.df.getDesign().getDesignId();
        try {
            this.da.getMainPage().getDesignAreaHandler().removeDesignFromAllDesignAreas(designIndex);
            this.da.updateVisualDesignList();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
