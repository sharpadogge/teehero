// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import Util.OptionValues.SpecialValueTrio;
import javafx.scene.control.ComboBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RemoveFromCascadeTrioFieldAction implements EventHandler<ActionEvent>
{
    private ComboBox<SpecialValueTrio> comPattern;
    private ComboBox<String> comTeeHeroPattern;
    private ComboBox<String> comUserPattern;
    private ComboBox<String> comUserColor;
    
    public RemoveFromCascadeTrioFieldAction(final ComboBox<String> comUserPattern, final ComboBox<String> comTeeHeroPattern, final ComboBox<SpecialValueTrio> comPattern, final ComboBox<String> comUserColor) {
        this.comPattern = comPattern;
        this.comTeeHeroPattern = comTeeHeroPattern;
        this.comUserPattern = comUserPattern;
        this.comUserColor = comUserColor;
    }
    
    public void handle(final ActionEvent e) {
        if (this.comPattern.getValue() != null) {
            this.comTeeHeroPattern.getItems().add((Object)((SpecialValueTrio)this.comPattern.getValue()).getTeeHeroPattern());
            this.comUserPattern.getItems().add((Object)((SpecialValueTrio)this.comPattern.getValue()).getShirteePattern());
            this.comPattern.getItems().remove(this.comPattern.getValue());
        }
    }
}
