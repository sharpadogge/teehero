// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import Util.ServerRequests.LoginRequest;
import javafx.scene.Scene;
import javafx.stage.Stage;
import GUI.Pages.MainPage;
import javafx.scene.layout.BorderPane;
import GUI.Window.Window;
import Util.User.User;
import java.util.Date;
import GUI.Pages.LoginPage;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class LoginAction implements EventHandler<ActionEvent>
{
    private TextField tfUsername;
    private PasswordField tfPassword;
    private LoginPage lp;
    
    public LoginAction(final TextField tfEmail, final PasswordField tfPassword, final LoginPage lp) {
        this.tfUsername = tfEmail;
        this.tfPassword = tfPassword;
        this.lp = lp;
    }
    
    public void handle(final ActionEvent arg0) {
        final String password = this.tfPassword.getText();
        if (this.isLoginCorrect(this.tfUsername.getText(), password.toString())) {
            final User user = new User(this.tfUsername.getText(), new Date());
            final Window window = new Window();
            window.setScene((Scene)new MainPage(new BorderPane(), window, user));
            this.lp.getStage().close();
            window.show();
        }
    }
    
    private boolean isLoginCorrect(final String email, final String password) {
        final LoginRequest connector = new LoginRequest(email, password);
        return connector.isLoginValid();
    }
}
