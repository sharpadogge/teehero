// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.util.Iterator;
import java.util.List;
import GUI.DesignFrames.DesignFrame;
import GUI.DesignArea.DesignArea;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ChangeSelectionStateOfAllDesignsAction implements EventHandler<ActionEvent>
{
    private DesignArea<?> da;
    private boolean state;
    
    public ChangeSelectionStateOfAllDesignsAction(final DesignArea<?> da, final boolean state) {
        this.da = da;
        this.state = state;
    }
    
    public void handle(final ActionEvent e) {
        final List<DesignFrame> dfl = (List<DesignFrame>)this.da.getDesignFrameList();
        for (final DesignFrame d : dfl) {
            d.getCheckBox().setSelected(this.state);
        }
    }
}
