// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions.TextfieldChange;

import javafx.beans.value.ObservableValue;
import java.lang.reflect.Field;
import javafx.beans.value.ChangeListener;
import Util.Designs.Design;

public class TextfieldChangeEventForStringAction<DesignType extends Design> implements ChangeListener<String>
{
    private String property;
    private Field[] classFields;
    private Field[] superclassFields;
    private DesignType design;
    
    public TextfieldChangeEventForStringAction(final String property, final DesignType design) {
        this.property = property;
        this.classFields = design.getClass().getDeclaredFields();
        this.superclassFields = design.getClass().getSuperclass().getDeclaredFields();
        this.design = design;
    }
    
    public void changed(final ObservableValue<? extends String> observable, final String oldValue, final String newValue) {
        Field[] classFields;
        for (int length = (classFields = this.classFields).length, i = 0; i < length; ++i) {
            final Field f = classFields[i];
            if (f.getName().toUpperCase().equals(this.property.toUpperCase())) {
                try {
                    f.set(this.design, newValue);
                    break;
                }
                catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                }
            }
        }
        Field[] superclassFields;
        for (int length2 = (superclassFields = this.superclassFields).length, j = 0; j < length2; ++j) {
            final Field f = superclassFields[j];
            if (f.getName().toUpperCase().equals(this.property.toUpperCase())) {
                try {
                    f.set(this.design, newValue);
                    break;
                }
                catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
