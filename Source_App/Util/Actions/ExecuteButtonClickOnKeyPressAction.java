// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import javafx.scene.input.KeyCode;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;

public class ExecuteButtonClickOnKeyPressAction implements EventHandler<KeyEvent>
{
    private Button btn;
    private KeyCode key;
    
    public ExecuteButtonClickOnKeyPressAction(final Button btn, final KeyCode key) {
        this.btn = btn;
        this.key = key;
    }
    
    public void handle(final KeyEvent arg0) {
        if (arg0.getCode() == this.key) {
            this.btn.fire();
        }
    }
}
