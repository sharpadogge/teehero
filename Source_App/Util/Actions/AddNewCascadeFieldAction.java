// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import javafx.scene.control.Alert;
import Util.OptionValues.KeyValuePair;
import javafx.scene.control.ComboBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class AddNewCascadeFieldAction implements EventHandler<ActionEvent>
{
    private ComboBox<String> comUserPattern;
    private ComboBox<KeyValuePair> comPattern;
    private ComboBox<String> comTeeHeroPattern;
    
    public AddNewCascadeFieldAction(final ComboBox<String> comUserPattern, final ComboBox<String> comTeeHeroPattern, final ComboBox<KeyValuePair> comPattern) {
        this.comUserPattern = comUserPattern;
        this.comPattern = comPattern;
        this.comTeeHeroPattern = comTeeHeroPattern;
    }
    
    public void handle(final ActionEvent arg0) {
        if (this.comPattern.getItems().size() >= 20) {
            final Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText((String)null);
            alert.setContentText("Not more than 20 patterns");
            alert.setTitle("Too many");
            alert.showAndWait();
            return;
        }
        if (this.comUserPattern.getValue() != null && this.comTeeHeroPattern.getValue() != null) {
            this.comPattern.getItems().add((Object)new KeyValuePair(((String)this.comTeeHeroPattern.getValue()).toString(), ((String)this.comUserPattern.getValue()).toString()));
            this.comTeeHeroPattern.getItems().remove(this.comTeeHeroPattern.getValue());
            this.comUserPattern.getItems().remove(this.comUserPattern.getValue());
        }
    }
}
