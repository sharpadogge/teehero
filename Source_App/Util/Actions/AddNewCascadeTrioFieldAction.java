// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import javafx.scene.control.Alert;
import Util.OptionValues.SpecialValueTrio;
import javafx.scene.control.ComboBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class AddNewCascadeTrioFieldAction implements EventHandler<ActionEvent>
{
    private ComboBox<String> comUserPattern;
    private ComboBox<SpecialValueTrio> comPattern;
    private ComboBox<String> comTeeHeroPattern;
    private ComboBox<String> comUserColor;
    
    public AddNewCascadeTrioFieldAction(final ComboBox<String> comUserPattern, final ComboBox<String> comTeeHeroPattern, final ComboBox<SpecialValueTrio> comPattern, final ComboBox<String> comUserColor) {
        this.comUserPattern = comUserPattern;
        this.comPattern = comPattern;
        this.comTeeHeroPattern = comTeeHeroPattern;
        this.comUserColor = comUserColor;
    }
    
    public void handle(final ActionEvent arg0) {
        if (this.comPattern.getItems().size() >= 20) {
            final Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText((String)null);
            alert.setContentText("Not more than 20 patterns");
            alert.setTitle("Too many");
            alert.showAndWait();
            return;
        }
        if (this.comUserPattern.getValue() != null && this.comTeeHeroPattern.getValue() != null && this.comUserColor.getValue() != null) {
            this.comPattern.getItems().add((Object)new SpecialValueTrio(((String)this.comTeeHeroPattern.getValue()).toString(), ((String)this.comUserPattern.getValue()).toString(), ((String)this.comUserColor.getValue()).toString()));
            this.comTeeHeroPattern.getItems().remove(this.comTeeHeroPattern.getValue());
            this.comUserPattern.getItems().remove(this.comUserPattern.getValue());
        }
    }
}
