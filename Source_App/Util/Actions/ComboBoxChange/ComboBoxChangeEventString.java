// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions.ComboBoxChange;

import javafx.event.Event;
import Util.Enums.OptionNamesInConfig;
import Util.Enums.PlatformColors;
import Util.Config.ConfigHandler;
import javafx.scene.control.ComboBox;
import java.lang.reflect.Field;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import Util.Designs.Design;

public class ComboBoxChangeEventString<DesignType extends Design> implements EventHandler<ActionEvent>
{
    private String property;
    private Field[] classFields;
    private Field[] superclassFields;
    private DesignType design;
    private ComboBox<String> comboPattern;
    
    public ComboBoxChangeEventString(final String property, final DesignType design, final ComboBox<String> comboPattern) {
        this.property = property;
        this.classFields = design.getClass().getDeclaredFields();
        this.superclassFields = design.getClass().getSuperclass().getDeclaredFields();
        this.design = design;
        this.comboPattern = comboPattern;
    }
    
    public void handle(final ActionEvent arg0) {
        if (this.comboPattern.getValue() != null) {
            Field[] classFields;
            for (int length = (classFields = this.classFields).length, i = 0; i < length; ++i) {
                final Field f = classFields[i];
                if (f.getName().toUpperCase().equals(this.property.toUpperCase())) {
                    try {
                        if (this.property.toUpperCase().equals("ID")) {
                            f.set(this.design, ((String)this.comboPattern.getValue()).toString());
                            Field[] classFields2;
                            for (int length2 = (classFields2 = this.classFields).length, j = 0; j < length2; ++j) {
                                final Field f2 = classFields2[j];
                                if (f2.getName().toUpperCase().equals("STANDARDCOLOR")) {
                                    System.out.println(((String)this.comboPattern.getValue()).toString());
                                    f2.set(this.design, this.getShirteeColorBasedOnId(((String)this.comboPattern.getValue()).toString()));
                                }
                            }
                            break;
                        }
                        f.set(this.design, ((String)this.comboPattern.getValue()).toString());
                        break;
                    }
                    catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    catch (IllegalAccessException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            Field[] superclassFields;
            for (int length3 = (superclassFields = this.superclassFields).length, k = 0; k < length3; ++k) {
                final Field f = superclassFields[k];
                if (f.getName().toUpperCase().equals(this.property.toUpperCase())) {
                    try {
                        if (this.property.toUpperCase().equals("ID")) {
                            f.set(this.design, ((String)this.comboPattern.getValue()).toString());
                            Field[] classFields3;
                            for (int length4 = (classFields3 = this.classFields).length, l = 0; l < length4; ++l) {
                                final Field f2 = classFields3[l];
                                if (f2.getName().toUpperCase().equals("STANDARDCOLOR")) {
                                    System.out.println(((String)this.comboPattern.getValue()).toString());
                                    f2.set(this.design, this.getShirteeColorBasedOnId(((String)this.comboPattern.getValue()).toString()));
                                }
                            }
                            break;
                        }
                        f.set(this.design, ((String)this.comboPattern.getValue()).toString());
                        break;
                    }
                    catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    catch (IllegalAccessException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }
    
    private String[] getShirteeColorBasedOnId(final String id) {
        final ConfigHandler ch = new ConfigHandler();
        System.out.println(PlatformColors.SHIRTEE.getMatchedPlatformColors(this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SHIRTEE_DESIGNID_COLOR_CASCADE.getOptionName()), id, 2))[0]);
        return PlatformColors.SHIRTEE.getMatchedPlatformColors(this.findDefaultPattern(ch.readSetting(OptionNamesInConfig.SHIRTEE_DESIGNID_COLOR_CASCADE.getOptionName()), id, 2));
    }
    
    private String findDefaultPattern(final String valueString, final String searchPattern, final int location) {
        if (valueString != null && valueString.length() > 0) {
            final String[] values = valueString.split(",");
            final String[] array;
            final int length = (array = values).length;
            int i = 0;
            while (i < length) {
                final String s = array[i];
                final String[] keyValuePair = s.split(":");
                if (keyValuePair[1].substring(0, keyValuePair[1].length()).equalsIgnoreCase(searchPattern)) {
                    final String pattern = keyValuePair[location].substring(0, keyValuePair[location].length());
                    if (pattern.substring(pattern.length() - 1, pattern.length()).equalsIgnoreCase("]")) {
                        return keyValuePair[location].substring(0, keyValuePair[location].length() - 1);
                    }
                    return keyValuePair[location].substring(0, keyValuePair[location].length());
                }
                else {
                    ++i;
                }
            }
        }
        return "";
    }
}
