// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ToggleUploadButton implements EventHandler<ActionEvent>
{
    private CheckBox cb;
    private Button btn;
    
    public ToggleUploadButton(final CheckBox cb, final Button btn) {
        this.btn = btn;
        this.cb = cb;
    }
    
    public void handle(final ActionEvent arg0) {
        if (this.cb.isSelected()) {
            this.btn.setDisable(false);
        }
        else {
            this.btn.setDisable(true);
        }
    }
}
