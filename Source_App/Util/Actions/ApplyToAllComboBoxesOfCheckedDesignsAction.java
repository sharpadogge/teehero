// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.util.Iterator;
import GUI.DesignFrames.DesignFrame;
import java.lang.reflect.Field;
import javafx.scene.control.ComboBox;
import GUI.DesignArea.DesignArea;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ApplyToAllComboBoxesOfCheckedDesignsAction implements EventHandler<ActionEvent>
{
    private DesignArea da;
    private String componentName;
    private ComboBox combo;
    private Field[] classFields;
    private Field[] superclassFields;
    
    public ApplyToAllComboBoxesOfCheckedDesignsAction(final DesignArea da, final String componentName, final ComboBox<String> combo) {
        this.da = da;
        this.componentName = componentName;
        this.combo = combo;
    }
    
    public void handle(final ActionEvent arg0) {
        for (final DesignFrame df : this.da.getDesignFrameList()) {
            if (df.getCheckBox().isSelected()) {
                this.classFields = df.getClass().getDeclaredFields();
                this.superclassFields = df.getClass().getSuperclass().getDeclaredFields();
                Field[] classFields;
                for (int length = (classFields = this.classFields).length, i = 0; i < length; ++i) {
                    final Field f = classFields[i];
                    if (f.getName().toUpperCase().equals(this.componentName.toUpperCase())) {
                        try {
                            ((ComboBox)f.get(df)).setValue((Object)this.combo.getValue().toString());
                            break;
                        }
                        catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        catch (IllegalAccessException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                Field[] superclassFields;
                for (int length2 = (superclassFields = this.superclassFields).length, j = 0; j < length2; ++j) {
                    final Field f = superclassFields[j];
                    if (f.getName().toUpperCase().equals(this.componentName.toUpperCase())) {
                        try {
                            ((ComboBox)f.get(df)).setValue((Object)this.combo.getValue().toString());
                            break;
                        }
                        catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        catch (IllegalAccessException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
