// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.util.List;
import javafx.scene.control.Control;
import Util.Enums.GUIStyles;
import javafx.scene.control.Button;
import java.util.ArrayList;
import GUI.DesignArea.DesignArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import GUI.Pages.MainPage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ChangeDesignAreaAction implements EventHandler<ActionEvent>
{
    private String designAreaName;
    private MainPage mp;
    private Pane panel;
    private String[][] actionButtonNamesAndActions;
    private HBox actionButtonContainer;
    private DesignArea da;
    private ArrayList<Button> btnList;
    
    public ChangeDesignAreaAction(final String designAreaName, final MainPage mp, final Pane panel, final String[][] actionButtonNamesAndActions, final HBox actionButtonContainer) {
        this.designAreaName = designAreaName;
        this.mp = mp;
        this.panel = panel;
        this.actionButtonNamesAndActions = actionButtonNamesAndActions;
        this.actionButtonContainer = actionButtonContainer;
    }
    
    public void handle(final ActionEvent arg0) {
        this.da = this.mp.getDesignAreaHandler().getDesignArea(this.designAreaName);
        if (this.da == null) {
            return;
        }
        this.panel.getChildren().clear();
        this.panel.getChildren().add((Object)this.da);
        try {
            this.da.updateVisualDesignList();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.activateButton((Button)arg0.getSource());
        this.changeDeleteButtonAction();
        this.mp.getBtnAll().setOnAction((EventHandler)new ChangeSelectionStateOfAllDesignsAction(this.da, true));
        this.mp.getBtnNone().setOnAction((EventHandler)new ChangeSelectionStateOfAllDesignsAction(this.da, false));
        this.createAndSetButtonsForDesignArea();
    }
    
    private void activateButton(final Button btn) {
        this.mp.restoreDefaultDesignAreaSelectionButtonAppearance();
        btn.setStyle("-fx-text-fill: #e60000");
    }
    
    private void createAndSetButtonsForDesignArea() {
        this.actionButtonContainer.getChildren().clear();
        this.btnList = new ArrayList<Button>();
        this.mp.getBtnApply().setOnAction((EventHandler)null);
        this.mp.getTbApplyValue().setText((String)null);
        for (int i = 0; i < this.actionButtonNamesAndActions.length; ++i) {
            final Button btn = new Button(this.actionButtonNamesAndActions[i][0]);
            btn.getStyleClass().addAll((Object[])new String[] { GUIStyles.BASIC_BTN.getStyleClasses() });
            final String upperCase;
            switch (upperCase = this.actionButtonNamesAndActions[i][2].toUpperCase()) {
                case "TEXTBOX": {
                    final ApplyToAllTextFieldsOfCheckedDesignsAction action = new ApplyToAllTextFieldsOfCheckedDesignsAction(this.da, this.actionButtonNamesAndActions[i][1], this.mp.getTbApplyValue());
                    btn.setOnAction((EventHandler)new ChangeApplyBtnFunctionality((EventHandler<ActionEvent>)action, this.mp.getBtnApply(), (Control)this.mp.getTbApplyValue(), this.btnList, this.actionButtonNamesAndActions[i][2].toUpperCase(), null, this.mp));
                    break;
                }
                case "COMBOBOX": {
                    final ApplyToAllComboBoxesOfCheckedDesignsAction action2 = new ApplyToAllComboBoxesOfCheckedDesignsAction(this.da, this.actionButtonNamesAndActions[i][1], this.mp.getComboBoxApplyValue());
                    if (this.da.getDesignFrameList().size() > 0) {
                        final String pattern = this.actionButtonNamesAndActions[i][3];
                        btn.setOnAction((EventHandler)new ChangeApplyBtnFunctionality((EventHandler<ActionEvent>)action2, this.mp.getBtnApply(), (Control)this.mp.getComboBoxApplyValue(), this.btnList, this.actionButtonNamesAndActions[i][2].toUpperCase(), pattern, this.mp));
                        break;
                    }
                    break;
                }
                default:
                    break;
            }
            btn.setPrefSize(80.0, 22.0);
            this.actionButtonContainer.getChildren().add((Object)btn);
            this.btnList.add(btn);
        }
        this.btnList.get(0).fire();
    }
    
    private void changeDeleteButtonAction() {
        final Button btn = this.mp.getDeleteSelectedDesignsButton();
        btn.setOnAction((EventHandler)new DeleteAllSelectedDesignsAction(this.da));
    }
}
