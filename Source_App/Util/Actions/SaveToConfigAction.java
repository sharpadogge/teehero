// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.io.IOException;
import javafx.scene.control.Alert;
import GUI.OptionArea.OptionsArea;
import java.util.Map;
import GUI.Pages.MainPage;
import Util.Config.ConfigHandler;
import GUI.Pages.OptionsPage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SaveToConfigAction implements EventHandler<ActionEvent>
{
    private OptionsPage op;
    private ConfigHandler ch;
    private MainPage mp;
    
    public SaveToConfigAction(final OptionsPage op, final MainPage mp) {
        this.op = op;
        this.ch = new ConfigHandler();
        this.mp = mp;
    }
    
    public void handle(final ActionEvent arg0) {
        try {
            this.ch.getOptionsFromOptionAreas(this.op.getOptionsAreaHandler().getOptionsAreas());
            final Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText((String)null);
            alert.setContentText("Changes have successfully been saved.");
            alert.setTitle("Saved");
            alert.showAndWait();
        }
        catch (IOException e) {
            e.printStackTrace();
            final Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            alert2.setHeaderText((String)null);
            alert2.setContentText("It's seems like you have insufficient permissions to write on this machine.");
            alert2.setTitle("Unable to save changes");
            alert2.showAndWait();
        }
        this.mp.getBtnGeneral().fire();
        this.op.getStage().close();
    }
}
