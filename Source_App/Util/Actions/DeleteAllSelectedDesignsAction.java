// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import java.util.Optional;
import java.util.Iterator;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert;
import GUI.DesignFrames.DesignFrame;
import javafx.scene.control.ProgressBar;
import GUI.DesignArea.DesignArea;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class DeleteAllSelectedDesignsAction implements EventHandler<ActionEvent>
{
    private DesignArea da;
    private ProgressBar progress;
    
    public DeleteAllSelectedDesignsAction(final DesignArea da) {
        this.da = da;
    }
    
    public void handle(final ActionEvent arg0) {
        int counter = 0;
        for (final DesignFrame df : this.da.getDesignFrameList()) {
            if (df.isChecked()) {
                ++counter;
            }
        }
        final Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText((String)null);
        alert.getButtonTypes().clear();
        alert.setContentText("Do you really want to delete these " + counter + " selected designs?");
        final ButtonType btnYes = new ButtonType("Yes");
        final ButtonType btnNo = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().add((Object)btnYes);
        alert.getButtonTypes().add((Object)btnNo);
        final Optional<ButtonType> answer = (Optional<ButtonType>)alert.showAndWait();
        if (answer.get() == btnYes) {
            int progressCounter = 0;
            (this.progress = this.da.getMainPage().getProgressBar()).setProgress((double)(progressCounter / counter));
            this.progress.setVisible(true);
            for (int i = 0; i < this.da.getDesignFrameList().size(); ++i) {
                final DesignFrame df2 = this.da.getDesignFrameList().get(i);
                if (df2.isChecked()) {
                    df2.getDeleteButton().fire();
                    this.updateProgress(++progressCounter / counter);
                    --i;
                }
            }
            final Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            alert2.setContentText("Deletion completed");
            alert2.setHeaderText((String)null);
            alert2.setTitle("Finished");
            alert2.showAndWait();
            this.progress.setVisible(false);
        }
    }
    
    private void updateProgress(final int val) {
        this.progress.setProgress((double)val);
    }
}
