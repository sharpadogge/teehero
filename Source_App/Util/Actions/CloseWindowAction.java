// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import GUI.Window.Window;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class CloseWindowAction implements EventHandler<ActionEvent>
{
    private Window w;
    
    public CloseWindowAction(final Window w) {
        this.w = w;
    }
    
    public void handle(final ActionEvent arg0) {
        this.w.closeWindow();
    }
}
