// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions.Tests;

import org.junit.Test;
import org.junit.Assert;
import javafx.event.ActionEvent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import GUI.Pages.OptionsPage;
import javafx.stage.Stage;
import GUI.Pages.MainPage;
import Util.User.User;
import java.util.Date;
import GUI.Window.Window;
import javafx.scene.layout.BorderPane;
import org.junit.ClassRule;
import Util.Tests.JavaFXThreadingRule;
import Util.Actions.AddNewPatternAction;
import GUI.OptionArea.OptionsArea;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AddNewPatternActionTest
{
    private static TextField textfield;
    private static ComboBox<String> comPattern;
    private static String[] connectedOptions;
    private static String[] connectedOptions2;
    private static String optionName;
    private static OptionsArea oa;
    private static AddNewPatternAction action1;
    private static AddNewPatternAction action2;
    @ClassRule
    public static JavaFXThreadingRule javafxRule;
    
    static {
        AddNewPatternActionTest.javafxRule = new JavaFXThreadingRule();
    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        AddNewPatternActionTest.textfield = new TextField();
        AddNewPatternActionTest.comPattern = (ComboBox<String>)new ComboBox();
        AddNewPatternActionTest.connectedOptions = new String[] { "None" };
        AddNewPatternActionTest.connectedOptions2 = new String[] { "Option1", "Option2" };
        AddNewPatternActionTest.optionName = "TestOption";
        AddNewPatternActionTest.oa = new OptionsArea("TestArea", new OptionsPage(new MainPage(new BorderPane(), new Window(), new User("TestUser", new Date())), new BorderPane(), new Window()));
        AddNewPatternActionTest.action1 = new AddNewPatternAction(AddNewPatternActionTest.textfield, AddNewPatternActionTest.comPattern, AddNewPatternActionTest.connectedOptions, AddNewPatternActionTest.optionName, AddNewPatternActionTest.oa);
        AddNewPatternActionTest.action2 = new AddNewPatternAction(AddNewPatternActionTest.textfield, AddNewPatternActionTest.comPattern, AddNewPatternActionTest.connectedOptions2, AddNewPatternActionTest.optionName, AddNewPatternActionTest.oa);
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        AddNewPatternActionTest.textfield = null;
        AddNewPatternActionTest.comPattern = null;
        AddNewPatternActionTest.connectedOptions = null;
        AddNewPatternActionTest.connectedOptions2 = null;
        AddNewPatternActionTest.optionName = null;
        AddNewPatternActionTest.oa = null;
        AddNewPatternActionTest.action1 = null;
        AddNewPatternActionTest.action2 = null;
    }
    
    @Test
    public void addNewPatternTest() {
        AddNewPatternActionTest.textfield.setText("TestPattern");
        AddNewPatternActionTest.action1.handle(new ActionEvent());
        Assert.assertEquals("TestPattern", AddNewPatternActionTest.comPattern.getItems().get(0));
        AddNewPatternActionTest.comPattern.getItems().remove(0);
        AddNewPatternActionTest.textfield.setText("TestPattern2");
        AddNewPatternActionTest.action2.handle(new ActionEvent());
        Assert.assertEquals("TestPattern2", AddNewPatternActionTest.comPattern.getItems().get(0));
        AddNewPatternActionTest.comPattern.getItems().remove(0);
    }
    
    @Test(expected = Exception.class)
    public void addDuplicatTest() {
        AddNewPatternActionTest.textfield.setText("TestPattern");
        AddNewPatternActionTest.action1.handle(new ActionEvent());
        Assert.assertEquals("TestPattern", AddNewPatternActionTest.comPattern.getItems().get(0));
        AddNewPatternActionTest.comPattern.getItems().get(1);
    }
}
