// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions.Tests;

import org.junit.Test;
import org.junit.Assert;
import javafx.event.ActionEvent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import Util.Tests.JavaFXThreadingRule;
import Util.Actions.AddNewCascadeTrioFieldAction;
import Util.OptionValues.SpecialValueTrio;
import javafx.scene.control.ComboBox;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AddNewCascadeTrioFieldActionTest
{
    private static ComboBox<String> comUserPattern;
    private static ComboBox<String> comTeeHeroPattern;
    private static ComboBox<SpecialValueTrio> comPattern;
    private static ComboBox<String> comUserColor;
    private static AddNewCascadeTrioFieldAction action;
    @ClassRule
    public static JavaFXThreadingRule javafxRule;
    
    static {
        AddNewCascadeTrioFieldActionTest.javafxRule = new JavaFXThreadingRule();
    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        AddNewCascadeTrioFieldActionTest.comUserPattern = (ComboBox<String>)new ComboBox();
        AddNewCascadeTrioFieldActionTest.comTeeHeroPattern = (ComboBox<String>)new ComboBox();
        AddNewCascadeTrioFieldActionTest.comUserColor = (ComboBox<String>)new ComboBox();
        AddNewCascadeTrioFieldActionTest.comPattern = (ComboBox<SpecialValueTrio>)new ComboBox();
        AddNewCascadeTrioFieldActionTest.action = new AddNewCascadeTrioFieldAction(AddNewCascadeTrioFieldActionTest.comUserPattern, AddNewCascadeTrioFieldActionTest.comTeeHeroPattern, AddNewCascadeTrioFieldActionTest.comPattern, AddNewCascadeTrioFieldActionTest.comUserColor);
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        AddNewCascadeTrioFieldActionTest.comUserPattern = null;
        AddNewCascadeTrioFieldActionTest.comTeeHeroPattern = null;
        AddNewCascadeTrioFieldActionTest.comUserColor = null;
        AddNewCascadeTrioFieldActionTest.comPattern = null;
        AddNewCascadeTrioFieldActionTest.action = null;
    }
    
    @Test
    public void addNewCascadeTest() {
        AddNewCascadeTrioFieldActionTest.comUserPattern.getItems().add((Object)"Test1");
        AddNewCascadeTrioFieldActionTest.comUserPattern.setValue((Object)"Test1");
        AddNewCascadeTrioFieldActionTest.comTeeHeroPattern.getItems().add((Object)"Test2");
        AddNewCascadeTrioFieldActionTest.comTeeHeroPattern.setValue((Object)"Test2");
        AddNewCascadeTrioFieldActionTest.comUserColor.getItems().add((Object)"TestColor");
        AddNewCascadeTrioFieldActionTest.comUserColor.setValue((Object)"TestColor");
        AddNewCascadeTrioFieldActionTest.action.handle(new ActionEvent());
        Assert.assertEquals("Test1", ((SpecialValueTrio)AddNewCascadeTrioFieldActionTest.comPattern.getItems().get(0)).getShirteePattern());
        Assert.assertEquals("Test2", ((SpecialValueTrio)AddNewCascadeTrioFieldActionTest.comPattern.getItems().get(0)).getTeeHeroPattern());
        Assert.assertEquals("TestColor", ((SpecialValueTrio)AddNewCascadeTrioFieldActionTest.comPattern.getItems().get(0)).getShirteeColor());
    }
    
    @Test
    public void tooManyEntriesTest() {
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        AddNewCascadeTrioFieldActionTest.comPattern.getItems().add((Object)new SpecialValueTrio("Test1", "Test2", "TestColor"));
        Assert.assertEquals(20L, AddNewCascadeTrioFieldActionTest.comPattern.getItems().size());
        AddNewCascadeTrioFieldActionTest.comUserPattern.getItems().add((Object)"Test1");
        AddNewCascadeTrioFieldActionTest.comUserPattern.setValue((Object)"Test1");
        AddNewCascadeTrioFieldActionTest.comTeeHeroPattern.getItems().add((Object)"Test2");
        AddNewCascadeTrioFieldActionTest.comTeeHeroPattern.setValue((Object)"Test2");
        AddNewCascadeTrioFieldActionTest.comUserColor.getItems().add((Object)"TestColor");
        AddNewCascadeTrioFieldActionTest.comUserColor.setValue((Object)"TestColor");
        AddNewCascadeTrioFieldActionTest.action.handle(new ActionEvent());
        Assert.assertEquals(20L, AddNewCascadeTrioFieldActionTest.comPattern.getItems().size());
    }
}
