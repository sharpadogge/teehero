// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions.Tests;

import org.junit.Test;
import org.junit.Assert;
import javafx.event.ActionEvent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import Util.Tests.JavaFXThreadingRule;
import Util.Actions.AddNewCascadeFieldAction;
import Util.OptionValues.KeyValuePair;
import javafx.scene.control.ComboBox;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AddNewCascadeFieldActionTest
{
    private static ComboBox<String> comUserPattern;
    private static ComboBox<String> comTeeHeroPattern;
    private static ComboBox<KeyValuePair> comPattern;
    private static AddNewCascadeFieldAction action;
    @ClassRule
    public static JavaFXThreadingRule javafxRule;
    
    static {
        AddNewCascadeFieldActionTest.javafxRule = new JavaFXThreadingRule();
    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        AddNewCascadeFieldActionTest.comUserPattern = (ComboBox<String>)new ComboBox();
        AddNewCascadeFieldActionTest.comTeeHeroPattern = (ComboBox<String>)new ComboBox();
        AddNewCascadeFieldActionTest.comPattern = (ComboBox<KeyValuePair>)new ComboBox();
        AddNewCascadeFieldActionTest.action = new AddNewCascadeFieldAction(AddNewCascadeFieldActionTest.comUserPattern, AddNewCascadeFieldActionTest.comTeeHeroPattern, AddNewCascadeFieldActionTest.comPattern);
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        AddNewCascadeFieldActionTest.comUserPattern = null;
        AddNewCascadeFieldActionTest.comTeeHeroPattern = null;
        AddNewCascadeFieldActionTest.comPattern = null;
        AddNewCascadeFieldActionTest.action = null;
    }
    
    @Test
    public void addNewCascadeTest() {
        AddNewCascadeFieldActionTest.comUserPattern.getItems().add((Object)"Test1");
        AddNewCascadeFieldActionTest.comUserPattern.setValue((Object)"Test1");
        AddNewCascadeFieldActionTest.comTeeHeroPattern.getItems().add((Object)"Test2");
        AddNewCascadeFieldActionTest.comTeeHeroPattern.setValue((Object)"Test2");
        AddNewCascadeFieldActionTest.action.handle(new ActionEvent());
        Assert.assertEquals("Test2", ((KeyValuePair)AddNewCascadeFieldActionTest.comPattern.getItems().get(0)).getTeeHeroPattern());
        Assert.assertEquals("Test1", ((KeyValuePair)AddNewCascadeFieldActionTest.comPattern.getItems().get(0)).getOtherPlatformPattern());
    }
    
    @Test
    public void tooManyEntriesTest() {
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        AddNewCascadeFieldActionTest.comPattern.getItems().add((Object)new KeyValuePair("Test1", "Test2"));
        Assert.assertEquals(20L, AddNewCascadeFieldActionTest.comPattern.getItems().size());
        AddNewCascadeFieldActionTest.comUserPattern.getItems().add((Object)"Test1");
        AddNewCascadeFieldActionTest.comUserPattern.setValue((Object)"Test1");
        AddNewCascadeFieldActionTest.comTeeHeroPattern.getItems().add((Object)"Test2");
        AddNewCascadeFieldActionTest.comTeeHeroPattern.setValue((Object)"Test2");
        AddNewCascadeFieldActionTest.action.handle(new ActionEvent());
        Assert.assertEquals(20L, AddNewCascadeFieldActionTest.comPattern.getItems().size());
    }
}
