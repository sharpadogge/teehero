// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions.Tests;

import org.junit.Test;
import javafx.event.ActionEvent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import java.util.List;
import java.io.File;
import java.util.ArrayList;
import javafx.stage.Stage;
import GUI.Pages.MainPage;
import Util.User.User;
import java.util.Date;
import GUI.Window.Window;
import javafx.scene.layout.BorderPane;
import org.junit.ClassRule;
import Util.Tests.JavaFXThreadingRule;
import Util.Actions.ApplyToAllComboBoxesOfCheckedDesignsAction;
import Util.Canvas.ImageCanvas;
import Util.Designs.SpreadshirtDesign;
import javafx.scene.control.ComboBox;
import GUI.DesignFrames.SpreadshirtComDesignFrame;
import GUI.DesignArea.DesignArea;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApplyToAllComboBoxesOfCheckedDesignsActionTest
{
    private static DesignArea<SpreadshirtComDesignFrame> da;
    private static String componentName;
    private static ComboBox<String> combo;
    private static SpreadshirtDesign sprdDesign;
    private static ImageCanvas image;
    private static SpreadshirtComDesignFrame designFrame1;
    private static SpreadshirtComDesignFrame designFrame2;
    private static ApplyToAllComboBoxesOfCheckedDesignsAction action;
    @ClassRule
    public static JavaFXThreadingRule javafxRule;
    
    static {
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.javafxRule = new JavaFXThreadingRule();
    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.da = new DesignArea<SpreadshirtComDesignFrame>("TestArea", new MainPage(new BorderPane(), new Window(), new User("TestUser", new Date())));
        final String title = "testDesign";
        final String description = "testDescription";
        final int designId = 1;
        final double price = 8.0;
        final String teeHeroPattern = "Black";
        final String templateName = "Black";
        final ArrayList<String> tags = new ArrayList<String>();
        tags.add("Tag1");
        tags.add("Tag2");
        tags.add("Tag3");
        final File file = new File("Test Files/Don't fart - Gym workout training muscle shirt.png");
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.image = new ImageCanvas(file);
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.sprdDesign = new SpreadshirtDesign(file, designId, title, description, tags, templateName, price, teeHeroPattern);
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.designFrame1 = new SpreadshirtComDesignFrame(ApplyToAllComboBoxesOfCheckedDesignsActionTest.sprdDesign, ApplyToAllComboBoxesOfCheckedDesignsActionTest.da, ApplyToAllComboBoxesOfCheckedDesignsActionTest.image);
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.designFrame2 = new SpreadshirtComDesignFrame(ApplyToAllComboBoxesOfCheckedDesignsActionTest.sprdDesign, ApplyToAllComboBoxesOfCheckedDesignsActionTest.da, ApplyToAllComboBoxesOfCheckedDesignsActionTest.image);
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.componentName = "comboTemplate";
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.combo = (ComboBox<String>)new ComboBox();
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.combo.getItems().add((Object)"TestPattern");
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.combo.setValue((Object)"TestPattern");
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.da.addDesign(ApplyToAllComboBoxesOfCheckedDesignsActionTest.designFrame1);
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.da.addDesign(ApplyToAllComboBoxesOfCheckedDesignsActionTest.designFrame2);
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.da = null;
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.componentName = null;
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.combo = null;
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.sprdDesign = null;
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.image = null;
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.designFrame1 = null;
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.designFrame2 = null;
        ApplyToAllComboBoxesOfCheckedDesignsActionTest.action = null;
    }
    
    @Test
    public void actionTest() {
        (ApplyToAllComboBoxesOfCheckedDesignsActionTest.action = new ApplyToAllComboBoxesOfCheckedDesignsAction(ApplyToAllComboBoxesOfCheckedDesignsActionTest.da, ApplyToAllComboBoxesOfCheckedDesignsActionTest.componentName, ApplyToAllComboBoxesOfCheckedDesignsActionTest.combo)).handle(new ActionEvent());
    }
}
