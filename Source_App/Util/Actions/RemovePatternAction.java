// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import Util.Enums.OptionNamesInConfig;
import Util.OptionValues.KeyValuePair;
import java.util.Iterator;
import java.util.HashMap;
import GUI.OptionFrame.OptionFrame;
import java.util.Map;
import GUI.OptionArea.OptionsArea;
import javafx.scene.control.ComboBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RemovePatternAction implements EventHandler<ActionEvent>
{
    private ComboBox<String> comPattern;
    private String[] connectedOptions;
    private OptionsArea oa;
    private String optionName;
    
    public RemovePatternAction(final ComboBox<String> comPattern, final String[] conntectedOptions, final String optionName, final OptionsArea oa) {
        this.comPattern = comPattern;
        this.connectedOptions = conntectedOptions;
        this.oa = oa;
        this.optionName = optionName;
    }
    
    public void handle(final ActionEvent e) {
        if (this.comPattern.getValue() != null) {
            String[] connectedOptions;
            for (int length = (connectedOptions = this.connectedOptions).length, i = 0; i < length; ++i) {
                final String s = connectedOptions[i];
                final HashMap<String, OptionsArea> optionAreas = this.oa.getOpionPage().getOptionsAreaHandler().getOptionsAreas();
                for (final Map.Entry<String, OptionsArea> entry : optionAreas.entrySet()) {
                    for (final OptionFrame of : entry.getValue().getOptionFrameList()) {
                        if (of.getOptionName().equals(s)) {
                            if (this.isTeeHeroDesign(this.optionName)) {
                                this.findAndDeleteInMultiCol(of.getCascadeComboBox(), of.getPlatformComboBox(), ((String)this.comPattern.getValue()).toString(), 0);
                                this.findAndDelete(of.getTeeHeroComboBox(), ((String)this.comPattern.getValue()).toString());
                            }
                            else {
                                this.findAndDeleteInMultiCol(of.getCascadeComboBox(), of.getTeeHeroComboBox(), ((String)this.comPattern.getValue()).toString(), 1);
                                this.findAndDelete(of.getPlatformComboBox(), ((String)this.comPattern.getValue()).toString());
                            }
                            this.findAndDelete(of.getDefaultComboBox(), ((String)this.comPattern.getValue()).toString());
                        }
                    }
                }
            }
            this.comPattern.getItems().remove(this.comPattern.getValue());
        }
    }
    
    private void findAndDelete(final ComboBox<String> combo, final String searchFor) {
        if (combo != null) {
            for (int i = 0; i < combo.getItems().size(); ++i) {
                if (((String)combo.getItems().get(i)).equals(searchFor)) {
                    combo.getItems().remove(i);
                    break;
                }
            }
        }
    }
    
    private void findAndDeleteInMultiCol(final ComboBox<KeyValuePair> combo, final ComboBox<String> comboRemainder, final String searchFor, final int searchIndex) {
        if (combo != null) {
            for (int i = 0; i < combo.getItems().size(); ++i) {
                if (searchIndex == 0) {
                    if (((KeyValuePair)combo.getItems().get(i)).getTeeHeroPattern().equals(searchFor)) {
                        comboRemainder.getItems().add((Object)((KeyValuePair)combo.getItems().get(i)).getOtherPlatformPattern());
                        combo.getItems().remove(i);
                    }
                }
                else if (searchIndex == 1 && ((KeyValuePair)combo.getItems().get(i)).getOtherPlatformPattern().equals(searchFor)) {
                    comboRemainder.getItems().add((Object)((KeyValuePair)combo.getItems().get(i)).getTeeHeroPattern());
                    combo.getItems().remove(i);
                }
            }
        }
    }
    
    private boolean isTeeHeroDesign(final String s) {
        return s.equals(OptionNamesInConfig.GEN_TEEHERO_PATTERN.getOptionName());
    }
}
