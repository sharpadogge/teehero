// 
// Decompiled by Procyon v0.5.36
// 

package Util.Actions;

import javafx.event.Event;
import GUI.DesignFrames.ShirteeDesignFrame;
import Util.Designs.ShirteeDesign;
import Util.Enums.PlatformColors;
import GUI.DesignFrames.SpreadshirtComDesignFrame;
import GUI.DesignFrames.SpreadshirtEuDesignFrame;
import Util.Designs.SpreadshirtDesign;
import java.util.List;
import GUI.DesignArea.DesignArea;
import GUI.DesignFrames.DefaultDesignFrame;
import Util.Designs.Design;
import Util.Canvas.ImageCanvas;
import java.util.Collection;
import java.util.Arrays;
import org.w3c.dom.Element;
import java.util.ArrayList;
import Util.Enums.DesignAreaDefinition;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import javafx.scene.control.Alert;
import javax.xml.parsers.DocumentBuilderFactory;
import javafx.stage.Window;
import javafx.stage.FileChooser;
import java.io.File;
import GUI.Pages.MainPage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ImportDataFromFileAction implements EventHandler<ActionEvent>
{
    private MainPage mp;
    private File file;
    
    public ImportDataFromFileAction(final MainPage mp) {
        this.mp = mp;
    }
    
    public void handle(final ActionEvent arg0) {
        final FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add((Object)new FileChooser.ExtensionFilter("TeeHero-File", new String[] { "*.teehero" }));
        this.file = fc.showOpenDialog((Window)this.mp.getStage());
        if (this.file == null) {
            return;
        }
        try {
            final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            final Document doc = dBuilder.parse(this.file);
            doc.getDocumentElement().normalize();
            this.createDefaultDesignFrame(doc.getElementsByTagName("DEFAULT_DESIGN_FRAME"), doc);
            this.createSpreadShirtEuDesignFrame(doc.getElementsByTagName("SPREADSHIRT_EU_DESIGN_FRAME"), doc);
            this.createSpreadShirtComDesignFrame(doc.getElementsByTagName("SPREADSHIRT_COM_DESIGN_FRAME"), doc);
            this.createShirteeDesignFrame(doc.getElementsByTagName("SHIRTEE_DESIGN_FRAME"), doc);
            this.mp.getBtnGeneral().fire();
            final Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText((String)null);
            alert.setTitle("Import complete");
            alert.setContentText("Data was imported from file: " + this.file.getName());
            alert.showAndWait();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (ParserConfigurationException e2) {
            e2.printStackTrace();
        }
        catch (SAXException e3) {
            e3.printStackTrace();
        }
    }
    
    private void createDefaultDesignFrame(final NodeList nodes, final Document doc) {
        final DesignArea<DefaultDesignFrame> dda = (DesignArea<DefaultDesignFrame>)this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.DEFAULT_AREA.getAreaName());
        for (int i = 0; i < nodes.getLength(); ++i) {
            if (nodes.item(i).getNodeType() == 1) {
                int designId = 0;
                String title = "";
                String description = "";
                File f = null;
                String teeHeroPattern = "";
                List<String> tags = new ArrayList<String>();
                double price = 0.0;
                boolean isSelected = false;
                final Element e = (Element)nodes.item(i);
                if (e.getElementsByTagName("DESIGN_ID").item(0) != null) {
                    designId = Integer.parseInt(e.getElementsByTagName("DESIGN_ID").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TITLE").item(0) != null) {
                    title = e.getElementsByTagName("TITLE").item(0).getTextContent();
                }
                if (e.getElementsByTagName("DESCRIPTION").item(0) != null) {
                    description = e.getElementsByTagName("DESCRIPTION").item(0).getTextContent();
                }
                if (e.getElementsByTagName("FILE").item(0) != null) {
                    f = new File(e.getElementsByTagName("FILE").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TEEHERO_PATTERN").item(0) != null) {
                    teeHeroPattern = e.getElementsByTagName("TEEHERO_PATTERN").item(0).getTextContent();
                }
                if (e.getElementsByTagName("TAGS").item(0) != null) {
                    final String[] array = e.getElementsByTagName("TAGS").item(0).getTextContent().split(",");
                    tags = new ArrayList<String>(Arrays.asList(array));
                }
                if (e.getElementsByTagName("PRICE").item(0) != null) {
                    price = Double.parseDouble(e.getElementsByTagName("PRICE").item(0).getTextContent());
                }
                if (e.getElementsByTagName("IS_SELECTED").item(0) != null) {
                    isSelected = Boolean.getBoolean(e.getElementsByTagName("IS_SELECTED").item(0).getTextContent());
                }
                final ImageCanvas ic = new ImageCanvas(f);
                final Design d = new Design(f, designId, title, description, teeHeroPattern, tags, price);
                final DefaultDesignFrame ddf = new DefaultDesignFrame(d, dda, ic);
                dda.addDesign(ddf);
            }
        }
    }
    
    private void createSpreadShirtEuDesignFrame(final NodeList nodes, final Document doc) {
        final DesignArea<SpreadshirtEuDesignFrame> sda = (DesignArea<SpreadshirtEuDesignFrame>)this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.SPREADSHIRT_EU_AREA.getAreaName());
        for (int i = 0; i < nodes.getLength(); ++i) {
            if (nodes.item(i).getNodeType() == 1) {
                int designId = 0;
                String title = "";
                String description = "";
                File f = null;
                String teeHeroPattern = "";
                List<String> tags = new ArrayList<String>();
                double price = 0.0;
                boolean isSelected = false;
                int templateNumber = 0;
                String templateName = "";
                final Element e = (Element)nodes.item(i);
                if (e.getElementsByTagName("DESIGN_ID").item(0) != null) {
                    designId = Integer.parseInt(e.getElementsByTagName("DESIGN_ID").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TITLE").item(0) != null) {
                    title = e.getElementsByTagName("TITLE").item(0).getTextContent();
                }
                if (e.getElementsByTagName("DESCRIPTION").item(0) != null) {
                    description = e.getElementsByTagName("DESCRIPTION").item(0).getTextContent();
                }
                if (e.getElementsByTagName("FILE").item(0) != null) {
                    f = new File(e.getElementsByTagName("FILE").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TEEHERO_PATTERN").item(0) != null) {
                    teeHeroPattern = e.getElementsByTagName("TEEHERO_PATTERN").item(0).getTextContent();
                }
                if (e.getElementsByTagName("TAGS").item(0) != null) {
                    final String[] array = e.getElementsByTagName("TAGS").item(0).getTextContent().split(",");
                    tags = new ArrayList<String>(Arrays.asList(array));
                }
                if (e.getElementsByTagName("PRICE").item(0) != null) {
                    price = Double.parseDouble(e.getElementsByTagName("PRICE").item(0).getTextContent());
                }
                if (e.getElementsByTagName("IS_SELECTED").item(0) != null) {
                    isSelected = Boolean.getBoolean(e.getElementsByTagName("IS_SELECTED").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TEMPLATE_NAME").item(0) != null) {
                    templateName = e.getElementsByTagName("TEMPLATE_NAME").item(0).getTextContent();
                }
                if (e.getElementsByTagName("TEMPLATE_NUMBER").item(0) != null) {
                    templateNumber = Integer.parseInt(e.getElementsByTagName("TEMPLATE_NUMBER").item(0).getTextContent());
                }
                final ImageCanvas ic = new ImageCanvas(f);
                final SpreadshirtDesign scd = new SpreadshirtDesign(f, designId, title, description, tags, templateName, price, teeHeroPattern);
                final SpreadshirtEuDesignFrame scdf = new SpreadshirtEuDesignFrame(scd, sda, ic);
                sda.addDesign(scdf);
            }
        }
    }
    
    private void createSpreadShirtComDesignFrame(final NodeList nodes, final Document doc) {
        final DesignArea<SpreadshirtComDesignFrame> scda = (DesignArea<SpreadshirtComDesignFrame>)this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.SPREADSHIRT_COM_AREA.getAreaName());
        for (int i = 0; i < nodes.getLength(); ++i) {
            if (nodes.item(i).getNodeType() == 1) {
                int designId = 0;
                String title = "";
                String description = "";
                File f = null;
                String teeHeroPattern = "";
                List<String> tags = new ArrayList<String>();
                double price = 0.0;
                boolean isSelected = false;
                int templateNumber = 0;
                String templateName = "";
                final Element e = (Element)nodes.item(i);
                if (e.getElementsByTagName("DESIGN_ID").item(0) != null) {
                    designId = Integer.parseInt(e.getElementsByTagName("DESIGN_ID").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TITLE").item(0) != null) {
                    title = e.getElementsByTagName("TITLE").item(0).getTextContent();
                }
                if (e.getElementsByTagName("DESCRIPTION").item(0) != null) {
                    description = e.getElementsByTagName("DESCRIPTION").item(0).getTextContent();
                }
                if (e.getElementsByTagName("FILE").item(0) != null) {
                    f = new File(e.getElementsByTagName("FILE").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TEEHERO_PATTERN").item(0) != null) {
                    teeHeroPattern = e.getElementsByTagName("TEEHERO_PATTERN").item(0).getTextContent();
                }
                if (e.getElementsByTagName("TAGS").item(0) != null) {
                    final String[] array = e.getElementsByTagName("TAGS").item(0).getTextContent().split(",");
                    tags = new ArrayList<String>(Arrays.asList(array));
                }
                if (e.getElementsByTagName("PRICE").item(0) != null) {
                    price = Double.parseDouble(e.getElementsByTagName("PRICE").item(0).getTextContent());
                }
                if (e.getElementsByTagName("IS_SELECTED").item(0) != null) {
                    isSelected = Boolean.getBoolean(e.getElementsByTagName("IS_SELECTED").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TEMPLATE_NAME").item(0) != null) {
                    templateName = e.getElementsByTagName("TEMPLATE_NAME").item(0).getTextContent();
                }
                if (e.getElementsByTagName("TEMPLATE_NUMBER").item(0) != null) {
                    templateNumber = Integer.parseInt(e.getElementsByTagName("TEMPLATE_NUMBER").item(0).getTextContent());
                }
                final ImageCanvas ic = new ImageCanvas(f);
                final SpreadshirtDesign scd = new SpreadshirtDesign(f, designId, title, description, tags, templateName, price, teeHeroPattern);
                final SpreadshirtComDesignFrame scdf = new SpreadshirtComDesignFrame(scd, scda, ic);
                scda.addDesign(scdf);
            }
        }
    }
    
    private void createShirteeDesignFrame(final NodeList nodes, final Document doc) {
        final DesignArea<ShirteeDesignFrame> stda = (DesignArea<ShirteeDesignFrame>)this.mp.getDesignAreaHandler().getDesignArea(DesignAreaDefinition.SHIRTEE_AREA.getAreaName());
        for (int i = 0; i < nodes.getLength(); ++i) {
            if (nodes.item(i).getNodeType() == 1) {
                int designId = 0;
                String title = "";
                String description = "";
                File f = null;
                String teeHeroPattern = "";
                List<String> tags = new ArrayList<String>();
                double price = 0.0;
                boolean isSelected = false;
                String marketplace = "";
                String id = "";
                String category = "";
                String color = "";
                final Element e = (Element)nodes.item(i);
                if (e.getElementsByTagName("DESIGN_ID").item(0) != null) {
                    designId = Integer.parseInt(e.getElementsByTagName("DESIGN_ID").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TITLE").item(0) != null) {
                    title = e.getElementsByTagName("TITLE").item(0).getTextContent();
                }
                if (e.getElementsByTagName("DESCRIPTION").item(0) != null) {
                    description = e.getElementsByTagName("DESCRIPTION").item(0).getTextContent();
                }
                if (e.getElementsByTagName("FILE").item(0) != null) {
                    f = new File(e.getElementsByTagName("FILE").item(0).getTextContent());
                }
                if (e.getElementsByTagName("TEEHERO_PATTERN").item(0) != null) {
                    teeHeroPattern = e.getElementsByTagName("TEEHERO_PATTERN").item(0).getTextContent();
                }
                if (e.getElementsByTagName("TAGS").item(0) != null) {
                    final String[] array = e.getElementsByTagName("TAGS").item(0).getTextContent().split(",");
                    tags = new ArrayList<String>(Arrays.asList(array));
                }
                if (e.getElementsByTagName("PRICE").item(0) != null) {
                    price = Double.parseDouble(e.getElementsByTagName("PRICE").item(0).getTextContent());
                }
                if (e.getElementsByTagName("IS_SELECTED").item(0) != null) {
                    isSelected = Boolean.getBoolean(e.getElementsByTagName("IS_SELECTED").item(0).getTextContent());
                }
                if (e.getElementsByTagName("MARKETPLACE").item(0) != null) {
                    marketplace = e.getElementsByTagName("MARKETPLACE").item(0).getTextContent();
                }
                if (e.getElementsByTagName("ID").item(0) != null) {
                    id = e.getElementsByTagName("ID").item(0).getTextContent();
                }
                if (e.getElementsByTagName("CATEGORY").item(0) != null) {
                    category = e.getElementsByTagName("CATEGORY").item(0).getTextContent();
                }
                if (e.getElementsByTagName("COLOR").item(0) != null) {
                    color = e.getElementsByTagName("COLOR").item(0).getTextContent();
                }
                final ImageCanvas ic = new ImageCanvas(f);
                final ShirteeDesign std = new ShirteeDesign(f, designId, title, description, tags, id, category, marketplace, teeHeroPattern, PlatformColors.SHIRTEE.getMatchedPlatformColors(color));
                final ShirteeDesignFrame stdf = new ShirteeDesignFrame(std, stda, ic);
                stda.addDesign(stdf);
            }
        }
    }
}
