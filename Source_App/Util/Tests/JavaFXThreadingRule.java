// 
// Decompiled by Procyon v0.5.36
// 

package Util.Tests;

import javax.swing.SwingUtilities;
import javafx.embed.swing.JFXPanel;
import javafx.application.Platform;
import java.util.concurrent.CountDownLatch;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.junit.rules.TestRule;

public class JavaFXThreadingRule implements TestRule
{
    private static boolean jfxIsSetup;
    
    @Override
    public Statement apply(final Statement statement, final Description description) {
        return new OnJFXThreadStatement(statement);
    }
    
    static /* synthetic */ void access$1(final boolean jfxIsSetup) {
        JavaFXThreadingRule.jfxIsSetup = jfxIsSetup;
    }
    
    private static class OnJFXThreadStatement extends Statement
    {
        private final Statement statement;
        private Throwable rethrownException;
        
        public OnJFXThreadStatement(final Statement aStatement) {
            this.rethrownException = null;
            this.statement = aStatement;
        }
        
        @Override
        public void evaluate() throws Throwable {
            if (!JavaFXThreadingRule.jfxIsSetup) {
                this.setupJavaFX();
                JavaFXThreadingRule.access$1(true);
            }
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            Platform.runLater((Runnable)new Runnable() {
                @Override
                public void run() {
                    try {
                        OnJFXThreadStatement.this.statement.evaluate();
                    }
                    catch (Throwable e) {
                        OnJFXThreadStatement.access$1(OnJFXThreadStatement.this, e);
                    }
                    countDownLatch.countDown();
                }
            });
            countDownLatch.await();
            if (this.rethrownException != null) {
                throw this.rethrownException;
            }
        }
        
        protected void setupJavaFX() throws InterruptedException {
            final long timeMillis = System.currentTimeMillis();
            final CountDownLatch latch = new CountDownLatch(1);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new JFXPanel();
                    latch.countDown();
                }
            });
            System.out.println("javafx initialising...");
            latch.await();
            System.out.println("javafx is initialised in " + (System.currentTimeMillis() - timeMillis) + "ms");
        }
        
        static /* synthetic */ void access$1(final OnJFXThreadStatement onJFXThreadStatement, final Throwable rethrownException) {
            onJFXThreadStatement.rethrownException = rethrownException;
        }
    }
}
