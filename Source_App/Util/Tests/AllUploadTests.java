// 
// Decompiled by Procyon v0.5.36
// 

package Util.Tests;

import Util.Designs.ShirteeDesignTest;
import Util.Sessions.ShirteeSessionTest;
import Util.Designs.SpreadshirtDesignTest;
import Util.Sessions.SpreadshirtSessionTest;
import org.junit.runners.Suite;
import org.junit.runner.RunWith;

@RunWith(Suite.class)
@Suite.SuiteClasses({ SpreadshirtSessionTest.class, SpreadshirtDesignTest.class, ShirteeSessionTest.class, ShirteeDesignTest.class })
public class AllUploadTests
{
}
