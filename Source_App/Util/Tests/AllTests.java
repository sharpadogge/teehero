// 
// Decompiled by Procyon v0.5.36
// 

package Util.Tests;

import GUI.Pages.Tests.OptionsPageTest;
import GUI.Pages.Tests.MainPageTest;
import GUI.Pages.Tests.LoginPageTest;
import Util.Designs.ShirteeDesignTest;
import Util.Sessions.ShirteeSessionTest;
import Util.Designs.SpreadshirtDesignTest;
import Util.Sessions.SpreadshirtSessionTest;
import GUI.OptionFrame.Tests.OptionFrameTextFieldTest;
import GUI.OptionFrame.Tests.OptionFrameSimpleDropDownConfigTest;
import GUI.OptionFrame.Tests.OptionFrameSimpleDropDownColorEnumTest;
import GUI.OptionFrame.Tests.OptionFramePasswordTest;
import GUI.OptionFrame.Tests.OptionFrameKeyValueValueDropDownTest;
import GUI.OptionFrame.Tests.OptionFrameKeyValueDropDownTest;
import GUI.OptionFrame.Tests.OptionFrameDropDownTest;
import GUI.OptionFrame.Tests.OptionFrameDoubleFieldTest;
import GUI.OptionFrame.Tests.OptionFrameCheckboxTest;
import GUI.DesignFrames.SpreadshirtEuDesignFrameTest;
import GUI.DesignFrames.Tests.SpreadshirtComDesignFrameTest;
import GUI.DesignFrames.Tests.ShirteeDesignFrameTest;
import GUI.DesignFrames.Tests.DefaultDesignFrameTest;
import GUI.DesignArea.Tests.DesignAreaTest;
import GUI.Window.Tests.WindowTest;
import org.junit.runners.Suite;
import org.junit.runner.RunWith;

@RunWith(Suite.class)
@Suite.SuiteClasses({ WindowTest.class, DesignAreaTest.class, DefaultDesignFrameTest.class, ShirteeDesignFrameTest.class, SpreadshirtComDesignFrameTest.class, SpreadshirtEuDesignFrameTest.class, OptionFrameCheckboxTest.class, OptionFrameDoubleFieldTest.class, OptionFrameDropDownTest.class, OptionFrameKeyValueDropDownTest.class, OptionFrameKeyValueValueDropDownTest.class, OptionFramePasswordTest.class, OptionFrameSimpleDropDownColorEnumTest.class, OptionFrameSimpleDropDownConfigTest.class, OptionFrameTextFieldTest.class, SpreadshirtSessionTest.class, SpreadshirtDesignTest.class, ShirteeSessionTest.class, ShirteeDesignTest.class, LoginPageTest.class, MainPageTest.class, OptionsPageTest.class })
public class AllTests
{
}
