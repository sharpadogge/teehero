// 
// Decompiled by Procyon v0.5.36
// 

package Util.Filter;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.AttributeSet;
import javax.swing.text.DocumentFilter;

public class IntegerFilterForTextfield extends DocumentFilter
{
    @Override
    public void insertString(final FilterBypass fb, final int offset, final String string, final AttributeSet attr) throws BadLocationException {
        final Document doc = fb.getDocument();
        final StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.insert(offset, string);
        if (this.test(sb.toString())) {
            super.insertString(fb, offset, string, attr);
        }
    }
    
    private boolean test(final String text) {
        try {
            Integer.parseInt(text);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }
    
    @Override
    public void replace(final FilterBypass fb, final int offset, final int length, final String text, final AttributeSet attrs) throws BadLocationException {
        final Document doc = fb.getDocument();
        final StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.replace(offset, offset + length, text);
        if (this.test(sb.toString())) {
            super.replace(fb, offset, length, text, attrs);
        }
    }
    
    @Override
    public void remove(final FilterBypass fb, final int offset, final int length) throws BadLocationException {
        final Document doc = fb.getDocument();
        final StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.delete(offset, offset + length);
        if (this.test(sb.toString())) {
            super.remove(fb, offset, length);
        }
    }
}
