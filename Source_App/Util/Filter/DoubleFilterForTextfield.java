// 
// Decompiled by Procyon v0.5.36
// 

package Util.Filter;

import java.util.regex.Pattern;
import javafx.scene.control.TextFormatter;

public class DoubleFilterForTextfield extends TextFormatter
{
    public DoubleFilterForTextfield() {
        super(change -> Pattern.compile("\\d*|\\d+\\.\\d*").matcher(change.getControlNewText()).matches() ? change : null);
    }
}
