// 
// Decompiled by Procyon v0.5.36
// 

package Util.User;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User
{
    private String username;
    private Date loginTime;
    
    public User(final String username, final Date loginTime) {
        this.username = username;
        this.loginTime = loginTime;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public String getLoginTimeAsString() {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(this.loginTime);
    }
    
    public Date getLoginTime() {
        return this.loginTime;
    }
}
