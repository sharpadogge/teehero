// 
// Decompiled by Procyon v0.5.36
// 

package Util.Factories;

import GUI.OptionFrame.OptionFrameCheckbox;
import GUI.OptionFrame.OptionFramePassword;
import GUI.OptionFrame.OptionFrameKeyValueDropDown;
import GUI.OptionFrame.OptionFrameTextField;
import GUI.OptionFrame.OptionFrameDropDown;
import GUI.OptionFrame.OptionFrameSimpleDropDownConfig;
import GUI.OptionFrame.OptionFrameKeyValueValueDropDown;
import GUI.OptionFrame.OptionFrameSimpleDropDownColorEnum;
import GUI.OptionFrame.OptionFrame;
import GUI.OptionFrame.OptionFrameDoubleField;
import Util.Enums.OptionNamesInConfig;
import GUI.OptionArea.OptionsArea;

public class OptionFrameFactory
{
    private OptionsArea oa;
    
    public OptionFrameFactory(final OptionsArea oa) {
        this.oa = oa;
    }
    
    public void createOptionFrames() {
        OptionNamesInConfig[] values;
        for (int length = (values = OptionNamesInConfig.values()).length, i = 0; i < length; ++i) {
            final OptionNamesInConfig o = values[i];
            if (o.getOptionsAreaName() == this.oa.getOptionsAreaName()) {
                this.createOptionDetail(o.getOptionType(), o);
            }
        }
    }
    
    private void createOptionDetail(final String optionType, final OptionNamesInConfig o) {
        switch (optionType) {
            case "double": {
                this.oa.addOption(new OptionFrameDoubleField(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), this.oa));
                break;
            }
            case "defaultListEnum": {
                this.oa.addOption(new OptionFrameSimpleDropDownColorEnum(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), this.oa));
                break;
            }
            case "threeColList": {
                this.oa.addOption(new OptionFrameKeyValueValueDropDown(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), this.oa));
                break;
            }
            case "defaultList": {
                this.oa.addOption(new OptionFrameSimpleDropDownConfig(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), this.oa));
                break;
            }
            case "list": {
                this.oa.addOption(new OptionFrameDropDown(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), o.getConntectedOptions(), this.oa));
                break;
            }
            case "text": {
                this.oa.addOption(new OptionFrameTextField(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), this.oa));
                break;
            }
            case "twoColList": {
                this.oa.addOption(new OptionFrameKeyValueDropDown(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), this.oa));
                break;
            }
            case "password": {
                this.oa.addOption(new OptionFramePassword(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), this.oa));
                break;
            }
            case "checkbox": {
                this.oa.addOption(new OptionFrameCheckbox(o.getReadFromOptions(), o.getOptionName(), o.getOptionText(), this.oa));
                break;
            }
            default:
                break;
        }
    }
}
