// 
// Decompiled by Procyon v0.5.36
// 

package Util.Factories;

import Util.MenuActions.ChangeEnableStateOfAreaAction;
import Util.MenuActions.LoadOptionsPageAction;
import Util.Actions.ImportDataFromFileAction;
import Util.Actions.ExportDataToFileAction;
import Util.MenuActions.ChangeEnableStateOfAllAreasAction;
import javafx.event.EventHandler;
import Util.Actions.ShowRestrictionPopupAction;
import Util.Enums.PlatformRestrictions;
import javafx.scene.Node;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Menu;
import Util.Enums.MenuItems;
import Util.Enums.GUIStyles;
import javafx.scene.control.MenuBar;
import GUI.Pages.MainPage;

public class MenuItemFactory
{
    private MainPage mp;
    private MenuBar menuBar;
    
    public MenuItemFactory(final MainPage mp) {
        this.mp = mp;
        this.createMenuBar();
        this.menuBar.setLayoutX(0.0);
        this.menuBar.setLayoutY(0.0);
        mp.getLayoutPane().getChildren().add((Object)this.menuBar);
    }
    
    private void createMenuBar() {
        if (this.menuBar == null) {
            this.menuBar = new MenuBar();
            this.menuBar.getStyleClass().add((Object)GUIStyles.BASIC_MENU_BAR.getStyleClasses());
            this.createSubMenus(this.menuBar);
        }
        else {
            this.createSubMenus(this.menuBar);
        }
    }
    
    private void createSubMenus(final MenuBar menuBar) {
        MenuItems[] values;
        for (int length = (values = MenuItems.values()).length, j = 0; j < length; ++j) {
            final MenuItems mi = values[j];
            boolean exists = false;
            Menu menu = null;
            for (int i = 0; i < menuBar.getMenus().size(); ++i) {
                if (((Menu)menuBar.getMenus().get(i)).getText() == mi.getMenuName()) {
                    exists = true;
                    menu = (Menu)menuBar.getMenus().get(i);
                    break;
                }
            }
            if (exists) {
                this.createMenuItems(menu, mi);
            }
            else {
                final Menu subMenu = new Menu(mi.getMenuName());
                subMenu.getStyleClass().add((Object)GUIStyles.BASIC_SUB_MENU.getStyleClasses());
                this.createMenuItems(subMenu, mi);
                menuBar.getMenus().add((Object)subMenu);
            }
        }
    }
    
    private void createMenuItems(final Menu menu, final MenuItems mi) {
        MenuItem item = null;
        final String menuItemType;
        switch (menuItemType = mi.getMenuItemType()) {
            case "normal": {
                item = new MenuItem(mi.getMenuItemText());
                item.getStyleClass().add((Object)GUIStyles.BASIC_MENU_ITEM.getStyleClasses());
                this.addActionToMenuItem(item, mi.getFunction(), menu);
                menu.getItems().add((Object)item);
                break;
            }
            case "checkbox": {
                final CheckBox cb = new CheckBox(mi.getMenuItemText());
                item = (MenuItem)new CustomMenuItem((Node)cb);
                ((CustomMenuItem)item).setHideOnClick(false);
                cb.setSelected(true);
                cb.getStyleClass().addAll((Object[])new String[] { GUIStyles.BASIC_CHECKBOX.getStyleClasses(), GUIStyles.BASIC_MENU_CHECKBOX.getStyleClasses() });
                item.getStyleClass().add((Object)GUIStyles.BASIC_MENU_ITEM.getStyleClasses());
                this.addActionToMenuItem(item, mi.getFunction(), menu);
                menu.getItems().add((Object)item);
                break;
            }
            default:
                break;
        }
    }
    
    private void addActionToMenuItem(final MenuItem item, final String action, final Menu menu) {
        switch (action) {
            case "showRestrictionsShirtee": {
                item.setOnAction((EventHandler)new ShowRestrictionPopupAction(PlatformRestrictions.SHIRTEE.getHeaderText(), PlatformRestrictions.SHIRTEE.getPlatformRestrictions()));
                break;
            }
            case "disableAll": {
                item.setOnAction((EventHandler)new ChangeEnableStateOfAllAreasAction(this.mp.getSpecialAreasButtonList(), false, menu, this.mp.getBtnGeneral()));
                break;
            }
            case "showRestrictionsSpreadshirt": {
                item.setOnAction((EventHandler)new ShowRestrictionPopupAction(PlatformRestrictions.SPREADSHIRT.getHeaderText(), PlatformRestrictions.SPREADSHIRT.getPlatformRestrictions()));
                break;
            }
            case "export": {
                item.setOnAction((EventHandler)new ExportDataToFileAction(this.mp));
                break;
            }
            case "import": {
                item.setOnAction((EventHandler)new ImportDataFromFileAction(this.mp));
                break;
            }
            case "enableAll": {
                item.setOnAction((EventHandler)new ChangeEnableStateOfAllAreasAction(this.mp.getSpecialAreasButtonList(), true, menu, this.mp.getBtnGeneral()));
                break;
            }
            case "openSettings": {
                item.setOnAction((EventHandler)new LoadOptionsPageAction(this.mp));
                break;
            }
            case "spreadshirtCOM": {
                ((CustomMenuItem)item).setOnAction((EventHandler)new ChangeEnableStateOfAreaAction(this.mp.getBtnSpreadshirtCom(), this.mp.getBtnGeneral()));
                break;
            }
            case "spreadshirtEU": {
                ((CustomMenuItem)item).setOnAction((EventHandler)new ChangeEnableStateOfAreaAction(this.mp.getBtnSpreadshirtEu(), this.mp.getBtnGeneral()));
                break;
            }
            case "shirtee": {
                ((CustomMenuItem)item).setOnAction((EventHandler)new ChangeEnableStateOfAreaAction(this.mp.getBtnShirtee(), this.mp.getBtnGeneral()));
                break;
            }
            default:
                break;
        }
    }
}
