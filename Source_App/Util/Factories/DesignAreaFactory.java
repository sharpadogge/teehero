// 
// Decompiled by Procyon v0.5.36
// 

package Util.Factories;

import GUI.DesignArea.DesignArea;
import Util.Enums.DesignAreaDefinition;
import GUI.Pages.MainPage;

public class DesignAreaFactory
{
    private MainPage mp;
    
    public DesignAreaFactory(final MainPage mp) {
        this.mp = mp;
    }
    
    public void createNewDesignArea(final int areaCode) {
        final DesignAreaDefinition value = DesignAreaDefinition.values()[areaCode];
        switch (value) {
            case DEFAULT_AREA: {
                new DesignArea(DesignAreaDefinition.DEFAULT_AREA.getAreaName(), this.mp);
                break;
            }
            case SPREADSHIRT_EU_AREA: {
                new DesignArea(DesignAreaDefinition.SPREADSHIRT_EU_AREA.getAreaName(), this.mp);
                break;
            }
            case SPREADSHIRT_COM_AREA: {
                new DesignArea(DesignAreaDefinition.SPREADSHIRT_COM_AREA.getAreaName(), this.mp);
                break;
            }
            case SHIRTEE_AREA: {
                new DesignArea(DesignAreaDefinition.SHIRTEE_AREA.getAreaName(), this.mp);
                break;
            }
        }
    }
}
