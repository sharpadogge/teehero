// 
// Decompiled by Procyon v0.5.36
// 

package Util.Analytics;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.net.ssl.HttpsURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import Util.User.User;

public class Analyser
{
    private final String secret = "o1A4Fjo0bFrS7lb3VF3zS4CfeS5VS9Jr";
    private final String analyticsApi = "https://teehero.net/loginTesting/";
    private final User user;
    private final int sessionHash;
    
    public Analyser(final User user) {
        this.user = user;
        this.sessionHash = (String.valueOf(user.getUsername()) + user.getLoginTimeAsString()).hashCode();
    }
    
    public void writeUploadReportToServer(final int numberOfDesigns, final String platform) throws Exception {
        String query = "user=" + URLEncoder.encode(this.user.getUsername(), "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "numberOfDesigns=" + numberOfDesigns;
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "secret=" + URLEncoder.encode("o1A4Fjo0bFrS7lb3VF3zS4CfeS5VS9Jr", "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "platform=" + URLEncoder.encode(platform, "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "entryHash=" + this.sessionHash;
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "status=" + URLEncoder.encode("Upload started", "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "loginTime=" + URLEncoder.encode(this.user.getLoginTimeAsString(), "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "operation=" + URLEncoder.encode("upload", "UTF-8");
        this.sendRequest(query);
    }
    
    public void writeUploadFailReportToServer(final int numberOfDesigns, final String platform) throws Exception {
        String query = "user=" + URLEncoder.encode(this.user.getUsername(), "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "numberOfDesigns=" + numberOfDesigns;
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "secret=" + URLEncoder.encode("o1A4Fjo0bFrS7lb3VF3zS4CfeS5VS9Jr", "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "platform=" + URLEncoder.encode(platform, "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "entryHash=" + this.sessionHash;
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "status=" + URLEncoder.encode("Upload failed", "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "loginTime=" + URLEncoder.encode(this.user.getLoginTimeAsString(), "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "operation=" + URLEncoder.encode("upload", "UTF-8");
        this.sendRequest(query);
    }
    
    public void writeUploadSuccessReportToServer(final int numberOfDesigns, final String platform) throws Exception {
        String query = "user=" + URLEncoder.encode(this.user.getUsername(), "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "numberOfDesigns=" + numberOfDesigns;
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "secret=" + URLEncoder.encode("o1A4Fjo0bFrS7lb3VF3zS4CfeS5VS9Jr", "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "platform=" + URLEncoder.encode(platform, "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "entryHash=" + this.sessionHash;
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "status=" + URLEncoder.encode("Upload successful", "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "loginTime=" + URLEncoder.encode(this.user.getLoginTimeAsString(), "UTF-8");
        query = String.valueOf(query) + "&";
        query = String.valueOf(query) + "operation=" + URLEncoder.encode("upload", "UTF-8");
        this.sendRequest(query);
    }
    
    public String sendRequest(final String query) throws Exception {
        final String httpsURL = "https://teehero.net/loginTesting/";
        final URL myurl = new URL(httpsURL);
        final HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);
        final DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();
        final DataInputStream input = new DataInputStream(con.getInputStream());
        final StringBuffer response = new StringBuffer();
        for (int c = input.read(); c != -1; c = input.read()) {
            response.append((char)c);
        }
        input.close();
        return response.toString();
    }
}
