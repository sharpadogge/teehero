// 
// Decompiled by Procyon v0.5.36
// 

package Util.Canvas;

import java.io.IOException;
import javafx.scene.CacheHint;
import javafx.scene.image.ImageView;
import java.io.FileNotFoundException;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import java.io.InputStream;
import javafx.scene.image.Image;
import java.io.FileInputStream;
import Util.Enums.FilePaths;
import Util.Handler.InterfaceSizeManager;
import java.io.File;
import javafx.scene.layout.Pane;

public class ImageCanvas extends Pane implements Cloneable
{
    public ImageCanvas(final File design) {
        final InterfaceSizeManager ism = new InterfaceSizeManager();
        ism.setControllerSize(this, 100, 100);
        try {
            final BackgroundImage myBI = new BackgroundImage(new Image((InputStream)new FileInputStream(new File(String.valueOf(FilePaths.IMG_PATH.getPath()) + "Raster.png")), 100.0, 100.0, false, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
            this.setBackground(new Background(new BackgroundImage[] { myBI }));
        }
        catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        try {
            final FileInputStream inputstream = new FileInputStream(design);
            final Image img = new Image((InputStream)inputstream, 100.0, 100.0, false, true);
            final ImageView imgView = new ImageView(img);
            imgView.setCache(true);
            imgView.setCacheHint(CacheHint.SPEED);
            this.getChildren().add((Object)imgView);
            try {
                inputstream.close();
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        catch (FileNotFoundException e3) {
            e3.printStackTrace();
        }
    }
    
    public ImageCanvas getCloneOfImageCanvas() {
        try {
            return (ImageCanvas)this.clone();
        }
        catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
