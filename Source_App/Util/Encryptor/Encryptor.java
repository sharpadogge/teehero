// 
// Decompiled by Procyon v0.5.36
// 

package Util.Encryptor;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class Encryptor
{
    private final String seed = "wK4hE8Gj,cU\\Ag#n";
    private StandardPBEStringEncryptor encryptor;
    
    public Encryptor() {
        (this.encryptor = new StandardPBEStringEncryptor()).setPassword("wK4hE8Gj,cU\\Ag#n");
    }
    
    public String encryptData(final String data) {
        return this.encryptor.encrypt(data);
    }
    
    public String decryptData(final String encryptData) {
        return this.encryptor.decrypt(encryptData);
    }
}
