// 
// Decompiled by Procyon v0.5.36
// 

package Util.MenuActions;

import javafx.event.Event;
import java.util.Iterator;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.Button;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ChangeEnableStateOfAllAreasAction implements EventHandler<ActionEvent>
{
    private ArrayList<Button> btns;
    private boolean state;
    private Menu menu;
    private Button btnGeneral;
    
    public ChangeEnableStateOfAllAreasAction(final ArrayList<Button> btns, final boolean state, final Menu menu, final Button btnGeneral) {
        this.btns = btns;
        this.state = state;
        this.menu = menu;
        this.btnGeneral = btnGeneral;
    }
    
    public void handle(final ActionEvent arg0) {
        for (final Button btn : this.btns) {
            btn.setDisable(!this.state);
        }
        for (int i = 0; i < this.menu.getItems().size(); ++i) {
            if (((MenuItem)this.menu.getItems().get(i)).getClass() == CustomMenuItem.class) {
                final CheckBox cb = (CheckBox)((CustomMenuItem)this.menu.getItems().get(i)).getContent();
                cb.setSelected(this.state);
            }
        }
        this.btnGeneral.fire();
    }
}
