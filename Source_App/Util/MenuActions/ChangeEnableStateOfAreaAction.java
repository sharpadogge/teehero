// 
// Decompiled by Procyon v0.5.36
// 

package Util.MenuActions;

import javafx.event.Event;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ChangeEnableStateOfAreaAction implements EventHandler<ActionEvent>
{
    private Button btn;
    private Button btnGeneral;
    
    public ChangeEnableStateOfAreaAction(final Button btn, final Button btnGeneral) {
        this.btn = btn;
        this.btnGeneral = btnGeneral;
    }
    
    public void handle(final ActionEvent arg0) {
        if (((CheckBox)((CustomMenuItem)arg0.getSource()).getContent()).isSelected()) {
            this.btn.setDisable(false);
        }
        else {
            this.btn.setDisable(true);
            this.btnGeneral.fire();
        }
    }
}
