// 
// Decompiled by Procyon v0.5.36
// 

package Util.MenuActions;

import javafx.event.Event;
import javafx.scene.Scene;
import GUI.Pages.OptionsPage;
import javafx.scene.layout.BorderPane;
import GUI.Window.Window;
import GUI.Pages.MainPage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class LoadOptionsPageAction implements EventHandler<ActionEvent>
{
    private MainPage mp;
    
    public LoadOptionsPageAction(final MainPage mp) {
        this.mp = mp;
    }
    
    public void handle(final ActionEvent arg0) {
        final Window window = new Window("Options", 1190, 778);
        final OptionsPage op = new OptionsPage(this.mp, new BorderPane(), window);
        window.setScene((Scene)op);
        window.show();
    }
}
