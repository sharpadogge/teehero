// 
// Decompiled by Procyon v0.5.36
// 

package Util.Handler;

import GUI.OptionArea.OptionsArea;
import java.util.HashMap;

public class OptionsAreaHandler
{
    private HashMap<String, OptionsArea> optionsAreas;
    
    public OptionsAreaHandler() {
        this.optionsAreas = new HashMap<String, OptionsArea>();
    }
    
    public OptionsArea getOptionsArea(final String name) {
        return this.optionsAreas.get(name);
    }
    
    public void addOptionsArea(final String name, final OptionsArea oa) {
        this.optionsAreas.put(name, oa);
    }
    
    public HashMap<String, OptionsArea> getOptionsAreas() {
        return this.optionsAreas;
    }
}
