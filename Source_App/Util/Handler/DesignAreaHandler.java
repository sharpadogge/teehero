// 
// Decompiled by Procyon v0.5.36
// 

package Util.Handler;

import java.util.List;
import java.util.Iterator;
import GUI.DesignFrames.DesignFrame;
import java.util.Map;
import GUI.DesignArea.DesignArea;
import java.util.HashMap;

public class DesignAreaHandler
{
    private HashMap<String, DesignArea> designAreas;
    private int designIdCounter;
    
    public DesignAreaHandler() {
        this.designIdCounter = 1;
        this.designAreas = new HashMap<String, DesignArea>();
    }
    
    public DesignArea getDesignArea(final String name) {
        return this.designAreas.get(name);
    }
    
    public void addDesignArea(final String name, final DesignArea da) {
        this.designAreas.put(name, da);
    }
    
    public void removeDesignFromAllDesignAreas(final int designIndex) throws Exception {
        for (final Map.Entry<String, DesignArea> entry : this.designAreas.entrySet()) {
            final List<DesignFrame> li = entry.getValue().getDesignFrameList();
            for (final DesignFrame df : li) {
                if (df.getDesign().getDesignId() == designIndex) {
                    entry.getValue().removeDesign(df);
                    break;
                }
            }
        }
    }
    
    public int getDesignId() {
        return this.designIdCounter++;
    }
    
    public HashMap<String, DesignArea> getDesignAreas() {
        return this.designAreas;
    }
}
