// 
// Decompiled by Procyon v0.5.36
// 

package Util.Handler;

import javafx.scene.layout.Pane;
import javafx.scene.control.Control;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Dimension;

public class InterfaceSizeManager
{
    private Dimension screenSize;
    private double screenHeight;
    private double screenWidth;
    private double hdScreenHeight;
    private double hdScreenWidth;
    private double hdScreenRatio;
    private double screenRatio;
    
    public InterfaceSizeManager() {
        this.hdScreenHeight = 1080.0;
        this.hdScreenWidth = 1920.0;
        this.hdScreenRatio = this.hdScreenWidth / this.hdScreenHeight;
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.screenHeight = this.screenSize.getHeight();
        this.screenWidth = this.screenSize.getWidth();
        this.screenRatio = this.screenWidth / this.screenHeight;
    }
    
    public int getRelativeHeight(final double percentage) {
        return (int)Math.round(this.screenHeight * percentage * this.screenRatio);
    }
    
    public int getRelativeWidth(final double percentage) {
        return (int)Math.round(this.screenWidth * percentage);
    }
    
    public int getScaledHeightBasedOnFullHD(final double height) {
        return this.getRelativeHeight(height / this.hdScreenHeight / this.hdScreenRatio);
    }
    
    public int getScaledWidthBasedOnFullHD(final double width) {
        return this.getRelativeWidth(width / this.hdScreenWidth);
    }
    
    public Rectangle createRelativeBounds(final int x, final int y, final int width, final int height) {
        return new Rectangle(this.getScaledWidthBasedOnFullHD(x), this.getScaledHeightBasedOnFullHD(y), this.getScaledWidthBasedOnFullHD(width), this.getScaledHeightBasedOnFullHD(height));
    }
    
    public Dimension createRelativeDimension(final int x, final int y) {
        return new Dimension(this.getScaledWidthBasedOnFullHD(x), this.getScaledHeightBasedOnFullHD(y));
    }
    
    public double createRelativeHeight(final int y) {
        return this.getScaledHeightBasedOnFullHD(y);
    }
    
    public double createRelativeWidth(final int x) {
        return this.getScaledWidthBasedOnFullHD(x);
    }
    
    public double createRelativeYPosition(final int y) {
        return this.getScaledHeightBasedOnFullHD(y);
    }
    
    public double createRelativeXPosition(final int x) {
        return this.getScaledWidthBasedOnFullHD(x);
    }
    
    public Dimension getScreenSize() {
        return this.screenSize;
    }
    
    public double getScreenHeight() {
        return this.screenHeight;
    }
    
    public double getScreenWidth() {
        return this.screenWidth;
    }
    
    public void setControllerPositionAndSize(final Control c, final int x, final int y, final int width, final int height) {
        c.setLayoutX((double)x);
        c.setLayoutY((double)y);
        c.setMaxWidth((double)width);
        c.setMaxHeight((double)height);
        c.setMinWidth((double)width);
        c.setMinHeight((double)height);
    }
    
    public void setControllerPositionAndSize(final Pane c, final int x, final int y, final int width, final int height) {
        c.setLayoutX((double)x);
        c.setLayoutY((double)y);
        c.setMaxWidth((double)width);
        c.setMaxHeight((double)height);
        c.setMinWidth((double)width);
        c.setMinHeight((double)height);
    }
    
    public void setControllerSize(final Pane c, final int width, final int height) {
        c.setMaxWidth((double)width);
        c.setMaxHeight((double)height);
        c.setMinWidth((double)width);
        c.setMinHeight((double)height);
        c.setPrefHeight((double)height);
        c.setPrefWidth((double)width);
    }
    
    public void setControllerSize(final Control c, final int width, final int height) {
        c.setMaxWidth((double)width);
        c.setMaxHeight((double)height);
        c.setMinWidth((double)width);
        c.setMinHeight((double)height);
        c.setPrefHeight((double)height);
        c.setPrefWidth((double)width);
    }
}
