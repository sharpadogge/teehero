// 
// Decompiled by Procyon v0.5.36
// 

package Util.Launcher;

import javafx.scene.Scene;
import GUI.Pages.LoginPage;
import javafx.scene.layout.BorderPane;
import GUI.Window.Window;
import javafx.stage.Stage;
import javafx.application.Application;

public class Launcher extends Application
{
    public static void main(final String[] args) {
        launch(args);
    }
    
    public void start(final Stage primaryStage) throws Exception {
        System.out.println(System.getProperty("user.dir"));
        final Window l = new Window("TeeHero Uploader", 400, 400);
        final LoginPage lg = new LoginPage(new BorderPane(), l);
        l.setScene((Scene)lg);
        l.show();
    }
}
